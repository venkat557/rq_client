import pika, sys, os

#import get_topics_using_clusters_v1 as gtc_v1
print("client file executed")

def main():
    connection = pika.BlockingConnection(pika.ConnectionParameters('35.229.150.151',8080))
    channel = connection.channel()

    channel.queue_declare(queue='hello')

    def callback(ch, method, properties, body):
        print(" [x] Received %r" % body)
        print(body.decode("utf-8"))

    channel.basic_consume(queue='hello', on_message_callback=callback, auto_ack=True)

    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
