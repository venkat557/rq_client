try:
    #!/usr/bin/env python
    # coding: utf-8

    # In[ ]:


    import warnings
    warnings.filterwarnings("ignore")

    import gc
    import pandas as pd
    import math
    import time
    import sys, os
    import numpy as np
    sys.path.append(os.environ['DATA_SCIENCE_HOME'])
    import helper.mysql.get_connection  as SQL
    #import helper.lib.utilities  as UT
    from datetime import datetime, timedelta

    print("Script start at: ", datetime.now())

    #from_day = datetime.strftime(datetime.now() - timedelta(2), '%Y-%m-%d')


    # In[ ]:





    # #### Fetched Raw data

    # In[ ]:


    # pwd


    # In[ ]:


    x = datetime.now()
    print(x)

    df = SQL.fetch_data_with_schema("""
    select sho.awb, 
    sho.order_id, 
    sho.id as shipment_id,
    sho.carrier_id, 
    c.name as carrier_name,
    case when w.id is null then 'DS' else w.id end as warehouse_id,
    case when w.city is null then 'DS' else w.city end as warehouse_city, 
    sa.city_name, 
    state.name as state,
    b.label as actual_zone,
    CASE WHEN b.label in ('A','Local','LocalRAS') THEN 'Local'
         WHEN b.label in ('B','RegionalRAS','Regional','RegionalROS','RegionalUP') THEN 'Regional'
         WHEN b.label in ('Metro','C1','C2','MetroRAS','C') THEN 'Metro'
         WHEN b.label in ('ROI','D1','D2','ROIRAS','ROIROS','ROIUP','D','East','EastRAS') THEN 'ROI'
         WHEN b.label in ('SpecialZone','E','Special','SpecialRAS','Northeast','NortheastROS','NortheastUP','JK','JKUP','JKROS','NA') THEN 'SpecialZone'
         else 'NA'
    END as zone,
    CASE WHEN b.label in ('A','Local','LocalRAS', 'B','RegionalRAS','Regional','RegionalROS','RegionalUP') THEN 'short_zone'
         WHEN b.label is null then 'NA'
         else 'long_zone'
    END as zone2,
    case when b.expected_delivery_hours is null then 0 else b.expected_delivery_hours end as expected_delivery_hours,

    -- dates
    from_unixtime(sho.delivery_timestamp) as delivery_date,
    from_unixtime(sho.dispatch_time) as warehouse_dispatch_date, -- *****
    from_unixtime(so.time_stamp) as order_date,
    -- from_unixtime(sho.dispatch_time+(b.expected_delivery_hours*3600)) as expected_delivery_date, -- *****

    -- monthname(from_unixtime(sho.dispatch_time)) as months, -- *****
    week(from_unixtime(sho.dispatch_time)) as weeks_wh, -- *****

    monthname(from_unixtime(so.time_stamp)) as months_order, 
    week(from_unixtime(so.time_stamp)) as weeks_order,


    -- conditions
    case when sho.carrier_id in (28, 43) then 1 else 0 end as is_express_order,
    -- case when from_unixtime(sho.delivery_timestamp, '%Y-%m-%d') < from_unixtime(sho.dispatch_time+(b.expected_delivery_hours*3600), '%Y-%m-%d')  then 0 else 1 end as is_breached_by_dispatch, -- *****

    -- tats
    round((sho.dispatch_time - so.time_stamp)/3600, 1) as tat_wh_o2s, -- *****
    round((sho.delivery_timestamp - sho.dispatch_time)/3600, 1) as tat_wh_s2d, -- *****
    round((sho.delivery_timestamp - so.time_stamp)/3600, 1) as tat_o2d,

    -- other parameters
    sho.status as shipment_status, 
    so.status as order_status, 
    sa.postal_code as pincode,
    lower(so.method) as method,
    sho.weight,
    sho.vol_weight,
    sho.total_cost as shipment_total_cost,
    sho.collection_amount as shipment_collection_amount,
    sho.shipment_type as shipment_shipment_type

    from shipping_order sho
        join shop_order so on sho.order_id = so.id
        join shipping_areas sa on so.ship_postal_code=sa.postal_code
        left join
            (
                select spa.area_id, spa.zone_id, spa.carrier_id, spa.warehouse_id, sz.label, expected_delivery_hours
                from shipping_prepaidarea spa
                join shipping_zone sz on spa.zone_id = sz.id
                where zone_id > 0
                group by 1,2,3,4
            )b
            on sa.id=b.area_id and sho.carrier_id=b.carrier_id and sho.warehouse_id=b.warehouse_id
        left join shipping_carrier c on c.id = sho.carrier_id
        left join warehouse_warehouse w on w.id = sho.warehouse_id
        join shipping_state state on state.id = sa.state_id
    WHERE sho.dispatch_time >=  UNIX_TIMESTAMP(date_sub(current_date(), interval 150 day)) -- UNIX_TIMESTAMP('2020-11-01') -- change 10 to 100 days 
        and sho.status not in ('Pending', 'Prepared', 'Preparing', 'Processing', 'Lost')
        and so.order_type != 'b2b'
    """, 'slave')

    print("Total time to fetched data is: ", datetime.now() - x)


    # In[ ]:


    # df.shape


    # In[ ]:


    # df_copy= df.copy()
    # #df = df_copy.copy()


    # In[ ]:





    # In[ ]:


    # df.shipment_status.value_counts()


    # In[ ]:


    df.shipment_status.replace(['Reverted', 'Returned'], 'RTO', inplace=True)

    df.weight = pd.to_numeric(df.weight, errors='coerce').fillna(250)
    df.weight[df.weight==0] = 0.250
    df.weight = df.weight.astype(float) * 1000

    df.vol_weight = df.vol_weight.astype(float) * 1000
    df = df[~df.carrier_id.isna()]
    df.carrier_id = df.carrier_id.astype(int)
    df.pincode = df.pincode.astype(int)

    df.vol_weight.fillna(0, inplace=True)


    # In[ ]:


    df['box_max_weight'] = df[['weight', 'vol_weight']].max(axis=1)
    df.box_max_weight[df.box_max_weight>20000] = 20000


    # In[ ]:


    # df.box_max_weight.dtypes


    # In[ ]:





    # In[ ]:





    # In[ ]:





    # In[ ]:





    # In[ ]:


    x = datetime.now()
    print(x)

    df.reset_index(inplace=True)
    df.drop(['index'], axis=1, inplace=True)

    final_max_weight = []
    for i in range(len(df)):
        if df['box_max_weight'][i] <= 250:
            final_max_weight.append(250)
        else:
            final_max_weight.append((math.ceil(df['box_max_weight'][i]/500))*500)

    print("Total time for calculation is: ", datetime.now() - x)


    # In[ ]:


    df['box_max_weight'] = final_max_weight


    # In[ ]:


    # df.box_max_weight.unique()


    # In[ ]:


    # sorted(df.carrier_id.unique())


    # # append inscan date

    # In[ ]:


    order_list= "','".join(df.order_id.unique().astype(str).tolist())
    awb_list= "','".join(df.awb.unique().astype(str).tolist())


    # In[ ]:


    x = datetime.now()
    print(x)

    #getting Carrier In-Scan time
    query_inscan_time="""
    select css.awb,  css.carrier_id, css.status_code, 
    css.main_status 
    , convert_tz(min(status_date_time),'+00:00','+00:00') as dispatch_date
    , week(convert_tz(min(status_date_time),'+00:00','+00:00')) as weeks
    from courier_shipment_status css
    where css.awb in ('{}') 
    and css.status_code in ('PUD','PKD','X-PPOM','X-PROM','Received At DC','S-001','S-015','INSCAN','002','INT','In Scan - HUB','cp_4', 'cp_5')
    -- and (css.status_code in ('PUD','PKD','X-PPOM','X-PROM','Received At DC','S-001','S-015','INSCAN','002','INT','In Scan - HUB','cp_4') or css.main_status in ('PUD','PKD','X-PPOM','X-PROM','Received At DC','S-001','S-015','INSCAN','002','INT','In Scan - HUB','cp_4')) 
    GROUP BY css.awb
    """.format(awb_list)
    tmp_df_inscan_time = SQL.fetch_data_with_schema(query_inscan_time,'slave')

    print("Total time to fetched data is: ", datetime.now() - x)


    # In[ ]:


    tmp_df_inscan_time.shape


    # In[ ]:


    tmp_df_inscan_time.head()


    # In[ ]:


    tmp_df_inscan_time.dispatch_date = tmp_df_inscan_time.dispatch_date.astype('datetime64')


    # In[ ]:


    df = pd.merge(left=df, right=tmp_df_inscan_time[['awb', 'dispatch_date', 'weeks']], how='left', on='awb')
    df.dispatch_date.fillna(df.warehouse_dispatch_date, inplace=True)
    df.weeks.fillna(df.weeks_wh, inplace=True)
    df.head()


    # In[ ]:





    # In[ ]:


    ### Other field calculations ###
    # expected_delivery_date
    df.expected_delivery_hours.fillna(0, inplace=True)
    df['expected_delivery_date'] = df.dispatch_date + pd.to_timedelta(df.expected_delivery_hours, unit='hour')

    # Dispatch month and week
    df['months'] = df.dispatch_date.dt.month
    #df['weeks'] = df.dispatch_date.dt.week -- calculated in sql as seems getting error in python

    #is_breached_by_dispatch (tat breach)
    df['is_breached_by_dispatch'] = (df.delivery_date <= df.expected_delivery_date)*1

    # TAT o2s, s2d
    df['tat_o2s'] = (df.dispatch_date - df.order_date).astype('timedelta64[h]')
    df['tat_s2d'] = (df.delivery_date - df.dispatch_date).astype('timedelta64[h]')


    # In[ ]:


    #df1[df1.shipment_status=='Delivered'][['awb', 'order_date', 'delivery_date', 'dispatch_date','tat_o2s', 'tat_s2d']].head()


    # In[ ]:


    sorted(df.carrier_id.unique())


    # In[ ]:


    calculated_cp = [13, 17, 25, 26, 28, 29, 37, 40, 41, 43]


    # In[ ]:


    # df splits
    sorted(df.carrier_id.unique())

    ecom = df[df.carrier_id== 13 ] #Ecom
    xb = df[df.carrier_id== 17] #XB
    sfx = df[df.carrier_id== 25] #SFX
    delv = df[df.carrier_id== 26] #Del
    sfxExp = df[df.carrier_id== 28] #sfxExp
    bdp = df[df.carrier_id== 29] #bdp
    bda = df[df.carrier_id== 37] #bda
    on = df[df.carrier_id== 40] #on
    am = df[df.carrier_id== 41] #am
    ke = df[df.carrier_id== 43] #kerryIndev


    # In[ ]:


    other_cp = df[~df.carrier_id.isin(calculated_cp)]


    # In[ ]:


    other_cp.shape


    # In[ ]:





    # ## Appending Cost

    # ### Ecom costing

    # In[ ]:


    ecom.shape, ecom.actual_zone.unique()


    # In[ ]:


    if ecom.shape[0] != 0:
        ecom['purplle_cost'] = 0
        ecom1 = ecom[ecom.dispatch_date.dt.date <= datetime.now().date()]

        if ecom1.shape[0]!=0:
            ecom1.replace(250, 500, inplace=True)
            ecom1.purplle_cost.loc[ecom1.actual_zone.isin(['Local'])] = 17 + ((ecom1.box_max_weight.loc[ecom1.actual_zone.isin(['Local'])]-500)/500) * 12
            ecom1.purplle_cost.loc[ecom1.actual_zone.isin(['Regional', 'RegionalROS', 'RegionalUP'])] = 21 + ((ecom1.box_max_weight.loc[ecom1.actual_zone.isin(['Regional', 'RegionalROS', 'RegionalUP'])]-500)/500) * 18
            ecom1.purplle_cost.loc[ecom1.actual_zone.isin(['Metro'])] = 27 + ((ecom1.box_max_weight.loc[ecom1.actual_zone.isin(['Metro'])]-500)/500) * 26
            ecom1.purplle_cost.loc[ecom1.actual_zone.isin(['ROI', 'ROIROS', 'ROIUP'])] = 33 + ((ecom1.box_max_weight.loc[ecom1.actual_zone.isin(['ROI', 'ROIROS', 'ROIUP'])]-500)/500) * 28
            ecom1.purplle_cost.loc[ecom1.actual_zone.isin(['JK', 'Northeast', 'JKUP', 'NortheastUP', 'JKROS', 'NortheastROS'])] = 38 + ((ecom1.box_max_weight.loc[ecom1.actual_zone.isin(['JK', 'Northeast', 'JKUP', 'NortheastUP', 'JKROS', 'NortheastROS'])]-500)/500) * 32
            # for DS
            tmp_cost = ecom1[~ecom1.actual_zone.isna()].purplle_cost.mean()
            ecom1.purplle_cost.loc[ecom1.actual_zone.isna()] = tmp_cost

            ecom1.purplle_cost.loc[(ecom1.method=='cod') & (ecom1.shipment_status != 'RTO')] =  ecom1.purplle_cost.loc[(ecom1.method=='cod') & (ecom1.shipment_status != 'RTO')] * 1.015
            ecom1.purplle_cost.loc[ecom1.shipment_status == 'RTO'] = ecom1.purplle_cost.loc[ecom1.shipment_status == 'RTO']*2

        ecom = pd.concat([ecom1])

    else:
        ecom = None


    # In[ ]:





    # #### XB Costing

    # In[ ]:


    xb.shape, xb.actual_zone.unique()


    # In[ ]:


    if xb.shape[0] != 0:
        xb['purplle_cost'] = 0
        xb1 = xb[xb.dispatch_date.dt.date <= datetime.now().date()]

        if xb1.shape[0]!=0:
            df1 = xb1[xb1.box_max_weight==250]
            df2 = xb1[xb1.box_max_weight!=250]

            # For 250
            df1.purplle_cost.loc[df1.actual_zone == "Local"] = 18
            df1.purplle_cost.loc[df1.actual_zone == "Regional"] = 21
            df1.purplle_cost.loc[df1.actual_zone == "Metro"] = 28
            df1.purplle_cost.loc[df1.actual_zone == "ROI"] = 33
            df1.purplle_cost.loc[df1.actual_zone == "SpecialZone"] = 36
            # for DS
            tmp_cost = df1[~df1.actual_zone.isna()].purplle_cost.mean()
            df1.purplle_cost.loc[df1.actual_zone.isna()] = tmp_cost

            df1.purplle_cost.loc[(df1.method=='cod') & (df1.shipment_status != 'RTO')] =  df1.purplle_cost.loc[(df1.method=='cod') & (df1.shipment_status != 'RTO')] + 24
            df1.purplle_cost.loc[df1.shipment_status == 'RTO'] = df1.purplle_cost.loc[df1.shipment_status == 'RTO']*2


            # For 500+
            df2.purplle_cost.loc[df2.actual_zone == "Local"] = 18 + ((df2.box_max_weight.loc[df2.actual_zone == "Local"]-500)/500)*12
            df2.purplle_cost.loc[df2.actual_zone == "Regional"] = 22 + ((df2.box_max_weight.loc[df2.actual_zone == "Regional"]-500)/500)*18
            df2.purplle_cost.loc[df2.actual_zone == "Metro"] = 28 + ((df2.box_max_weight.loc[df2.actual_zone == "Metro"]-500)/500)*27
            df2.purplle_cost.loc[df2.actual_zone == "ROI"] = 34 + ((df2.box_max_weight.loc[df2.actual_zone == "ROI"]-500)/500)*28
            df2.purplle_cost.loc[df2.actual_zone == "SpecialZone"] = 39 + ((df2.box_max_weight.loc[df2.actual_zone == "SpecialZone"]-500)/500)*32
            # for DS
            tmp_cost = df2[~df2.actual_zone.isna()].purplle_cost.mean()
            df2.purplle_cost.loc[df2.actual_zone.isna()] = tmp_cost

            df2.purplle_cost.loc[(df2.method=='cod') & (df2.shipment_status != 'RTO')] =  df2.purplle_cost.loc[(df2.method=='cod') & (df2.shipment_status != 'RTO')] + 24
            df2.purplle_cost.loc[df2.shipment_status == 'RTO'] = df2.purplle_cost.loc[df2.shipment_status == 'RTO']*2

            xb1 = pd.concat([df1, df2])

        xb = pd.concat([xb1])

    else:
        xb = None


    # In[ ]:





    # In[ ]:





    # In[ ]:





    # #### SFX costing

    # In[ ]:


    sfx.shape, sfx.actual_zone.unique()


    # In[ ]:


    if sfx.shape[0] != 0:
        sfx['purplle_cost'] = 0
        sfx1 = sfx[sfx.dispatch_date.dt.date <= datetime.now().date()]

        if sfx1.shape[0]!=0:
            df1 = sfx1[sfx1.box_max_weight==250]
            df2 = sfx1[sfx1.box_max_weight!=250]

            # For 250
            df1.purplle_cost.loc[df1.actual_zone == "Local"] = 18
            df1.purplle_cost.loc[df1.actual_zone == "Regional"] = 22
            df1.purplle_cost.loc[df1.actual_zone == "Metro"] = 30
            df1.purplle_cost.loc[df1.actual_zone == "ROI"] = 32
            # for DS
            tmp_cost = df1[~df1.actual_zone.isna()].purplle_cost.mean()
            df1.purplle_cost.loc[df1.actual_zone.isna()] = tmp_cost

            df1.purplle_cost.loc[(df1.method=='cod') & (df1.shipment_status != 'RTO')] =  df1.purplle_cost.loc[(df1.method=='cod') & (df1.shipment_status != 'RTO')] + 15
            df1.purplle_cost.loc[df1.shipment_status == 'RTO'] = df1.purplle_cost.loc[df1.shipment_status == 'RTO']*2


            # For 500+
            df2.purplle_cost.loc[df2.actual_zone == "Local"] = 20 + ((df2.box_max_weight.loc[df2.actual_zone == "Local"]-500)/500)*12
            df2.purplle_cost.loc[df2.actual_zone == "Regional"] = 24 + ((df2.box_max_weight.loc[df2.actual_zone == "Regional"]-500)/500)*15
            df2.purplle_cost.loc[df2.actual_zone == "Metro"] = 32 + ((df2.box_max_weight.loc[df2.actual_zone == "Metro"]-500)/500)*18
            df2.purplle_cost.loc[df2.actual_zone == "ROI"] = 34 + ((df2.box_max_weight.loc[df2.actual_zone == "ROI"]-500)/500)*23
            # for DS
            tmp_cost = df2[~df2.actual_zone.isna()].purplle_cost.mean()
            df2.purplle_cost.loc[df2.actual_zone.isna()] = tmp_cost

            df2.purplle_cost.loc[(df2.method=='cod') & (df2.shipment_status != 'RTO')] =  df2.purplle_cost.loc[(df2.method=='cod') & (df2.shipment_status != 'RTO')] + 15
            df2.purplle_cost.loc[df2.shipment_status == 'RTO'] = df2.purplle_cost.loc[df2.shipment_status == 'RTO']*2

            sfx1 = pd.concat([df1, df2])

        sfx = pd.concat([sfx1])

    else:
        sfx = None


    # In[ ]:





    # In[ ]:





    # In[ ]:





    # #### Delivery costing

    # In[ ]:


    delv.shape, delv.actual_zone.unique()


    # In[ ]:


    if delv.shape[0] != 0:
        delv['purplle_cost'] = 0
        delv1 = delv[delv.dispatch_date.dt.date <= datetime.now().date()]

        if delv1.shape[0]!=0:
            delv1.replace(250, 500, inplace=True)
            delv1['tmp_box_max_weight'] = [0 if i<= 3000 else i-3000 for i in delv1.box_max_weight]
            delv1.purplle_cost[delv1.actual_zone=="A"] = 29 + (((delv1.box_max_weight[delv1.actual_zone=="A"]-delv1.tmp_box_max_weight[delv1.actual_zone=="A"])-500)/500)*12 + ((delv1.tmp_box_max_weight[delv1.actual_zone=="A"]+800)//1000)*12
            delv1.purplle_cost[delv1.actual_zone=="B"] = 33 + (((delv1.box_max_weight[delv1.actual_zone=="B"]-delv1.tmp_box_max_weight[delv1.actual_zone=="B"])-500)/500)*20 + ((delv1.tmp_box_max_weight[delv1.actual_zone=="B"]+800)//1000)*16
            delv1.purplle_cost[delv1.actual_zone=="C1"] = 35 + (((delv1.box_max_weight[delv1.actual_zone=="C1"]-delv1.tmp_box_max_weight[delv1.actual_zone=="C1"])-500)/500)*22 + ((delv1.tmp_box_max_weight[delv1.actual_zone=="C1"]+800)//1000)*23
            delv1.purplle_cost[delv1.actual_zone=="C2"] = 37 + (((delv1.box_max_weight[delv1.actual_zone=="C2"]-delv1.tmp_box_max_weight[delv1.actual_zone=="C2"])-500)/500)*24 + ((delv1.tmp_box_max_weight[delv1.actual_zone=="C2"]+800)//1000)*26
            delv1.purplle_cost[delv1.actual_zone=="D1"] = 36 + (((delv1.box_max_weight[delv1.actual_zone=="D1"]-delv1.tmp_box_max_weight[delv1.actual_zone=="D1"])-500)/500)*23 + ((delv1.tmp_box_max_weight[delv1.actual_zone=="D1"]+800)//1000)*25
            delv1.purplle_cost[delv1.actual_zone=="D2"] = 38 + (((delv1.box_max_weight[delv1.actual_zone=="D2"]-delv1.tmp_box_max_weight[delv1.actual_zone=="D2"])-500)/500)*26 + ((delv1.tmp_box_max_weight[delv1.actual_zone=="D2"]+800)//1000)*27
            delv1.purplle_cost[delv1.actual_zone=="E"] = 45 + (((delv1.box_max_weight[delv1.actual_zone=="E"]-delv1.tmp_box_max_weight[delv1.actual_zone=="E"])-500)/500)*33 + ((delv1.tmp_box_max_weight[delv1.actual_zone=="E"]+800)//1000)*34
            # for DS
            tmp_cost = delv1[~delv1.actual_zone.isna()].purplle_cost.mean()
            delv1.purplle_cost.loc[delv1.actual_zone.isna()] = tmp_cost

            delv1.purplle_cost[(delv1.method=='cod') & (delv1.shipment_status!='RTO')] = delv1.purplle_cost[(delv1.method=='cod') & (delv1.shipment_status!='RTO')]+28
            delv1.purplle_cost[(delv1.shipment_status=='RTO') & (delv1.actual_zone == 'A')] = delv1.purplle_cost[(delv1.shipment_status=='RTO') & (delv1.actual_zone == 'A')] + ((delv1.box_max_weight[(delv1.shipment_status=='RTO') & (delv1.actual_zone == 'A')] + 800)//1000)* 12
            delv1.purplle_cost[(delv1.shipment_status=='RTO') & (delv1.actual_zone == 'B')] = delv1.purplle_cost[(delv1.shipment_status=='RTO') & (delv1.actual_zone == 'B')] + ((delv1.box_max_weight[(delv1.shipment_status=='RTO') & (delv1.actual_zone == 'B')] + 800)//1000)* 16
            delv1.purplle_cost[(delv1.shipment_status=='RTO') & (delv1.actual_zone == 'C1')] = delv1.purplle_cost[(delv1.shipment_status=='RTO') & (delv1.actual_zone == 'C1')] + ((delv1.box_max_weight[(delv1.shipment_status=='RTO') & (delv1.actual_zone == 'C1')] + 800)//1000)* 23
            delv1.purplle_cost[(delv1.shipment_status=='RTO') & (delv1.actual_zone == 'C2')] = delv1.purplle_cost[(delv1.shipment_status=='RTO') & (delv1.actual_zone == 'C2')] + ((delv1.box_max_weight[(delv1.shipment_status=='RTO') & (delv1.actual_zone == 'C2')] + 800)//1000)* 26
            delv1.purplle_cost[(delv1.shipment_status=='RTO') & (delv1.actual_zone == 'D1')] = delv1.purplle_cost[(delv1.shipment_status=='RTO') & (delv1.actual_zone == 'D1')] + ((delv1.box_max_weight[(delv1.shipment_status=='RTO') & (delv1.actual_zone == 'D1')] + 800)//1000)* 25
            delv1.purplle_cost[(delv1.shipment_status=='RTO') & (delv1.actual_zone == 'D2')] = delv1.purplle_cost[(delv1.shipment_status=='RTO') & (delv1.actual_zone == 'D2')] + ((delv1.box_max_weight[(delv1.shipment_status=='RTO') & (delv1.actual_zone == 'D2')] + 800)//1000)* 27
            delv1.purplle_cost[(delv1.shipment_status=='RTO') & (delv1.actual_zone == 'E')] = delv1.purplle_cost[(delv1.shipment_status=='RTO') & (delv1.actual_zone == 'E')] + ((delv1.box_max_weight[(delv1.shipment_status=='RTO') & (delv1.actual_zone == 'E')] + 800)//1000)* 34

            delv1.drop('tmp_box_max_weight', axis=1, inplace=True)
        delv = pd.concat([delv1]) 
    else:
        delv = None


    # In[ ]:


    #delv[(delv.actual_zone=='B') & (delv.tmp_box_max_weight==1500)][['shipment_status', 'method', 'box_max_weight', 'actual_zone', 'zone', 'purplle_cost']]


    # In[ ]:





    # In[ ]:





    # #### SFX Express costing

    # In[ ]:


    sfxExp.shape, sfxExp.actual_zone.unique()


    # In[ ]:


    if sfxExp.shape[0] != 0:
        sfxExp['purplle_cost'] = 0
        sfxExp1 = sfxExp[sfxExp.dispatch_date.dt.date <= datetime.now().date()]

        if sfxExp1.shape[0]!=0:
            sfxExp1.replace(250, 500, inplace=True)
            sfxExp1.purplle_cost = 41 + ((sfxExp1.box_max_weight-500)/500) * 26

            sfxExp1.purplle_cost.loc[(sfxExp1.method=='cod') & (sfxExp1.shipment_status != 'RTO')] =  sfxExp1.purplle_cost.loc[(sfxExp1.method=='cod') & (sfxExp1.shipment_status != 'RTO')] + 15
            sfxExp1.purplle_cost.loc[sfxExp1.shipment_status == 'RTO'] = sfxExp1.purplle_cost.loc[sfxExp1.shipment_status == 'RTO']*2

        sfxExp = pd.concat([sfxExp1])

    else:
        sfxExp = None


    # In[ ]:





    # In[ ]:





    # #### Kerry Indev costing

    # In[ ]:


    ke.shape, ke.actual_zone.unique()


    # In[ ]:


    if ke.shape[0] != 0:
        ke['purplle_cost'] = 0
        ke1 = ke[ke.dispatch_date.dt.date <= datetime.now().date()]

        if ke1.shape[0]!=0:
            ke1.replace(250, 500, inplace=True)
            ke1.purplle_cost = 39 + ((ke1.box_max_weight-500)/500) * 39
            ke1.purplle_cost.loc[(ke1.method=='cod') & (ke1.shipment_status != 'RTO')] =  ke1.purplle_cost.loc[(ke1.method=='cod') & (ke1.shipment_status != 'RTO')] + 15
            ke1.purplle_cost.loc[ke1.shipment_status == 'RTO'] = ke1.purplle_cost.loc[ke1.shipment_status == 'RTO']*2

        ke = pd.concat([ke1])

    else:
        ke = None



    # In[ ]:





    # In[ ]:





    # In[ ]:





    # #### BD plus costing

    # In[ ]:


    bdp.shape, bdp.actual_zone.unique()


    # In[ ]:


    if bdp.shape[0] != 0:
        bdp['purplle_cost'] = 0
        bdp1 = bdp[bdp.dispatch_date.dt.date <= datetime.now().date()]

        if bdp1.shape[0]!=0:
            bdp1.replace(250, 500, inplace=True)
            bdp1.purplle_cost.loc[bdp1.actual_zone.isin(['Local', 'LocalRAS'])] = 21 + ((bdp1.box_max_weight.loc[bdp1.actual_zone.isin(['Local', 'LocalRAS'])]-500)/500) * 20
            bdp1.purplle_cost.loc[bdp1.actual_zone.isin(['Regional', 'RegionalRAS'])] = 21 + ((bdp1.box_max_weight.loc[bdp1.actual_zone.isin(['Regional', 'RegionalRAS'])]-500)/500) * 20
            bdp1.purplle_cost.loc[bdp1.actual_zone.isin(['Metro', 'MetroRAS'])] = 25.2 + ((bdp1.box_max_weight.loc[bdp1.actual_zone.isin(['Metro', 'MetroRAS'])]-500)/500) * 24
            bdp1.purplle_cost.loc[bdp1.actual_zone.isin(['ROI', 'ROIRAS'])] = 31.5 + ((bdp1.box_max_weight.loc[bdp1.actual_zone.isin(['ROI', 'ROIRAS'])]-500)/500) * 30
            bdp1.purplle_cost.loc[bdp1.actual_zone.isin(['Special', 'SpecialRAS'])] = 36.75 + ((bdp1.box_max_weight.loc[bdp1.actual_zone.isin(['Special', 'SpecialRAS'])]-500)/500) * 35
            # for DS
            tmp_cost = bdp1[~bdp1.actual_zone.isna()].purplle_cost.mean()
            bdp1.purplle_cost.loc[bdp1.actual_zone.isna()] = tmp_cost

            # other charges - FSC, IDC
            bdp1.purplle_cost.loc[bdp1.actual_zone.isin(['Metro', 'MetroRAS', 'ROI', 'ROIRAS', 'Special', 'SpecialRAS'])] *= 1.32

            bdp1.purplle_cost.loc[(bdp1.method=='cod') & (bdp1.shipment_status != 'RTO')] =  bdp1.purplle_cost.loc[(bdp1.method=='cod') & (bdp1.shipment_status != 'RTO')] * 1.015
            bdp1.purplle_cost.loc[bdp1.shipment_status == 'RTO'] = bdp1.purplle_cost.loc[bdp1.shipment_status == 'RTO']*2

        bdp = pd.concat([bdp1])

    else:
        bdp = None


    # In[ ]:





    # In[ ]:





    # #### BD Air costing

    # In[ ]:


    bda.shape, bda.actual_zone.unique()


    # In[ ]:


    if bda.shape[0] != 0:
        bda['purplle_cost'] = 0
        bda1 = bda[bda.dispatch_date.dt.date <= datetime.now().date()]

        if bda1.shape[0]!=0:
            bda1.replace(250, 500, inplace=True)
            bda1.purplle_cost.loc[bda1.actual_zone.isin(['Local', 'LocalRAS'])] = 23 + ((bda1.box_max_weight.loc[bda1.actual_zone.isin(['Local', 'LocalRAS'])]-500)/500) * 22
            bda1.purplle_cost.loc[bda1.actual_zone.isin(['Regional', 'RegionalRAS'])] = 27 + ((bda1.box_max_weight.loc[bda1.actual_zone.isin(['Regional', 'RegionalRAS'])]-500)/500) * 25
            bda1.purplle_cost.loc[bda1.actual_zone.isin(['Metro', 'MetroRAS'])] = 27 + ((bda1.box_max_weight.loc[bda1.actual_zone.isin(['Metro', 'MetroRAS'])]-500)/500) * 27
            bda1.purplle_cost.loc[bda1.actual_zone.isin(['ROI', 'ROIRAS'])] = 31 + ((bda1.box_max_weight.loc[bda1.actual_zone.isin(['ROI', 'ROIRAS'])]-500)/500) * 31
            bda1.purplle_cost.loc[bda1.actual_zone.isin(['East', 'EastRAS'])] = 34 + ((bda1.box_max_weight.loc[bda1.actual_zone.isin(['East', 'EastRAS'])]-500)/500) * 34
            bda1.purplle_cost.loc[bda1.actual_zone.isin(['Special', 'SpecialRAS'])] = 50 + ((bda1.box_max_weight.loc[bda1.actual_zone.isin(['Special', 'SpecialRAS'])]-500)/500) * 47
            # for DS
            tmp_cost = bda1[~bda1.actual_zone.isna()].purplle_cost.mean()
            bda1.purplle_cost.loc[bda1.actual_zone.isna()] = tmp_cost

            # other charges - FSC, IDC
            bda1.purplle_cost *= 1.19

            bda1.purplle_cost.loc[(bda1.method=='cod') & (bda1.shipment_status != 'RTO')] =  bda1.purplle_cost.loc[(bda1.method=='cod') & (bda1.shipment_status != 'RTO')] + 20
            bda1.purplle_cost.loc[bda1.shipment_status == 'RTO'] = bda1.purplle_cost.loc[bda1.shipment_status == 'RTO']*2

            # Adding RAS charge
            bda1.purplle_cost.loc[bda1.actual_zone.isin(['LocalRAS', 'RegionalRAS', 'MetroRAS', 'ROIRAS', 'EastRAS', 'SpecialRAS'])] += 5

        bda = pd.concat([bda1])

    else:
        bda = None


    # In[ ]:





    # #### Online Express Costing

    # In[ ]:


    on.shape, on.actual_zone.unique()


    # In[ ]:


    if on.shape[0] != 0:
        on['purplle_cost'] = 0
        on1 = on[on.dispatch_date.dt.date <= datetime.now().date()]

        if on1.shape[0]!=0:
            df1 = on1[on1.box_max_weight==250]
            df2 = on1[on1.box_max_weight!=250]

            # For 250
            df1.purplle_cost.loc[df1.actual_zone.isin(["Local"])] = 16
            df1.purplle_cost.loc[df1.actual_zone.isin(["Regional"])] = 18
            # for DS
            tmp_cost = df1[~df1.actual_zone.isna()].purplle_cost.mean()
            df1.purplle_cost.loc[df1.actual_zone.isna()] = tmp_cost

            df1.purplle_cost.loc[(df1.method=='cod') & (df1.shipment_status != 'RTO')] =  df1.purplle_cost.loc[(df1.method=='cod') & (df1.shipment_status != 'RTO')] + 12
            df1.purplle_cost.loc[df1.shipment_status == 'RTO'] = df1.purplle_cost.loc[df1.shipment_status == 'RTO']*2


            # For 500+
            df2.purplle_cost.loc[df2.actual_zone.isin(["Local"])] = 16 + ((df2.box_max_weight.loc[df2.actual_zone.isin(["Local"])]-500)/500)*9
            df2.purplle_cost.loc[df2.actual_zone.isin(["Regional"])] = 17 + ((df2.box_max_weight.loc[df2.actual_zone.isin(["Regional"])]-500)/500)*12
            # for DS
            tmp_cost = df2[~df2.actual_zone.isna()].purplle_cost.mean()
            df2.purplle_cost.loc[df2.actual_zone.isna()] = tmp_cost

            df2.purplle_cost.loc[(df2.method=='cod') & (df2.shipment_status != 'RTO')] =  df2.purplle_cost.loc[(df2.method=='cod') & (df2.shipment_status != 'RTO')] + 12
            df2.purplle_cost.loc[df2.shipment_status == 'RTO'] = df2.purplle_cost.loc[df2.shipment_status == 'RTO']*2

            on1 = pd.concat([df1, df2])

        on = pd.concat([on1])

    else:
        on = None


    # In[ ]:





    # In[ ]:





    # #### Amaze Solution Costing

    # In[ ]:


    am.shape, am.actual_zone.unique()


    # In[ ]:


    if am.shape[0] != 0:
        am['purplle_cost'] = 0
        am1 = am[am.dispatch_date.dt.date <= datetime.now().date()]

        if am1.shape[0]!=0:
            df1 = am1[am1.box_max_weight==250]
            df2 = am1[am1.box_max_weight!=250]

            # For 250
            df1.purplle_cost.loc[df1.actual_zone.isin(["Local"])] = 15
            df1.purplle_cost.loc[df1.actual_zone.isin(["Regional"])] = 17
            df1.purplle_cost.loc[df1.actual_zone.isin(["Metro"])] = 25
            df1.purplle_cost.loc[df1.actual_zone.isin(["ROI"])] = 30
            df1.purplle_cost.loc[df1.actual_zone.isin(['Special', 'SpecialZone'])] = 32
            # for DS
            tmp_cost = df1[~df1.actual_zone.isna()].purplle_cost.mean()
            df1.purplle_cost.loc[df1.actual_zone.isna()] = tmp_cost

            df1.purplle_cost.loc[(df1.method=='cod') & (df1.shipment_status != 'RTO')] =  df1.purplle_cost.loc[(df1.method=='cod') & (df1.shipment_status != 'RTO')] + 13
            df1.purplle_cost.loc[df1.shipment_status == 'RTO'] = df1.purplle_cost.loc[df1.shipment_status == 'RTO']*2


            # For 500+
            df2.purplle_cost.loc[df2.actual_zone.isin(["Local"])] = 15 + ((df2.box_max_weight.loc[df2.actual_zone.isin(["Local"])]-500)/500)*9
            df2.purplle_cost.loc[df2.actual_zone.isin(["Regional"])] = 18 + ((df2.box_max_weight.loc[df2.actual_zone.isin(["Regional"])]-500)/500)*11
            df2.purplle_cost.loc[df2.actual_zone.isin(["Metro"])] = 26 + ((df2.box_max_weight.loc[df2.actual_zone.isin(["Metro"])]-500)/500)*25
            df2.purplle_cost.loc[df2.actual_zone.isin(["ROI"])] = 31 + ((df2.box_max_weight.loc[df2.actual_zone.isin(["ROI"])]-500)/500)*27
            df2.purplle_cost.loc[df2.actual_zone.isin(['Special', 'SpecialZone'])] = 34 + ((df2.box_max_weight.loc[df2.actual_zone.isin(['Special', 'SpecialZone'])]-500)/500)*29
            # for DS
            tmp_cost = df2[~df2.actual_zone.isna()].purplle_cost.mean()
            df2.purplle_cost.loc[df2.actual_zone.isna()] = tmp_cost

            df2.purplle_cost.loc[(df2.method=='cod') & (df2.shipment_status != 'RTO')] =  df2.purplle_cost.loc[(df2.method=='cod') & (df2.shipment_status != 'RTO')] + 13
            df2.purplle_cost.loc[df2.shipment_status == 'RTO'] = df2.purplle_cost.loc[df2.shipment_status == 'RTO']*2

            am1 = pd.concat([df1, df2])

        am = pd.concat([am1])

    else:
        am = None


    # In[ ]:





    # In[ ]:





    # #### --- Cost zero testing code

    # In[ ]:


    # testing = delv


    # In[ ]:


    # # Testing 0 cost to zone 
    # t = testing[~testing.actual_zone.isna()]
    # t[t.purplle_cost==0].actual_zone.unique()


    # In[ ]:


    # # Testing 0 cost in entire data
    # testing[testing.purplle_cost==0].actual_zone.unique()


    # In[ ]:


    # # testing null entries
    # testing.purplle_cost.isna().sum()


    # In[ ]:


    # sorted(testing.purplle_cost.unique())[:10]


    # In[ ]:


    # testing[testing.purplle_cost==0].actual_zone.unique()


    # # Fianl DF

    # In[ ]:


    other_cp['purplle_cost']=0
    final_df = pd.concat([ecom, xb, sfx, delv, sfxExp, bdp, bda, on, am, ke, other_cp])


    # In[ ]:


    avg_cps = final_df[final_df.carrier_id.isin(calculated_cp)].purplle_cost.mean()
    final_df.purplle_cost[~final_df.carrier_id.isin(calculated_cp)] = avg_cps
    final_df['is_cps_calculated'] = 1
    final_df.is_cps_calculated[~final_df.carrier_id.isin(calculated_cp)] = 0


    # In[ ]:





    # In[ ]:


    final_df.purplle_cost = round(final_df.purplle_cost,3)
    final_df.tat_o2s = final_df.tat_o2s.astype(float)
    final_df.tat_s2d = final_df.tat_s2d.astype(float)
    final_df.tat_o2d = final_df.tat_o2d.astype(float)
    final_df.tat_wh_o2s = final_df.tat_wh_o2s.astype(float)
    final_df.tat_wh_s2d = final_df.tat_wh_s2d.astype(float)


    # In[ ]:


    for i in [ecom, xb, sfx, delv, sfxExp, bdp, bda, on, am, ke, other_cp]:
        print(i.shape)


    # In[ ]:


    sum(final_df.purplle_cost==0)


    # In[ ]:


    #final_df.to_csv('data.csv', index=False)


    # #### Append external zone mapping

    # In[ ]:


    tmp_df = pd.read_excel('/home/ubuntu/purplle_data_science/marketing/dev/Nishant/CPS_dashboard/zone state maping.xlsx')


    # In[ ]:


    final_df = pd.merge(left=final_df, right=tmp_df[['pincode', 'zone3']], how='left', on='pincode')


    # In[ ]:


    final_df.carrier_name.unique()


    # In[ ]:





    # # Removing Old CPS data

    # In[ ]:


    # from sqlalchemy import create_engine
    # gcp_db_analytics_engine = create_engine('mysql+pymysql://', echo=False)

    # start = datetime.datetime.now()
    # if datetime.datetime.now().day not in [15, 30]:
    #     gcp_db_analytics_engine.execute("DELETE FROM db_analytics.old_cps_dashboard WHERE date(shipping_date) >= date_sub(current_date(), interval 90 day);")
    # else:
    #     gcp_db_analytics_engine.execute("DELETE FROM db_analytics.old_cps_dashboard WHERE date(shipping_date) >= date_sub(current_date(), interval 120 day);")

    # print("gcp old data is removed in: ", datetime.datetime.now()-start)


    # In[ ]:





    # # writing into BigQuery

    # In[ ]:


    x = datetime.now()
    print(x)

    final_df.to_gbq('datos_studios.nm_cps_data', if_exists='replace')
    print("Total time to write data into BigQuery is: ", datetime.now() - x)


    # In[ ]:


    # final_df = df
    # tmp = final_df[(final_df.dispatch_date.dt.date >= pd.to_datetime('2021-01-01')) & (final_df.dispatch_date.dt.date <= datetime.now().date())]

except Exception as e:
    import sys,os
    sys.path.append('/home/ubuntu/purplle_data_science/marketing/dev/Nishant')
    import mailer
    
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    #print(exc_type, fname, exc_tb.tb_lineno)
    
    body = 'CPS code fata'+str(e) + 'Error name: '+ str(exc_type)+ ' at line no:' +str(exc_tb.tb_lineno)
    mailer.send_mail(receiver=['nishant.m@purplle.com'], subject='CPS code fata', body=body)
