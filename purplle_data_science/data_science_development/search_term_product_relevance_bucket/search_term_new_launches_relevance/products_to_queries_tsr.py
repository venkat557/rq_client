#!/usr/bin/env python
# coding: utf-8

# In[1]:



# importing required libraries
print("importing required libraries")
import pandas as pd
import numpy as np
import time
import sys,os
sys.path.append(os.environ['DATA_SCIENCE_HOME'])
import helper.mysql.get_connection  as SQL
from datetime import datetime, timedelta
# import helper.lib.utilities as UT
import re
import time
from fuzzywuzzy import fuzz
import common_stprb as com


## improt packages to do operations with bigquery client
print("importing bigquery packages")
from google.cloud import bigquery
from google.oauth2 import service_account

# In[80]:


def import_data_in_chunks(df_data,max_rows,table_name,db):
    total_count=df_data.shape[0]
    total_splits=int(total_count/max_rows)+1
    df_splits = np.array_split(df_data, total_splits)
    result=False
    for df_chunk in df_splits :
        print("importing chunk:",df_chunk.shape)
        SQL.insert_dataframe_to_db(df_chunk,db,table_name)
        result=True
    return result


# In[74]:


print("getting old tagging......")
df_old_tagging=com.get_old_tagging()

df_dev_queries=com.get_dev_queries_from_search_term_rank_table()
df_dev_queries=df_dev_queries.drop_duplicates()


df_newly_added_products=com.get_newly_added_products_for_last_n_days(31)
df_newly_added_products_added_date=df_newly_added_products[['product_id','date_added']].rename(columns={'date_added':'launch_date'}).drop_duplicates()
df_newly_added_products=df_newly_added_products[['product_id','product_name']].drop_duplicates()

df_st_topics=com.get_search_term_topics_info()
df_osq_msq=df_st_topics[['original_search_query','modified_search_query']].drop_duplicates()
del df_st_topics['original_search_query']
df_st_topics=df_st_topics.rename(columns={'modified_search_query':'original_search_query'})
df_st_topics['topic_num_of_evs_in_each_eg']=df_st_topics.groupby(['original_search_query','topic_entity_group'])['topic_entity_value'].transform('nunique')
df_st_topics['modified_entity_group']=np.where((df_st_topics['topic_entity_group'].isin(['brand','category'])),df_st_topics['topic_entity_group'],'entity')
df_st_topics=df_st_topics.drop_duplicates()


# In[ ]:


print("reading data from search_term_high_relevant_new_launches_raw_data")
query_existed_prds_hr_topic_update="""
select original_search_query,modified_search_query,product_id,token_set_ratio,launch_date
from search_term_high_relevant_new_launches_raw_data
"""
df_existed_prds_hr_topic_update=SQL.fetch_data_with_schema(query_existed_prds_hr_topic_update,"warehouse")
if df_existed_prds_hr_topic_update.empty:
    existed_prds_list=[]
else:
    df_existed_prds_hr_topic_update=df_existed_prds_hr_topic_update.drop_duplicates()
    df_existed_prds_hr_topic_update['token_set_ratio']=df_existed_prds_hr_topic_update['token_set_ratio'].astype(int)
    existed_prds_list=list(df_existed_prds_hr_topic_update['product_id'].unique())


# In[ ]:


df_prds_not_having_tsr=df_newly_added_products.query("product_id not in @existed_prds_list").copy()
df_prds_not_having_tsr=df_prds_not_having_tsr.drop_duplicates()

print("newly active prds....",df_prds_not_having_tsr.shape)


# In[75]:


print("search terms ..........",df_dev_queries.shape)
print("search term topics info .....",df_st_topics.shape)
print("oringal_search_term_and_modified_search_term_df...",df_osq_msq.shape)
print("search terms highly relevant new launches raw data.....",df_existed_prds_hr_topic_update.shape)
print("products not having token set ratio...... ",df_prds_not_having_tsr.shape)


# In[81]:


def get_token_set_ratio(df_input_data,df_dev_queries,df_osq_msq):
    df_dev_queries['join_col']=1
    df_input_data['join_col']=1

    df_input_data=pd.merge(df_input_data,df_dev_queries,on=['join_col'])
    df_input_data=pd.merge(df_input_data,df_osq_msq,on=['original_search_query'],how='left')
    df_input_data['modified_search_query']=np.where(df_input_data['modified_search_query'].isnull(),df_input_data['original_search_query'],df_input_data['modified_search_query'])
    print("calculating token set ratio")
    df_input_data['token_set_ratio']=df_input_data.apply(lambda x:fuzz.token_set_ratio(x['modified_search_query'],x['product_name']),axis=1)
    return df_input_data

def get_token_Set_ratio_in_chunks(df_data,df_dev_queries,df_osq_msq,max_rows):
    # SQL.update_data("truncate table tsr_for_new_products_and_search_terms","","warehouse")

    credentials = service_account.Credentials.from_service_account_file('/purplle_data_science/credentials/DataPipelineProduction-8521e6e33aff.json')
    client = bigquery.Client(credentials= credentials,project='datapipelineproduction')
    sql = """TRUNCATE TABLE datos_pocos."""+"tsr_for_new_products_and_search_terms"
    df = client.query(sql)

    total_count=df_data.shape[0]
    total_splits=int(total_count/max_rows)+1
    df_splits = np.array_split(df_data, total_splits)
    result=False
    for df_chunk in df_splits :
        print("applying algo for chunk:",df_chunk.shape)
        df_tsr_chunk=get_token_set_ratio(df_chunk,df_dev_queries,df_osq_msq)
        for each_column in ['product_name','join_col']:
            del df_tsr_chunk[each_column]
        print("pushing data of token_set_ratio of  newly launches products and search terms ....")
        # import_data_in_chunks(df_tsr_chunk,100000,"tsr_for_new_products_and_search_terms","warehouse")
        # print("pushing chunk into :"+db+" in table : "+table_name+".....", str(count)+'/'+str(total_splits))
        df_tsr_chunk.to_gbq('datos_pocos.tsr_for_new_products_and_search_terms', project_id="datapipelineproduction",if_exists='append')

    return True


# In[ ]:





# In[70]:


if df_prds_not_having_tsr.empty:
    df_tsr_for_prds_not_having_tsr=pd.DataFrame()
else:
    df_tsr_for_prds_not_having_tsr=get_token_Set_ratio_in_chunks(df_prds_not_having_tsr.copy(),df_dev_queries.copy(),df_osq_msq.copy(),10)


# In[68]:





# In[ ]:





# In[ ]:





# In[ ]:




