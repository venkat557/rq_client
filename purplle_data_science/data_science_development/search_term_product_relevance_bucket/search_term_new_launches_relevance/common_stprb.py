#!/usr/bin/env python
# coding: utf-8

# In[11]:


print("importing required libraries in stprb")
import pandas as pd
import numpy as np
import time
import sys,os
sys.path.append(os.environ['DATA_SCIENCE_HOME'])
import helper.mysql.get_connection  as SQL
from datetime import datetime, timedelta
# import helper.lib.utilities as UT
import re
import time
from fuzzywuzzy import fuzz


# In[7]:


def get_old_tagging():
    product_ids=''
    query_old_tagging="""
    select module_id as product_id,ev.id as entity_value_id,lower(ev.entity_value) as entity_value,ev.entity_group_id as entity_group_id,lower(eg.name) as entity_group
    from entity_products as ep
    join  entity_values ev on ev.id=ep.entity_value_id
    join entity_groups eg on ev.entity_group_id=eg.id
    where module='product'
    """
    df_old_tagging=SQL.fetch_data_with_schema(query_old_tagging,"slave")
    
    query_cat="""
    select product_id,category_id as entity_value_id,lower(category_name) as entity_value,-2 as entity_group_id,'category' as entity_group
    from product_master
    group by product_id,category_id
    """
    df_cat=SQL.fetch_data_with_schema(query_cat,"warehouse")

    query_brand="""
    select product_id,brand_id as entity_value_id,lower(brand_name) as entity_value,-3 as entity_group_id,'brand' as entity_group
    from product_master
    group by product_id,brand_id
    """
    df_brand=SQL.fetch_data_with_schema(query_brand,"warehouse")
    
    query_aliases="""
    select ep.module_id as product_id,ev.id as entity_value_id,lower(ea.alias_name) as entity_value,ev.entity_group_id as entity_group_id,lower(eg.name) as entity_group
    from entity_products as ep
    join  entity_values ev on ev.id=ep.entity_value_id
    join entity_groups eg on ev.entity_group_id=eg.id
    join entity_aliases ea on ea.module_id=ev.id and ea.module='entity_value'
    where ep.module='product'
    """
    df_aliases=SQL.fetch_data_with_schema(query_aliases,"slave")
    
    df_old_tagging=df_old_tagging.append([df_cat,df_brand,df_aliases],ignore_index=True)
    df_old_tagging['entity_value']=df_old_tagging['entity_value'].str.lower()
    #df_old_tagging['entity_value']=df_old_tagging['entity_value'].str.replace('&','and')
    df_old_tagging=df_old_tagging[~df_old_tagging.entity_value.isnull()]
    df_old_tagging.columns="product_"+df_old_tagging.columns
    df_old_tagging.rename(columns={'product_product_id':'product_id'},inplace=True)
    df_old_tagging=df_old_tagging.drop_duplicates()
    return df_old_tagging


# In[8]:


def get_dev_queries_from_search_term_rank_table():
    query_dev_queries="""
    select lower(keyword) as original_search_query
    from datos_pocos.search_term_wise_product_ranking
    group by keyword
    """
    df_dev_queries=SQL.fetch_data_with_schema(query_dev_queries,"bigquery")
    return df_dev_queries


# In[9]:


def get_search_term_topics_info():
    print("getting search term and topics......")
    query_st_topics="""
    SELECT modified_search_query,original_search_query,entity_value as topic_entity_value,entity_group as topic_entity_group
    FROM `topic_identification_of_query_raw_data`
    where orders_contrib>0
    group by original_search_query,entity_value,entity_group 
    """
    df_st_topics=SQL.fetch_data_with_schema(query_st_topics,"warehouse")
    return df_st_topics


# In[1]:


def get_newly_added_products_for_last_n_days(num_of_previous_days):
    #here we are giving only newly active products
    query_newly_added_products="""
    select pp.id as product_id,lower(name) as product_name,date_added
    from product_product pp 
    where date_added between unix_timestamp(CURRENT_DATE())- {} *86400 and unix_timestamp(CURRENT_DATE()) 
    group by pp.id
    """.format(num_of_previous_days)
    df_newly_added_products=SQL.fetch_data_with_schema(query_newly_added_products,"slave")
    

    query_newly_active_prds="""
    SELECT pl1.product_id,pp.name as product_name,pl1.time_stamp as date_added
    FROM product_logchange AS pl1
    join product_product pp on pl1.product_id=pp.id
    WHERE pl1.change = "ACTIVE"  and pl1.new_value = 1 and pl1.time_stamp>= (select UNIX_TIMESTAMP(CURRENT_TIMESTAMP())-{}*86400)
    and pl1.id = (
    SELECT min(pl2.id)
    FROM product_logchange AS pl2
    WHERE  pl2.product_id = pl1.product_id
    and pl2.change = "ACTIVE"  and pl2.new_value = 1 )
    """.format(num_of_previous_days)
    df_newly_active_prds=SQL.fetch_data_with_schema(query_newly_active_prds,'slave')

    newly_active_prds=list(df_newly_active_prds.product_id.unique())
    df_newly_added_products=df_newly_added_products.query("product_id not in @newly_active_prds").copy()
    df_new_products=df_newly_active_prds.append(df_newly_added_products,ignore_index=True,sort=False)
    return df_newly_active_prds


# In[ ]:


def get_HR_buckets_with_updated_topics(df_osq_msq,df_st_topics):
    import time
    print("reading data from search_term_high_relevant_new_launches_raw_data")
    query_existed_prds_hr_topic_update="""
    select original_search_query,modified_search_query,product_id,token_set_ratio,launch_date
    from search_term_high_relevant_new_launches_raw_data
    """
    df_existed_prds_hr_topic_update=SQL.fetch_data_with_schema(query_existed_prds_hr_topic_update,"warehouse")
    df_existed_prds_hr_topic_update=df_existed_prds_hr_topic_update.drop_duplicates()
    df_existed_prds_hr_topic_update['token_set_ratio']=df_existed_prds_hr_topic_update['token_set_ratio'].astype(int)
    
    df_st_all_prds=df_existed_prds_hr_topic_update[['modified_search_query','product_id','token_set_ratio']].drop_duplicates().copy()
    df_st_all_prds=df_st_all_prds.rename(columns={'modified_search_query':'original_search_query'})
    df_st_prds_tsr_100=df_st_all_prds.query("token_set_ratio==100").copy()
    df_st_prds_tsr_100['relevance_bucket']='HR'

    #df_st_prds_tsr_not_100=df_st_all_prds.query("token_set_ratio<100").copy()
    df_st_prds_tsr_not_100=df_st_all_prds.copy()
    print("claculating matched_num_of_evs_in_each_eg.....")
    df_test1=pd.merge(df_st_prds_tsr_not_100,df_old_tagging,on=['product_id'])

    df_test2=pd.merge(df_st_topics,df_test1,left_on=['original_search_query','topic_entity_value','topic_entity_group'],right_on=['original_search_query','product_entity_value','product_entity_group'],how='left')
    df_test2['matched_num_of_evs_in_each_eg']=df_test2.groupby(['original_search_query','product_id','product_entity_group'])['product_entity_value'].transform('nunique')
    df_test3=df_test2[['original_search_query','product_id','topic_entity_group','product_entity_group','topic_num_of_evs_in_each_eg','matched_num_of_evs_in_each_eg']].drop_duplicates().copy()
    #df_test3['matched_perc_of_evs_in_each_eg']=(df_test3['matched_num_of_evs_in_each_eg']/df_test3['topic_num_of_evs_in_each_eg'])*100
    #df_test3['matched_perc_of_evs_in_each_eg']=np.where(df_test3['product_entity_group'].isin(['brand','category']),100,df_test3['matched_perc_of_evs_in_each_eg'])
    df_test3['modified_entity_group']=np.where((df_test3['topic_entity_group'].isin(['brand','category'])),df_test3['topic_entity_group'],'entity')

    #print("creating search term product pivot.....")
    #df_st_product_pivot=pd.pivot_table(df_test3, values='matched_num_of_evs_in_each_eg', index=['original_search_query', 'product_id'],columns=['modified_entity_group'], aggfunc=np.sum).reset_index().fillna(0)
    #df_st_product_pivot.columns='product_'+df_st_product_pivot.columns
    #df_st_product_pivot=df_st_product_pivot.rename(columns={'product_original_search_query':'original_search_query','product_product_id':'product_id'})


    print("creating search term product pivot.....")
    df_st_product_pivot=pd.pivot_table(df_test3, values='matched_num_of_evs_in_each_eg', index=['original_search_query', 'product_id'],columns=['modified_entity_group'], aggfunc=np.sum).reset_index().fillna(0)
    df_st_product_pivot=pd.DataFrame(df_st_product_pivot.to_records())
    df_st_product_pivot.columns = [hdr.replace("(","").replace(")", "").replace("'","").replace('"',"").replace(",","") for hdr in df_st_product_pivot.columns]
    df_st_product_pivot.columns='product_'+df_st_product_pivot.columns.str.strip()
    df_st_product_pivot=df_st_product_pivot.rename(columns={'product_original_search_query':'original_search_query','product_product_id':'product_id'})
    # df_test3['matched_perc_of_evs_in_each_eg']=df_test3['matched_perc_of_evs_in_each_eg']*df_test3['matched_num_of_evs_in_each_eg']
    # df_test3['sum_matched_perc_of_evs_in_each_eg']=df_test3.groupby(['original_search_query','product_id'])['matched_perc_of_evs_in_each_eg'].transform('sum')
    # df_test3=df_test3.sort_values('sum_matched_perc_of_evs_in_each_eg',ascending=False)

    print("creating topic pivot.....")
    df_topic_pivot=pd.pivot_table(df_st_topics, values='topic_num_of_evs_in_each_eg', index=['original_search_query'],columns=['modified_entity_group'], aggfunc='count').reset_index().fillna(0)
    if 'category' in df_topic_pivot:
        df_topic_pivot['category']=np.where(df_topic_pivot['category']>1,1,df_topic_pivot['category'])
    if 'brand' in df_topic_pivot:
        df_topic_pivot['brand']=np.where(df_topic_pivot['brand']>1,1,df_topic_pivot['brand'])
    df_topic_pivot.columns='topic_'+df_topic_pivot.columns
    df_topic_pivot=df_topic_pivot.rename(columns={'topic_original_search_query':'original_search_query'})

    print("joining product pivot and topic pivot.....")
    df_final_st_prd_topic=pd.merge(df_topic_pivot,df_st_product_pivot,on=['original_search_query'],how='left').fillna(0)
    for each_col in ['topic_brand','topic_category','topic_entity','product_brand','product_category','product_entity']:
        if each_col in df_final_st_prd_topic:
            pass
        else:
            df_final_st_prd_topic[each_col]=0

    print("calculating topic_total_value,product_st_value,topic_product_value_diff,token_set_ratio_diff,final_diff_value,nor_final_diff_value.....")
    df_final_st_prd_topic['topic_total_value']=100*df_final_st_prd_topic['topic_brand']+100*df_final_st_prd_topic['topic_category']+50*df_final_st_prd_topic['topic_entity']
    df_final_st_prd_topic['product_st_value']=100*df_final_st_prd_topic['product_brand']+100*df_final_st_prd_topic['product_category']+50*df_final_st_prd_topic['product_entity']
    df_final_st_prd_topic['topic_product_value_diff']=(df_final_st_prd_topic['topic_total_value']-df_final_st_prd_topic['product_st_value'])/(df_final_st_prd_topic['topic_total_value'])*100
    df_final_st_prd_topic=df_final_st_prd_topic.sort_values('topic_product_value_diff')
    df_final_st_prd_topic['product_id']=df_final_st_prd_topic['product_id'].astype(int)

    df_final_st_prd_topic=pd.merge(df_final_st_prd_topic,df_st_all_prds[['original_search_query','product_id','token_set_ratio']],on=['original_search_query','product_id'],how='left')
    df_final_st_prd_topic['token_set_ratio']=df_final_st_prd_topic['token_set_ratio'].fillna(0)
    df_final_st_prd_topic['token_set_ratio_diff']=100-df_final_st_prd_topic['token_set_ratio']
    df_final_st_prd_topic['final_diff_value']=df_final_st_prd_topic['topic_product_value_diff']+df_final_st_prd_topic['token_set_ratio_diff']

    #df_final_st_prd_topic['final_diff_value_min_group_by_os']=df_final_st_prd_topic.groupby('original_search_query')['final_diff_value'].transform('min')
    df_final_st_prd_topic['final_diff_value_max_group_by_os']=(df_final_st_prd_topic.groupby('original_search_query')['final_diff_value'].transform('max'))
    #df_final_st_prd_topic['min_max_nor_final_diff_value']=(df_final_st_prd_topic['final_diff_value']-df_final_st_prd_topic['final_diff_value_min_group_by_os'])/ (df_final_st_prd_topic['final_diff_value_max_group_by_os']-df_final_st_prd_topic['final_diff_value_min_group_by_os'])
    df_final_st_prd_topic['final_diff_value_max_group_by_os']=np.where(df_final_st_prd_topic['final_diff_value_max_group_by_os']==0,1,df_final_st_prd_topic['final_diff_value_max_group_by_os'])
    df_final_st_prd_topic['nor_final_diff_value']=(df_final_st_prd_topic['final_diff_value'])/ (df_final_st_prd_topic['final_diff_value_max_group_by_os'])*100

    print("labelling HR,FR,TR,NR......")
    df_final_st_prd_topic=df_final_st_prd_topic.query("nor_final_diff_value!='inf' & nor_final_diff_value!='-inf'")


    #df_final_st_prd_topic.loc[df_final_st_prd_topic['nor_final_diff_value']<=10,'relevance_bucket']='HR'
    #df_final_st_prd_topic.loc[(df_final_st_prd_topic['nor_final_diff_value']>10) & (df_final_st_prd_topic['nor_final_diff_value']<=30),'relevance_bucket']='FR'
    #df_final_st_prd_topic.loc[(df_final_st_prd_topic['nor_final_diff_value']>30) & (df_final_st_prd_topic['nor_final_diff_value']<=70),'relevance_bucket']='TR'
    #df_final_st_prd_topic.loc[df_final_st_prd_topic['nor_final_diff_value']>70,'relevance_bucket']='NR'
    df_final_st_prd_topic['relevance_bucket']=np.where( (df_final_st_prd_topic['nor_final_diff_value']<=10) | (df_final_st_prd_topic['token_set_ratio']==100) ,'HR','NOT_HR')


    df_final_st_prd_topic['added_from_token_100']=0
    df_st_prds_tsr_100['added_from_token_100']=1
    df_final_st_prd_topic=df_final_st_prd_topic.append(df_st_prds_tsr_100[['original_search_query','product_id','token_set_ratio','relevance_bucket','added_from_token_100']],ignore_index=True,sort=False)
    df_final_st_prd_topic=df_final_st_prd_topic.sort_values(['original_search_query','product_id','added_from_token_100']).groupby(['original_search_query','product_id']).head(1)
    #df_final_st_prd_topic=pd.merge(df_final_st_prd_topic,df_pm,on=['product_id'],how='left')
    #df_final_st_prd_topic=df_final_st_prd_topic.query("relevance_bucket=='HR'")
    df_final_st_prd_topic=pd.merge(df_final_st_prd_topic.rename(columns={'original_search_query':'modified_search_query'}),df_osq_msq,on=['modified_search_query'],how='left')
    df_final_st_prd_topic['original_search_query']=np.where(df_final_st_prd_topic['original_search_query'].isnull(),df_final_st_prd_topic['modified_search_query'],df_final_st_prd_topic['original_search_query'])
    df_final_st_prd_topic=df_final_st_prd_topic[['original_search_query','modified_search_query','product_id','topic_brand','topic_category','topic_entity','product_brand','product_category','product_entity','topic_total_value','product_st_value','topic_product_value_diff','token_set_ratio','token_set_ratio_diff','final_diff_value','final_diff_value_max_group_by_os','nor_final_diff_value','relevance_bucket']]
    df_final_st_prd_topic=df_final_st_prd_topic.query("product_id>0").copy()

    df_final_st_prd_topic=pd.merge(df_final_st_prd_topic,df_existed_prds_hr_topic_update[['product_id','launch_date']],on=['product_id'],how='left')
    df_final_st_prd_topic['updated_timestamp']=int(time.time())
    return df_final_st_prd_topic
