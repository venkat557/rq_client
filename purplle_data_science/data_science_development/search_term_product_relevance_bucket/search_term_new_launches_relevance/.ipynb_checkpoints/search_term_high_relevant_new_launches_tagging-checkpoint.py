#!/usr/bin/env python
# coding: utf-8

# In[1]:


# importing required libraries
print("importing required libraries")
import pandas as pd
import numpy as np
import time
import sys,os
sys.path.append(os.environ['DATA_SCIENCE_HOME'])
import helper.mysql.get_connection  as SQL
from datetime import datetime, timedelta
import helper.lib.utilities as UT
import re
import time
from fuzzywuzzy import fuzz
import common_stprb as com


# In[2]:


upper_impressions_threshold=500
lower_impressions_threshold=100


# In[3]:


query="""
select original_search_query,modified_search_query,product_id,launch_date 
from search_term_high_relevant_new_launches_raw_data 
where relevance_bucket='HR'
"""
df_sthrnl=SQL.fetch_data_with_schema(query,"warehouse")


# In[4]:


prds_list_for_impressions=",".join(df_sthrnl.product_id.astype(str).unique())

query_st_prd_metrics="""
select dt1.original_search_query,dt1.product_id,dt1.num_of_impressions,dt2.num_of_clicks,(dt2.num_of_clicks/dt1.num_of_impressions)*100 as ctr
from
(select lower(page_type_value) as original_search_query,targetentityid as product_id,count(*) as num_of_impressions
from datos_pocos.listing_impression
where page_type='listing_search'
and event_timestamp between timestamp(DATE_ADD(CURRENT_DATE(), INTERVAL -30 DAY)) and TIMESTAMP(FORMAT_DATE("%Y-%m-%d",CURRENT_DATE()))
and targetentityid in ({})
group by original_search_query,product_id
) as dt1
left join
(
select lower(page_type_value) as original_search_query,targetentityid as product_id,count(*) as num_of_clicks
from datos_pocos.listing_click
where page_type='listing_search'
and event_timestamp between timestamp(DATE_ADD(CURRENT_DATE(), INTERVAL -30 DAY)) and TIMESTAMP(FORMAT_DATE("%Y-%m-%d",CURRENT_DATE()))
and targetentityid in ({})
group by original_search_query,product_id
) as dt2 on dt1.original_search_query=dt2.original_search_query and dt1.product_id=dt2.product_id
order by num_of_clicks desc
""".format(prds_list_for_impressions,prds_list_for_impressions)
df_st_prd_metrics=SQL.fetch_data_with_schema(query_st_prd_metrics,"bigquery")


# In[5]:


query_st_ctr_threshold="""

select dt1.original_search_query,(dt2.num_of_clicks/dt1.num_of_impressions)*100 as required_ctr
from
(select lower(page_type_value) as original_search_query,count(*) as num_of_impressions
from datos_pocos.listing_impression
where page_type='listing_search'
and event_timestamp between timestamp(DATE_ADD(CURRENT_DATE(), INTERVAL -30 DAY)) and TIMESTAMP(FORMAT_DATE("%Y-%m-%d",CURRENT_DATE()))
and item_position<=10
group by original_search_query
) as dt1
left join
(
select lower(page_type_value) as original_search_query,count(*) as num_of_clicks
from datos_pocos.listing_click
where page_type='listing_search'
and event_timestamp between timestamp(DATE_ADD(CURRENT_DATE(), INTERVAL -30 DAY)) and TIMESTAMP(FORMAT_DATE("%Y-%m-%d",CURRENT_DATE()))
and item_position<=10
group by original_search_query
) as dt2 on dt1.original_search_query=dt2.original_search_query
"""
df_st_ctr_threshold=SQL.fetch_data_with_schema(query_st_ctr_threshold,"bigquery")


# In[6]:


df_sthrnl=pd.merge(df_sthrnl,df_st_prd_metrics,on=['original_search_query','product_id'],how='left')
for each_column in ['num_of_impressions','num_of_clicks','ctr']:
    df_sthrnl[each_column]=df_sthrnl[each_column].fillna(0)


# In[7]:


df_sthrnl=pd.merge(df_sthrnl,df_st_ctr_threshold,on=['original_search_query'],how='left')
df_sthrnl['num_of_days_before_launched']=np.ceil((int(time.time())-df_sthrnl['launch_date'])/86400).astype(int)
df_sthrnl=df_sthrnl.drop_duplicates()


# In[8]:


df_send_to_client=df_sthrnl.query("~( (num_of_days_before_launched>30) | (num_of_impressions>@upper_impressions_threshold) | (num_of_impressions>@lower_impressions_threshold & ctr<required_ctr) )").copy()
df_not_send_to_client=df_sthrnl.query("( (num_of_days_before_launched>30) | (num_of_impressions>@upper_impressions_threshold) | (num_of_impressions>@lower_impressions_threshold & ctr<required_ctr) )").copy()

df_send_to_client['send_to_client_flag']=1
df_send_to_client['rank']=df_send_to_client.sort_values(['original_search_query','launch_date','ctr','num_of_clicks','num_of_impressions'],ascending=[True,False,False,False,False]).groupby(['original_search_query','modified_search_query']).cumcount()+1
df_not_send_to_client['send_to_client_flag']=0
df_not_send_to_client['rank']=99999
df_final_result=df_send_to_client.append(df_not_send_to_client,ignore_index=True,sort=False)
df_final_result=df_final_result.drop_duplicates()


# In[9]:


def import_data_in_chunks(df_data,max_rows,table_name,db):
    total_count=df_data.shape[0]
    total_splits=int(total_count/max_rows)+1
    df_splits = np.array_split(df_data, total_splits)
    result=False
    for df_chunk in df_splits :
        print("importing chunk:",df_chunk.shape)
        SQL.insert_dataframe_to_db(df_chunk,db,table_name)
        result=True
    return result


# In[10]:


SQL.update_data("truncate table search_term_high_relevant_new_launches_tagging","","warehouse")
import_data_in_chunks(df_final_result,100000,"search_term_high_relevant_new_launches_tagging","warehouse")

SQL.update_data("truncate table search_term_high_relevant_new_launches_tagging_bc","","warehouse")
import_data_in_chunks(df_final_result,100000,"search_term_high_relevant_new_launches_tagging_bc","warehouse")



