#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# importing required libraries
print("importing required libraries")
import pandas as pd
import numpy as np
import time
import sys,os
sys.path.append(os.environ['DATA_SCIENCE_HOME'])
import helper.mysql.get_connection  as SQL
from datetime import datetime, timedelta
# import helper.lib.utilities as UT
import re
import time
from fuzzywuzzy import fuzz
import common_stprb as com


# In[ ]:


print("getting old tagging......")
df_old_tagging=com.get_old_tagging()

df_dev_queries=com.get_dev_queries_from_search_term_rank_table()
df_dev_queries=df_dev_queries.drop_duplicates()


# In[ ]:


df_newly_added_products=com.get_newly_added_products_for_last_n_days(31)
df_newly_added_products_added_date=df_newly_added_products[['product_id','date_added']].rename(columns={'date_added':'launch_date'}).drop_duplicates()
df_newly_added_products=df_newly_added_products[['product_id','product_name']].drop_duplicates()

df_st_topics=com.get_search_term_topics_info()
df_osq_msq=df_st_topics[['original_search_query','modified_search_query']].drop_duplicates()
del df_st_topics['original_search_query']
df_st_topics=df_st_topics.rename(columns={'modified_search_query':'original_search_query'})
df_st_topics['topic_num_of_evs_in_each_eg']=df_st_topics.groupby(['original_search_query','topic_entity_group'])['topic_entity_value'].transform('nunique')
df_st_topics['modified_entity_group']=np.where((df_st_topics['topic_entity_group'].isin(['brand','category'])),df_st_topics['topic_entity_group'],'entity')
df_st_topics=df_st_topics.drop_duplicates()


# In[ ]:


print("reading data from search_term_high_relevant_new_launches_raw_data")
query_existed_prds_hr_topic_update="""
select original_search_query,modified_search_query,product_id,token_set_ratio,launch_date
from search_term_high_relevant_new_launches_raw_data
"""
df_existed_prds_hr_topic_update=SQL.fetch_data_with_schema(query_existed_prds_hr_topic_update,"warehouse")
if df_existed_prds_hr_topic_update.empty:
    existed_prds_list=[]
else:
    df_existed_prds_hr_topic_update=df_existed_prds_hr_topic_update.drop_duplicates()
    df_existed_prds_hr_topic_update['token_set_ratio']=df_existed_prds_hr_topic_update['token_set_ratio'].astype(int)
    existed_prds_list=list(df_existed_prds_hr_topic_update['product_id'].unique())


# In[ ]:


df_prds_not_having_tsr=df_newly_added_products.query("product_id not in @existed_prds_list").copy()
df_prds_not_having_tsr=df_prds_not_having_tsr.drop_duplicates()

print("newly active prds....",df_prds_not_having_tsr.shape)


# In[ ]:


def get_token_set_ratio(df_input_data,df_dev_queries,df_osq_msq):
    df_dev_queries['join_col']=1
    df_input_data['join_col']=1

    df_input_data=pd.merge(df_input_data,df_dev_queries,on=['join_col'])
    df_input_data=pd.merge(df_input_data,df_osq_msq,on=['original_search_query'],how='left')
    df_input_data['modified_search_query']=np.where(df_input_data['modified_search_query'].isnull(),df_input_data['original_search_query'],df_input_data['modified_search_query'])
    print("calculating token set ratio")
    df_input_data['token_set_ratio']=df_input_data.apply(lambda x:fuzz.token_set_ratio(x['modified_search_query'],x['product_name']),axis=1)
    return df_input_data

def get_token_Set_ratio_in_chunks(df_data,df_dev_queries,df_osq_msq,max_rows):
    df_final_result=pd.DataFrame()
    total_count=df_data.shape[0]
    total_splits=int(total_count/max_rows)+1
    df_splits = np.array_split(df_data, total_splits)
    result=False
    for df_chunk in df_splits :
        print("applying algo for chunk:",df_chunk.shape)
        df_temp_tsr=get_token_set_ratio(df_chunk,df_dev_queries,df_osq_msq)
        df_final_result=df_final_result.append(df_temp_tsr,ignore_index=True)
    for each_column in ['product_name','join_col']:
        del df_final_result[each_column]
    return df_final_result


# In[ ]:


#df_prds_not_having_tsr=pd.DataFrame()


# In[ ]:


if df_prds_not_having_tsr.empty:
    df_tsr_for_prds_not_having_tsr=pd.DataFrame()
else:
    #df_tsr_for_prds_not_having_tsr=get_token_Set_ratio_in_chunks(df_prds_not_having_tsr.copy(),df_dev_queries.copy(),df_osq_msq.copy(),60)
    products_and_search_terms_tsr_query="""select * from datos_pocos.tsr_for_new_products_and_search_terms"""
    df_tsr_for_prds_not_having_tsr=SQL.fetch_data_with_schema(products_and_search_terms_tsr_query,"bigquery")

df_existed_prds_hr_topic_update=df_tsr_for_prds_not_having_tsr.append(df_existed_prds_hr_topic_update,ignore_index=True,sort=False)
# In[17]:
df_existed_prds_hr_topic_update=df_existed_prds_hr_topic_update.drop_duplicates()


# In[ ]:


def get_HR_buckets_with_updated_topics(dev_queries_list,df_st_topics,df_newly_added_products_added_date):
    df_st_all_prds=df_existed_prds_hr_topic_update.query("original_search_query in @dev_queries_list")[['modified_search_query','product_id','token_set_ratio','launch_date']].drop_duplicates().copy()
    df_st_all_prds=df_st_all_prds.rename(columns={'modified_search_query':'original_search_query'})
    df_st_prds_tsr_100=df_st_all_prds.query("token_set_ratio==100").copy()
    df_st_prds_tsr_100['relevance_bucket']='HR'

    #df_st_prds_tsr_not_100=df_st_all_prds.query("token_set_ratio<100").copy()
    df_st_prds_tsr_not_100=df_st_all_prds.copy()
    #print("claculating matched_num_of_evs_in_each_eg.....")
    df_test1=pd.merge(df_st_prds_tsr_not_100,df_old_tagging,on=['product_id'])
    df_st_topics=df_st_topics.drop_duplicates()
    df_test2=pd.merge(df_st_topics,df_test1,left_on=['original_search_query','topic_entity_value','topic_entity_group'],right_on=['original_search_query','product_entity_value','product_entity_group'],how='left')
    df_test2['matched_num_of_evs_in_each_eg']=df_test2.groupby(['original_search_query','product_id','product_entity_group'])['product_entity_value'].transform('nunique')
    df_test3=df_test2[['original_search_query','product_id','topic_entity_group','product_entity_group','topic_num_of_evs_in_each_eg','matched_num_of_evs_in_each_eg','launch_date']].drop_duplicates().copy()
    df_test3['modified_entity_group']=np.where((df_test3['topic_entity_group'].isin(['brand','category'])),df_test3['topic_entity_group'],'entity')

    #print("creating search term product pivot.....")
    df_st_product_pivot=pd.pivot_table(df_test3, values='matched_num_of_evs_in_each_eg', index=['original_search_query', 'product_id'],columns=['modified_entity_group'], aggfunc=np.sum).reset_index().fillna(0)
    df_st_product_pivot=pd.DataFrame(df_st_product_pivot.to_records())
    df_st_product_pivot.columns = [hdr.replace("(","").replace(")", "").replace("'","").replace('"',"").replace(",","") for hdr in df_st_product_pivot.columns]
    df_st_product_pivot.columns='product_'+df_st_product_pivot.columns.str.strip()
    df_st_product_pivot=df_st_product_pivot.rename(columns={'product_original_search_query':'original_search_query','product_product_id':'product_id'})

    #print("creating topic pivot.....")
    df_topic_pivot=pd.pivot_table(df_st_topics, values='topic_num_of_evs_in_each_eg', index=['original_search_query'],columns=['modified_entity_group'], aggfunc='count').reset_index().fillna(0)
    if 'category' in df_topic_pivot:
        df_topic_pivot['category']=np.where(df_topic_pivot['category']>1,1,df_topic_pivot['category'])
    if 'brand' in df_topic_pivot:
        df_topic_pivot['brand']=np.where(df_topic_pivot['brand']>1,1,df_topic_pivot['brand'])
    df_topic_pivot.columns='topic_'+df_topic_pivot.columns
    df_topic_pivot=df_topic_pivot.rename(columns={'topic_original_search_query':'original_search_query'})

    #print("joining product pivot and topic pivot.....")
    df_final_st_prd_topic=pd.merge(df_topic_pivot,df_st_product_pivot,on=['original_search_query'],how='left').fillna(0)
    for each_col in ['topic_brand','topic_category','topic_entity','product_brand','product_category','product_entity']:
        if each_col in df_final_st_prd_topic:
            pass
        else:
            df_final_st_prd_topic[each_col]=0

    #print("calculating topic_total_value,product_st_value,topic_product_value_diff,token_set_ratio_diff,final_diff_value,nor_final_diff_value.....")
    df_final_st_prd_topic['topic_total_value']=100*df_final_st_prd_topic['topic_brand']+100*df_final_st_prd_topic['topic_category']+50*df_final_st_prd_topic['topic_entity']
    df_final_st_prd_topic['product_st_value']=100*df_final_st_prd_topic['product_brand']+100*df_final_st_prd_topic['product_category']+50*df_final_st_prd_topic['product_entity']
    df_final_st_prd_topic['topic_product_value_diff']=(df_final_st_prd_topic['topic_total_value']-df_final_st_prd_topic['product_st_value'])/(df_final_st_prd_topic['topic_total_value'])*100
    df_final_st_prd_topic=df_final_st_prd_topic.sort_values('topic_product_value_diff')
    df_final_st_prd_topic['product_id']=df_final_st_prd_topic['product_id'].astype(int)

    df_final_st_prd_topic=pd.merge(df_final_st_prd_topic,df_st_all_prds[['original_search_query','product_id','token_set_ratio','launch_date']],on=['original_search_query','product_id'],how='left')
    df_final_st_prd_topic['token_set_ratio']=df_final_st_prd_topic['token_set_ratio'].fillna(0)
    df_final_st_prd_topic['token_set_ratio_diff']=100-df_final_st_prd_topic['token_set_ratio']
    df_final_st_prd_topic['final_diff_value']=df_final_st_prd_topic['topic_product_value_diff']+df_final_st_prd_topic['token_set_ratio_diff']

    #df_final_st_prd_topic['final_diff_value_min_group_by_os']=df_final_st_prd_topic.groupby('original_search_query')['final_diff_value'].transform('min')
    df_final_st_prd_topic['final_diff_value_max_group_by_os']=(df_final_st_prd_topic.groupby('original_search_query')['final_diff_value'].transform('max'))
    #df_final_st_prd_topic['min_max_nor_final_diff_value']=(df_final_st_prd_topic['final_diff_value']-df_final_st_prd_topic['final_diff_value_min_group_by_os'])/ (df_final_st_prd_topic['final_diff_value_max_group_by_os']-df_final_st_prd_topic['final_diff_value_min_group_by_os'])
    df_final_st_prd_topic['final_diff_value_max_group_by_os']=np.where(df_final_st_prd_topic['final_diff_value_max_group_by_os']==0,1,df_final_st_prd_topic['final_diff_value_max_group_by_os'])
    df_final_st_prd_topic['nor_final_diff_value']=(df_final_st_prd_topic['final_diff_value'])/ (df_final_st_prd_topic['final_diff_value_max_group_by_os'])*100

    #print("labelling HR,FR,TR,NR......")
    df_final_st_prd_topic=df_final_st_prd_topic.query("nor_final_diff_value!='inf' & nor_final_diff_value!='-inf'")

    df_final_st_prd_topic['relevance_bucket']="NOT_HR"
    df_final_st_prd_topic['relevance_bucket']=np.where( (df_final_st_prd_topic['nor_final_diff_value']<=10),'HR','NOT_HR')
    df_final_st_prd_topic['relevance_bucket']=np.where( (df_final_st_prd_topic['token_set_ratio']==100) & (df_final_st_prd_topic['topic_product_value_diff']<=50) , 'HR',df_final_st_prd_topic['relevance_bucket'])         
    df_final_st_prd_topic['relevance_bucket']=np.where( (df_final_st_prd_topic['token_set_ratio']==100) & (df_final_st_prd_topic['topic_category']==df_final_st_prd_topic['product_category']) & (df_final_st_prd_topic['topic_brand']==df_final_st_prd_topic['product_brand']) , 'HR',df_final_st_prd_topic['relevance_bucket'])         

    df_final_st_prd_topic=pd.merge(df_final_st_prd_topic.rename(columns={'original_search_query':'modified_search_query'}),df_osq_msq,on=['modified_search_query'],how='left')
    df_final_st_prd_topic['original_search_query']=np.where(df_final_st_prd_topic['original_search_query'].isnull(),df_final_st_prd_topic['modified_search_query'],df_final_st_prd_topic['original_search_query'])
    df_final_st_prd_topic=df_final_st_prd_topic[['original_search_query','modified_search_query','product_id','topic_brand','topic_category','topic_entity','product_brand','product_category','product_entity','topic_total_value','product_st_value','topic_product_value_diff','token_set_ratio','token_set_ratio_diff','final_diff_value','final_diff_value_max_group_by_os','nor_final_diff_value','relevance_bucket','launch_date']]
    df_final_st_prd_topic=df_final_st_prd_topic.query("product_id>0").copy()
    del df_final_st_prd_topic['launch_date']
    df_final_st_prd_topic=pd.merge(df_final_st_prd_topic,df_newly_added_products_added_date,on=['product_id'],how='left')
    df_final_st_prd_topic=df_final_st_prd_topic.query("launch_date==launch_date").drop_duplicates().copy()
    df_final_st_prd_topic['launch_date']=df_final_st_prd_topic['launch_date'].astype(int)
    df_final_st_prd_topic['updated_timestamp']=int(time.time())
    df_final_st_prd_topic=df_final_st_prd_topic.drop_duplicates()
    return df_final_st_prd_topic

def import_data_in_chunks(df_data,max_rows,table_name,db):
    total_count=df_data.shape[0]
    total_splits=int(total_count/max_rows)+1
    df_splits = np.array_split(df_data, total_splits)
    result=False
    for df_chunk in df_splits :
        print("importing chunk:",df_chunk.shape)
        SQL.insert_dataframe_to_db(df_chunk,db,table_name)
        result=True
    return result

def apply_algo_in_chunks(df_data,max_rows,df_st_topics,df_newly_added_products_added_date):
    SQL.update_data("truncate table search_term_high_relevant_new_launches_raw_data","","warehouse")
    total_count=df_data.shape[0]
    total_splits=int(total_count/max_rows)+1
    df_splits = np.array_split(df_data, total_splits)
    result=False
    for df_chunk in df_splits :
        print("applying algo for chunk:",df_chunk.shape)
        dev_queries_list=list(df_chunk['original_search_query'].unique())
        df_final_st_prd_topic=get_HR_buckets_with_updated_topics(dev_queries_list,df_st_topics.copy(),df_newly_added_products_added_date.copy())        
        print("pushing data to search_term_high_relevant_new_launches_raw_data....")
        import_data_in_chunks(df_final_st_prd_topic,100000,"search_term_high_relevant_new_launches_raw_data","warehouse")
        result=True
    return result


# In[ ]:


df=apply_algo_in_chunks(df_dev_queries,1000,df_st_topics.copy(),df_newly_added_products_added_date.copy())


# ----
#                                                         ROUGH WORK
# ----

# In[ ]:


# dev_queries_list=df_dev_queries.head(1000)
# df_st_topics=df_st_topics
# df_newly_added_products_added_date=df_newly_added_products_added_date

# df_st_all_prds=df_existed_prds_hr_topic_update.query("original_search_query in @dev_queries_list")[['modified_search_query','product_id','token_set_ratio','launch_date']].drop_duplicates().copy()
# df_st_all_prds=df_st_all_prds.rename(columns={'modified_search_query':'original_search_query'})
# df_st_prds_tsr_100=df_st_all_prds.query("token_set_ratio==100").copy()
# df_st_prds_tsr_100['relevance_bucket']='HR'

# #df_st_prds_tsr_not_100=df_st_all_prds.query("token_set_ratio<100").copy()
# df_st_prds_tsr_not_100=df_st_all_prds.copy()
# #print("claculating matched_num_of_evs_in_each_eg.....")
# df_test1=pd.merge(df_st_prds_tsr_not_100,df_old_tagging,on=['product_id'])
# df_st_topics=df_st_topics.drop_duplicates()
# df_test2=pd.merge(df_st_topics,df_test1,left_on=['original_search_query','topic_entity_value','topic_entity_group'],right_on=['original_search_query','product_entity_value','product_entity_group'],how='left')
# df_test2['matched_num_of_evs_in_each_eg']=df_test2.groupby(['original_search_query','product_id','product_entity_group'])['product_entity_value'].transform('nunique')
# df_test3=df_test2[['original_search_query','product_id','topic_entity_group','product_entity_group','topic_num_of_evs_in_each_eg','matched_num_of_evs_in_each_eg','launch_date']].drop_duplicates().copy()
# df_test3['modified_entity_group']=np.where((df_test3['topic_entity_group'].isin(['brand','category'])),df_test3['topic_entity_group'],'entity')

# #print("creating search term product pivot.....")
# df_st_product_pivot=pd.pivot_table(df_test3, values='matched_num_of_evs_in_each_eg', index=['original_search_query', 'product_id'],columns=['modified_entity_group'], aggfunc=np.sum).reset_index().fillna(0)
# df_st_product_pivot=pd.DataFrame(df_st_product_pivot.to_records())
# df_st_product_pivot.columns = [hdr.replace("(","").replace(")", "").replace("'","").replace('"',"").replace(",","") for hdr in df_st_product_pivot.columns]
# df_st_product_pivot.columns='product_'+df_st_product_pivot.columns.str.strip()
# df_st_product_pivot=df_st_product_pivot.rename(columns={'product_original_search_query':'original_search_query','product_product_id':'product_id'})

# #print("creating topic pivot.....")
# df_topic_pivot=pd.pivot_table(df_st_topics, values='topic_num_of_evs_in_each_eg', index=['original_search_query'],columns=['modified_entity_group'], aggfunc='count').reset_index().fillna(0)
# if 'category' in df_topic_pivot:
#     df_topic_pivot['category']=np.where(df_topic_pivot['category']>1,1,df_topic_pivot['category'])
# if 'brand' in df_topic_pivot:
#     df_topic_pivot['brand']=np.where(df_topic_pivot['brand']>1,1,df_topic_pivot['brand'])
# df_topic_pivot.columns='topic_'+df_topic_pivot.columns
# df_topic_pivot=df_topic_pivot.rename(columns={'topic_original_search_query':'original_search_query'})

# #print("joining product pivot and topic pivot.....")
# df_final_st_prd_topic=pd.merge(df_topic_pivot,df_st_product_pivot,on=['original_search_query'],how='left').fillna(0)
# for each_col in ['topic_brand','topic_category','topic_entity','product_brand','product_category','product_entity']:
#     if each_col in df_final_st_prd_topic:
#         pass
#     else:
#         df_final_st_prd_topic[each_col]=0

# #print("calculating topic_total_value,product_st_value,topic_product_value_diff,token_set_ratio_diff,final_diff_value,nor_final_diff_value.....")
# df_final_st_prd_topic['topic_total_value']=100*df_final_st_prd_topic['topic_brand']+100*df_final_st_prd_topic['topic_category']+50*df_final_st_prd_topic['topic_entity']
# df_final_st_prd_topic['product_st_value']=100*df_final_st_prd_topic['product_brand']+100*df_final_st_prd_topic['product_category']+50*df_final_st_prd_topic['product_entity']
# df_final_st_prd_topic['topic_product_value_diff']=(df_final_st_prd_topic['topic_total_value']-df_final_st_prd_topic['product_st_value'])/(df_final_st_prd_topic['topic_total_value'])*100
# df_final_st_prd_topic=df_final_st_prd_topic.sort_values('topic_product_value_diff')
# df_final_st_prd_topic['product_id']=df_final_st_prd_topic['product_id'].astype(int)

# df_final_st_prd_topic=pd.merge(df_final_st_prd_topic,df_st_all_prds[['original_search_query','product_id','token_set_ratio','launch_date']],on=['original_search_query','product_id'],how='left')
# df_final_st_prd_topic['token_set_ratio']=df_final_st_prd_topic['token_set_ratio'].fillna(0)
# df_final_st_prd_topic['token_set_ratio_diff']=100-df_final_st_prd_topic['token_set_ratio']
# df_final_st_prd_topic['final_diff_value']=df_final_st_prd_topic['topic_product_value_diff']+df_final_st_prd_topic['token_set_ratio_diff']

# #df_final_st_prd_topic['final_diff_value_min_group_by_os']=df_final_st_prd_topic.groupby('original_search_query')['final_diff_value'].transform('min')
# df_final_st_prd_topic['final_diff_value_max_group_by_os']=(df_final_st_prd_topic.groupby('original_search_query')['final_diff_value'].transform('max'))
# #df_final_st_prd_topic['min_max_nor_final_diff_value']=(df_final_st_prd_topic['final_diff_value']-df_final_st_prd_topic['final_diff_value_min_group_by_os'])/ (df_final_st_prd_topic['final_diff_value_max_group_by_os']-df_final_st_prd_topic['final_diff_value_min_group_by_os'])
# df_final_st_prd_topic['final_diff_value_max_group_by_os']=np.where(df_final_st_prd_topic['final_diff_value_max_group_by_os']==0,1,df_final_st_prd_topic['final_diff_value_max_group_by_os'])
# df_final_st_prd_topic['nor_final_diff_value']=(df_final_st_prd_topic['final_diff_value'])/ (df_final_st_prd_topic['final_diff_value_max_group_by_os'])*100

# #print("labelling HR,FR,TR,NR......")
# df_final_st_prd_topic=df_final_st_prd_topic.query("nor_final_diff_value!='inf' & nor_final_diff_value!='-inf'")

# df_final_st_prd_topic['relevance_bucket']="NOT_HR"
# df_final_st_prd_topic['relevance_bucket']=np.where( (df_final_st_prd_topic['nor_final_diff_value']<=10),'HR','NOT_HR')
# df_final_st_prd_topic['relevance_bucket']=np.where( (df_final_st_prd_topic['token_set_ratio']==100) & (df_final_st_prd_topic['topic_product_value_diff']<=50) , 'HR',df_final_st_prd_topic['relevance_bucket'])         
# df_final_st_prd_topic['relevance_bucket']=np.where( (df_final_st_prd_topic['token_set_ratio']==100) & (df_final_st_prd_topic['topic_category']==df_final_st_prd_topic['product_category']) & (df_final_st_prd_topic['topic_brand']==df_final_st_prd_topic['product_brand']) , 'HR',df_final_st_prd_topic['relevance_bucket'])         

# df_final_st_prd_topic=pd.merge(df_final_st_prd_topic.rename(columns={'original_search_query':'modified_search_query'}),df_osq_msq,on=['modified_search_query'],how='left')
# df_final_st_prd_topic['original_search_query']=np.where(df_final_st_prd_topic['original_search_query'].isnull(),df_final_st_prd_topic['modified_search_query'],df_final_st_prd_topic['original_search_query'])
# df_final_st_prd_topic=df_final_st_prd_topic[['original_search_query','modified_search_query','product_id','topic_brand','topic_category','topic_entity','product_brand','product_category','product_entity','topic_total_value','product_st_value','topic_product_value_diff','token_set_ratio','token_set_ratio_diff','final_diff_value','final_diff_value_max_group_by_os','nor_final_diff_value','relevance_bucket','launch_date']]
# df_final_st_prd_topic=df_final_st_prd_topic.query("product_id>0").copy()
# del df_final_st_prd_topic['launch_date']
# df_final_st_prd_topic=pd.merge(df_final_st_prd_topic,df_newly_added_products_added_date,on=['product_id'],how='left')
# df_final_st_prd_topic=df_final_st_prd_topic.query("launch_date==launch_date").drop_duplicates().copy()
# df_final_st_prd_topic['launch_date']=df_final_st_prd_topic['launch_date'].astype(int)
# df_final_st_prd_topic['updated_timestamp']=int(time.time())
# df_final_st_prd_topic=df_final_st_prd_topic.drop_duplicates()


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:


