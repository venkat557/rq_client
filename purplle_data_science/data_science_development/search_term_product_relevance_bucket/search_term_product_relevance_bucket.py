#!/usr/bin/env python
# coding: utf-8

# In[1]:


# importing required libraries
print("importing required libraries")
import pandas as pd
import numpy as np
import time
import sys,os
sys.path.append(os.environ['DATA_SCIENCE_HOME'])
import helper.mysql.get_connection  as SQL
from datetime import datetime, timedelta
import helper.lib.utilities as UT
import re
import time

from nltk.corpus import stopwords 
import nltk, re, pprint, sys,os,json
from nltk.corpus.reader import ConllChunkCorpusReader 
from nltk import conlltags2tree, tree2conlltags
from nltk import word_tokenize, pos_tag, ne_chunk
from nltk.stem.wordnet import WordNetLemmatizer 
from fuzzywuzzy import fuzz


# In[2]:


def get_old_tagging():
    product_ids=''
    query_old_tagging="""
    select module_id as product_id,ev.id as entity_value_id,lower(ev.entity_value) as entity_value,ev.entity_group_id as entity_group_id,lower(eg.name) as entity_group
    from entity_products as ep
    join  entity_values ev on ev.id=ep.entity_value_id
    join entity_groups eg on ev.entity_group_id=eg.id
    where module='product'
    """
    df_old_tagging=SQL.fetch_data_with_schema(query_old_tagging,"slave")
    
    query_cat="""
    select product_id,category_id as entity_value_id,lower(category_name) as entity_value,-2 as entity_group_id,'category' as entity_group
    from product_master
    group by product_id,category_id
    """
    df_cat=SQL.fetch_data_with_schema(query_cat,"warehouse")

    query_brand="""
    select product_id,brand_id as entity_value_id,lower(brand_name) as entity_value,-3 as entity_group_id,'brand' as entity_group
    from product_master
    group by product_id,brand_id
    """
    df_brand=SQL.fetch_data_with_schema(query_brand,"warehouse")
    
    query_aliases="""
    select ep.module_id as product_id,ev.id as entity_value_id,lower(ea.alias_name) as entity_value,ev.entity_group_id as entity_group_id,lower(eg.name) as entity_group
    from entity_products as ep
    join  entity_values ev on ev.id=ep.entity_value_id
    join entity_groups eg on ev.entity_group_id=eg.id
    join entity_aliases ea on ea.module_id=ev.id and ea.module='entity_value'
    where ep.module='product'
    """
    df_aliases=SQL.fetch_data_with_schema(query_aliases,"slave")
    
    df_old_tagging=df_old_tagging.append([df_cat,df_brand,df_aliases],ignore_index=True)
    df_old_tagging['entity_value']=df_old_tagging['entity_value'].str.lower()
    #df_old_tagging['entity_value']=df_old_tagging['entity_value'].str.replace('&','and')
    df_old_tagging=df_old_tagging[~df_old_tagging.entity_value.isnull()]
    df_old_tagging.columns="product_"+df_old_tagging.columns
    df_old_tagging.rename(columns={'product_product_id':'product_id'},inplace=True)
    df_old_tagging=df_old_tagging.drop_duplicates()
    return df_old_tagging


# In[3]:


def import_data_in_chunks(df_data,max_rows,table_name,db):
    total_count=df_data.shape[0]
    total_splits=int(total_count/max_rows)+1
    df_splits = np.array_split(df_data, total_splits)
    result=False
    for df_chunk in df_splits :
        print("importing chunk:",df_chunk.shape)
        SQL.insert_dataframe_to_db(df_chunk,db,table_name)
        result=True
    return result


# In[4]:


print("getting old tagging......")
df_old_tagging=get_old_tagging()


# In[5]:


print("getting search term and topics......")
query_st_topics="""
SELECT modified_search_query,original_search_query,entity_value as topic_entity_value,entity_group as topic_entity_group
FROM `topic_identification_of_query_raw_data`
where orders_contrib>0
group by original_search_query,entity_value,entity_group 
"""
df_st_topics=SQL.fetch_data_with_schema(query_st_topics,"warehouse")

df_osq_msq=df_st_topics[['original_search_query','modified_search_query']].drop_duplicates()

del df_st_topics['original_search_query']
df_st_topics=df_st_topics.rename(columns={'modified_search_query':'original_search_query'})
df_st_topics['topic_num_of_evs_in_each_eg']=df_st_topics.groupby(['original_search_query','topic_entity_group'])['topic_entity_value'].transform('nunique')
df_st_topics['modified_entity_group']=np.where((df_st_topics['topic_entity_group'].isin(['brand','category'])),df_st_topics['topic_entity_group'],'entity')
df_st_topics=df_st_topics.drop_duplicates()


# In[6]:


print("getting all products......")
query_pm="""
select product_id,lower(name) as product_name
from product_master
group by product_id
"""
df_pm=SQL.fetch_data_with_schema(query_pm,"warehouse")

df_all_products=df_pm[['product_id']].drop_duplicates()
df_all_products['join_col']=1
df_all_products=pd.merge(df_all_products,df_pm,on=['product_id'],how='left')


# In[7]:


query_dev_queries="""
select lower(keyword) as original_search_query
from datos_pocos.search_term_wise_product_ranking
group by keyword
"""
df_dev_queries=SQL.fetch_data_with_schema(query_dev_queries,"bigquery")
df_dev_queries=df_dev_queries.drop_duplicates()


# In[20]:


print("getting all products......")
query_pm_for_exact_cat_brand="""
select product_id,lower(name) as product_name,lower(category_name) as category_name,lower(brand_name) as brand_name
from product_master
group by product_id
"""
df_pm_for_exact_cat_brand=SQL.fetch_data_with_schema(query_pm_for_exact_cat_brand,"warehouse")
df_pm_for_exact_cat_brand=df_pm_for_exact_cat_brand.drop_duplicates(subset='product_id')


# In[66]:


#highly relevant products for exact match of cat and brand keywords
df_dev_queries_for_exact_cat_brand=pd.merge(df_dev_queries,df_osq_msq,on=['original_search_query'],how='left')
df_dev_queries_for_exact_cat_brand['modified_search_query']=np.where(df_dev_queries_for_exact_cat_brand['modified_search_query'].isnull(),df_dev_queries_for_exact_cat_brand['original_search_query'],df_dev_queries_for_exact_cat_brand['modified_search_query'])
df_dev_queries_for_exact_cat_brand=pd.merge(df_dev_queries_for_exact_cat_brand,df_pm_for_exact_cat_brand[['product_id','category_name']],left_on=['modified_search_query'],right_on=['category_name'],how='left')
df_dev_queries_for_exact_cat_brand=pd.merge(df_dev_queries_for_exact_cat_brand,df_pm_for_exact_cat_brand[['product_id','brand_name']],left_on=['modified_search_query'],right_on=['brand_name'],how='left')
df_dev_queries_for_exact_cat_brand['product_id']=np.where(df_dev_queries_for_exact_cat_brand['product_id_x'].isnull(),df_dev_queries_for_exact_cat_brand['product_id_y'],df_dev_queries_for_exact_cat_brand['product_id_x'])
df_dev_queries_for_exact_cat_brand=df_dev_queries_for_exact_cat_brand[~df_dev_queries_for_exact_cat_brand['product_id'].isnull()][['modified_search_query','product_id']].drop_duplicates()
df_dev_queries_for_exact_cat_brand['product_id']=df_dev_queries_for_exact_cat_brand['product_id'].fillna(0).astype(int)
df_dev_queries_for_exact_cat_brand['token_set_ratio']=100
df_dev_queries_for_exact_cat_brand=df_dev_queries_for_exact_cat_brand.rename(columns={'modified_search_query':'original_search_query'})
df_dev_queries_for_exact_cat_brand=df_dev_queries_for_exact_cat_brand[['original_search_query','product_id','token_set_ratio']]


# In[70]:


current_timestamp=int(time.time())


# In[95]:


def get_HR_buckets(original_queries_list,df_pm,df_st_topics):
    #test_keyword="sandalwood"
    #for each_search_term in df_st_topics.query("original_search_query==@test_keyword").original_search_query.unique():
    #for each_search_term in df_st_topics.original_search_query.unique():
    print("getting tsr data from search_term_product_impt_gt1_token_set_ratio....")
    dev_queries='","'.join(original_queries_list)
    dev_queries='"'+dev_queries+'"'
    query_st_prd_tsr="""
    select modified_search_query as original_search_query,product_id,token_set_ratio
    from search_term_product_impt_gt1_token_set_ratio
    where original_search_query in ({})
    group by modified_search_query,product_id,token_set_ratio
    """.format(dev_queries)
    df_st_prd_tsr=SQL.fetch_data_with_schema(query_st_prd_tsr,"warehouse")
    #print(df_dev_queries_for_exact_cat_brand.query("original_search_query in @original_queries_list"))
    df_st_prd_tsr=df_st_prd_tsr.append(df_dev_queries_for_exact_cat_brand.query("original_search_query in @original_queries_list"),ignore_index=True)
    df_st_prd_tsr=df_st_prd_tsr.drop_duplicates()
    df_st_prd_tsr=pd.merge(df_st_prd_tsr,df_pm,on=['product_id'],how='left')
    df_st_all_prds=df_st_prd_tsr.copy()

    df_st_prds_tsr_100=df_st_all_prds.query("token_set_ratio==100").copy()
    df_st_prds_tsr_100['relevance_bucket']='HR'

    #df_st_prds_tsr_not_100=df_st_all_prds.query("token_set_ratio<100").copy()
    df_st_prds_tsr_not_100=df_st_all_prds.copy()

    print("claculating matched_num_of_evs_in_each_eg.....")
    df_test1=pd.merge(df_st_prds_tsr_not_100,df_old_tagging,on=['product_id'])
    df_st_topics=df_st_topics.drop_duplicates()
    df_test2=pd.merge(df_st_topics,df_test1,left_on=['original_search_query','topic_entity_value','topic_entity_group'],right_on=['original_search_query','product_entity_value','product_entity_group'],how='left')
    df_test2['matched_num_of_evs_in_each_eg']=df_test2.groupby(['original_search_query','product_id','product_entity_group'])['product_entity_value'].transform('nunique')
    df_test3=df_test2[['original_search_query','product_id','topic_entity_group','product_entity_group','topic_num_of_evs_in_each_eg','matched_num_of_evs_in_each_eg']].drop_duplicates().copy()
    #df_test3['matched_perc_of_evs_in_each_eg']=(df_test3['matched_num_of_evs_in_each_eg']/df_test3['topic_num_of_evs_in_each_eg'])*100
    #df_test3['matched_perc_of_evs_in_each_eg']=np.where(df_test3['product_entity_group'].isin(['brand','category']),100,df_test3['matched_perc_of_evs_in_each_eg'])
    df_test3['modified_entity_group']=np.where((df_test3['topic_entity_group'].isin(['brand','category'])),df_test3['topic_entity_group'],'entity')

    #print("creating search term product pivot.....")
    #df_st_product_pivot=pd.pivot_table(df_test3, values='matched_num_of_evs_in_each_eg', index=['original_search_query', 'product_id'],columns=['modified_entity_group'], aggfunc=np.sum).reset_index().fillna(0)
    #df_st_product_pivot.columns='product_'+df_st_product_pivot.columns
    #df_st_product_pivot=df_st_product_pivot.rename(columns={'product_original_search_query':'original_search_query','product_product_id':'product_id'})


    print("creating search term product pivot.....")
    df_st_product_pivot=pd.pivot_table(df_test3, values='matched_num_of_evs_in_each_eg', index=['original_search_query', 'product_id'],columns=['modified_entity_group'], aggfunc=np.sum).reset_index().fillna(0)
    df_st_product_pivot=pd.DataFrame(df_st_product_pivot.to_records())
    df_st_product_pivot.columns = [hdr.replace("(","").replace(")", "").replace("'","").replace('"',"").replace(",","") for hdr in df_st_product_pivot.columns]
    df_st_product_pivot.columns='product_'+df_st_product_pivot.columns.str.strip()
    df_st_product_pivot=df_st_product_pivot.rename(columns={'product_original_search_query':'original_search_query','product_product_id':'product_id'})
    # df_test3['matched_perc_of_evs_in_each_eg']=df_test3['matched_perc_of_evs_in_each_eg']*df_test3['matched_num_of_evs_in_each_eg']
    # df_test3['sum_matched_perc_of_evs_in_each_eg']=df_test3.groupby(['original_search_query','product_id'])['matched_perc_of_evs_in_each_eg'].transform('sum')
    # df_test3=df_test3.sort_values('sum_matched_perc_of_evs_in_each_eg',ascending=False)

    print("creating topic pivot.....")
    df_topic_pivot=pd.pivot_table(df_st_topics, values='topic_num_of_evs_in_each_eg', index=['original_search_query'],columns=['modified_entity_group'], aggfunc='count').reset_index().fillna(0)
    if 'category' in df_topic_pivot:
        df_topic_pivot['category']=np.where(df_topic_pivot['category']>1,1,df_topic_pivot['category'])
    if 'brand' in df_topic_pivot:
        df_topic_pivot['brand']=np.where(df_topic_pivot['brand']>1,1,df_topic_pivot['brand'])
    df_topic_pivot.columns='topic_'+df_topic_pivot.columns
    df_topic_pivot=df_topic_pivot.rename(columns={'topic_original_search_query':'original_search_query'})

    print("joining product pivot and topic pivot.....")
    df_final_st_prd_topic=pd.merge(df_topic_pivot,df_st_product_pivot,on=['original_search_query'],how='left').fillna(0)
    for each_col in ['topic_brand','topic_category','topic_entity','product_brand','product_category','product_entity']:
        if each_col in df_final_st_prd_topic:
            pass
        else:
            df_final_st_prd_topic[each_col]=0

    print("calculating topic_total_value,product_st_value,topic_product_value_diff,token_set_ratio_diff,final_diff_value,nor_final_diff_value.....")
    df_final_st_prd_topic['topic_total_value']=100*df_final_st_prd_topic['topic_brand']+100*df_final_st_prd_topic['topic_category']+50*df_final_st_prd_topic['topic_entity']
    df_final_st_prd_topic['product_st_value']=100*df_final_st_prd_topic['product_brand']+100*df_final_st_prd_topic['product_category']+50*df_final_st_prd_topic['product_entity']
    df_final_st_prd_topic['topic_product_value_diff']=(df_final_st_prd_topic['topic_total_value']-df_final_st_prd_topic['product_st_value'])/(df_final_st_prd_topic['topic_total_value'])*100
    df_final_st_prd_topic=df_final_st_prd_topic.sort_values('topic_product_value_diff')
    df_final_st_prd_topic['product_id']=df_final_st_prd_topic['product_id'].astype(int)

    df_final_st_prd_topic=pd.merge(df_final_st_prd_topic,df_st_all_prds[['original_search_query','product_id','token_set_ratio']],on=['original_search_query','product_id'],how='left')
    df_final_st_prd_topic['token_set_ratio']=df_final_st_prd_topic['token_set_ratio'].fillna(0)
    df_final_st_prd_topic['token_set_ratio_diff']=100-df_final_st_prd_topic['token_set_ratio']
    df_final_st_prd_topic['final_diff_value']=df_final_st_prd_topic['topic_product_value_diff']+df_final_st_prd_topic['token_set_ratio_diff']

    #df_final_st_prd_topic['final_diff_value_min_group_by_os']=df_final_st_prd_topic.groupby('original_search_query')['final_diff_value'].transform('min')
    df_final_st_prd_topic['final_diff_value_max_group_by_os']=df_final_st_prd_topic.groupby('original_search_query')['final_diff_value'].transform('max')
    #df_final_st_prd_topic['min_max_nor_final_diff_value']=(df_final_st_prd_topic['final_diff_value']-df_final_st_prd_topic['final_diff_value_min_group_by_os'])/ (df_final_st_prd_topic['final_diff_value_max_group_by_os']-df_final_st_prd_topic['final_diff_value_min_group_by_os'])
    df_final_st_prd_topic['nor_final_diff_value']=(df_final_st_prd_topic['final_diff_value'])/ (df_final_st_prd_topic['final_diff_value_max_group_by_os'])*100

    print("labelling HR,FR,TR,NR......")
    df_final_st_prd_topic=df_final_st_prd_topic.query("nor_final_diff_value!='inf' & nor_final_diff_value!='-inf'")
    
    df_final_st_prd_topic['relevance_bucket']="NOT_HR"
    df_final_st_prd_topic['relevance_bucket']=np.where( (df_final_st_prd_topic['nor_final_diff_value']<=10),'HR','NOT_HR')
    df_final_st_prd_topic['relevance_bucket']=np.where( (df_final_st_prd_topic['token_set_ratio']==100) & (df_final_st_prd_topic['topic_product_value_diff']<=50) , 'HR',df_final_st_prd_topic['relevance_bucket'])         
    df_final_st_prd_topic['relevance_bucket']=np.where( (df_final_st_prd_topic['token_set_ratio']==100) & (df_final_st_prd_topic['topic_category']==df_final_st_prd_topic['product_category']) & (df_final_st_prd_topic['topic_brand']==df_final_st_prd_topic['product_brand']) , 'HR',df_final_st_prd_topic['relevance_bucket'])         
    
    #df_final_st_prd_topic.loc[df_final_st_prd_topic['nor_final_diff_value']<=10,'relevance_bucket']='HR'
    #df_final_st_prd_topic.loc[(df_final_st_prd_topic['nor_final_diff_value']>10) & (df_final_st_prd_topic['nor_final_diff_value']<=30),'relevance_bucket']='FR'
    #df_final_st_prd_topic.loc[(df_final_st_prd_topic['nor_final_diff_value']>30) & (df_final_st_prd_topic['nor_final_diff_value']<=70),'relevance_bucket']='TR'
    #df_final_st_prd_topic.loc[df_final_st_prd_topic['nor_final_diff_value']>70,'relevance_bucket']='NR'

    
    #df_final_st_prd_topic=df_final_st_prd_topic.append(df_st_prds_tsr_100[['original_search_query','product_id','token_set_ratio','relevance_bucket']],ignore_index=True,sort=False)
    #df_final_st_prd_topic=pd.merge(df_final_st_prd_topic,df_pm,on=['product_id'],how='left')
    df_final_st_prd_topic=df_final_st_prd_topic.query("relevance_bucket=='HR'")
    df_final_st_prd_topic=pd.merge(df_final_st_prd_topic.rename(columns={'original_search_query':'modified_search_query'}),df_osq_msq,on=['modified_search_query'],how='left')
    df_final_st_prd_topic['original_search_query']=np.where(df_final_st_prd_topic['original_search_query'].isnull(),df_final_st_prd_topic['modified_search_query'],df_final_st_prd_topic['original_search_query'])
    df_final_st_prd_topic=df_final_st_prd_topic[['original_search_query','modified_search_query','product_id','topic_brand','topic_category','topic_entity','product_brand','product_category','product_entity','topic_total_value','product_st_value','topic_product_value_diff','token_set_ratio','token_set_ratio_diff','final_diff_value','final_diff_value_max_group_by_os','nor_final_diff_value','relevance_bucket']]
    df_final_st_prd_topic['updated_timestamp']=current_timestamp
    return df_final_st_prd_topic


# In[ ]:


def apply_algo_in_chunks(df_data,max_rows,df_pm,df_st_topics):
    total_count=df_data.shape[0]
    total_splits=int(total_count/max_rows)+1
    df_splits = np.array_split(df_data, total_splits)
    result=False
    for df_chunk in df_splits :
        print("applying algo for chunk:",df_chunk.shape)
        dev_queries_list=df_chunk['original_search_query'].unique()
        df_final_st_prd_topic=get_HR_buckets(dev_queries_list,df_pm,df_st_topics.copy())
        
        existed_original_queries='","'.join(df_final_st_prd_topic.original_search_query.unique())
        existed_original_queries='"'+existed_original_queries+'"'
        
        query_existed_queries="select * from search_term_high_relevant_products where original_search_query in ({})".format(existed_original_queries)
        df_existed_queries=SQL.fetch_data_with_schema(query_existed_queries,"warehouse")
        
        df_final_st_prd_topic=df_final_st_prd_topic.append(df_existed_queries,ignore_index=True)
        df_final_st_prd_topic=df_final_st_prd_topic.sort_values('updated_timestamp',ascending=False).groupby(['original_search_query','product_id']).head(1)
        #existed_st_prds=",".join(df_left_right_cross_join.product_id.astype(str).unique())
        

        #df_final_st_prd_topic['delete_query_list']=df_final_st_prd_topic.apply(lambda x:"(original_search_query='"+str(x['original_search_query'])+"' AND product_id="+str(x['product_id'])+")",axis=1)
        #delete_query_string=" OR ".join(df_final_st_prd_topic.delete_query_list.unique())
        
        delete_query_for_search_term_high_relevant_products ="""
        delete from search_term_high_relevant_products 
        where original_search_query in ({})""".format(existed_original_queries)
        print("delete_query_for_search_term_high_relevant_products....")
        SQL.update_data(delete_query_for_search_term_high_relevant_products,"","warehouse")
        
        print("pushing data to topic_identification_of_query_delta....")
        import_data_in_chunks(df_final_st_prd_topic,100000,"search_term_high_relevant_products","warehouse")
        result=True
    return result


# In[ ]:


df=apply_algo_in_chunks(df_dev_queries,1000,df_pm,df_st_topics.copy())


# In[ ]:


end_time=int(time.time())
print("completed in ",end_time-current_timestamp," seconds")


# In[ ]:





# ------
#                                                                     ROUGH WORK
# ------

# In[ ]:


# test_keyword="aloe vera gel"
# for each_search_term in df_st_topics.query("original_search_query==@test_keyword").original_search_query.unique():
# #for each_search_term in df_st_topics.original_search_query.unique():
#     print("-------------------------------------",each_search_term,"---------------------------------------------")
#     df_st_all_prds=df_st_prd_tsr.query("original_search_query==@each_search_term").copy()
    
#     df_st_prds_tsr_100=df_st_all_prds.query("token_set_ratio==100").copy()
#     df_st_prds_tsr_100['relevance_bucket']='HR'

#     df_st_prds_tsr_not_100=df_st_all_prds.query("token_set_ratio<100").copy()
    
#     print("claculating matched_num_of_evs_in_each_eg.....")
#     df_test1=pd.merge(df_st_prds_tsr_not_100,df_old_tagging,on=['product_id'])
#     df_test2=pd.merge(df_st_topics.query("original_search_query==@each_search_term"),df_test1,left_on=['original_search_query','topic_entity_value','topic_entity_group'],right_on=['original_search_query','product_entity_value','product_entity_group'],how='left')
#     df_test2['matched_num_of_evs_in_each_eg']=df_test2.groupby(['original_search_query','product_id','product_entity_group'])['product_entity_value'].transform('nunique')
#     df_test3=df_test2[['original_search_query','product_id','topic_entity_group','product_entity_group','topic_num_of_evs_in_each_eg','matched_num_of_evs_in_each_eg']].drop_duplicates().copy()
#     #df_test3['matched_perc_of_evs_in_each_eg']=(df_test3['matched_num_of_evs_in_each_eg']/df_test3['topic_num_of_evs_in_each_eg'])*100
#     #df_test3['matched_perc_of_evs_in_each_eg']=np.where(df_test3['product_entity_group'].isin(['brand','category']),100,df_test3['matched_perc_of_evs_in_each_eg'])
#     df_test3['modified_entity_group']=np.where((df_test3['topic_entity_group'].isin(['brand','category'])),df_test3['topic_entity_group'],'entity')

#     print("creating search term product pivot.....")
#     #df_st_product_pivot=pd.pivot_table(df_test3, values='matched_num_of_evs_in_each_eg', index=['original_search_query', 'product_id'],columns=['modified_entity_group'], aggfunc=np.sum).reset_index().fillna(0)
#     #df_st_product_pivot.columns='product_'+df_st_product_pivot.columns
#     #df_st_product_pivot=df_st_product_pivot.rename(columns={'product_original_search_query':'original_search_query','product_product_id':'product_id'})

    
#     print("creating search term product pivot.....")
#     df_st_product_pivot=pd.pivot_table(df_test3, values='matched_num_of_evs_in_each_eg', index=['original_search_query', 'product_id'],columns=['modified_entity_group'], aggfunc=np.sum).reset_index().fillna(0)
#     df_st_product_pivot=pd.DataFrame(df_st_product_pivot.to_records())
#     df_st_product_pivot.columns = [hdr.replace("(","").replace(")", "").replace("'","").replace('"',"").replace(",","") for hdr in df_st_product_pivot.columns]
#     df_st_product_pivot.columns='product_'+df_st_product_pivot.columns.str.strip()
#     df_st_product_pivot=df_st_product_pivot.rename(columns={'product_original_search_query':'original_search_query','product_product_id':'product_id'})
#     # df_test3['matched_perc_of_evs_in_each_eg']=df_test3['matched_perc_of_evs_in_each_eg']*df_test3['matched_num_of_evs_in_each_eg']
#     # df_test3['sum_matched_perc_of_evs_in_each_eg']=df_test3.groupby(['original_search_query','product_id'])['matched_perc_of_evs_in_each_eg'].transform('sum')
#     # df_test3=df_test3.sort_values('sum_matched_perc_of_evs_in_each_eg',ascending=False)

#     print("creating topic pivot.....")
#     df_topic_pivot=pd.pivot_table(df_st_topics.query("original_search_query==@each_search_term"), values='topic_num_of_evs_in_each_eg', index=['original_search_query'],columns=['modified_entity_group'], aggfunc='count').reset_index().fillna(0)
#     if 'category' in df_topic_pivot:
#         df_topic_pivot['category']=np.where(df_topic_pivot['category']>1,1,df_topic_pivot['category'])
#     if 'brand' in df_topic_pivot:
#         df_topic_pivot['brand']=np.where(df_topic_pivot['brand']>1,1,df_topic_pivot['brand'])
#     df_topic_pivot.columns='topic_'+df_topic_pivot.columns
#     df_topic_pivot=df_topic_pivot.rename(columns={'topic_original_search_query':'original_search_query'})

#     print("joining product pivot and topic pivot.....")
#     df_final_st_prd_topic=pd.merge(df_topic_pivot,df_st_product_pivot,on=['original_search_query'],how='left').fillna(0)
#     for each_col in ['topic_brand','topic_category','topic_entity','product_brand','product_category','product_entity']:
#         if each_col in df_final_st_prd_topic:
#             pass
#         else:
#             df_final_st_prd_topic[each_col]=0

#     print("calculating topic_total_value,product_st_value,topic_product_value_diff,token_set_ratio_diff,final_diff_value,nor_final_diff_value.....")
#     df_final_st_prd_topic['topic_total_value']=100*df_final_st_prd_topic['topic_brand']+100*df_final_st_prd_topic['topic_category']+50*df_final_st_prd_topic['topic_entity']
#     df_final_st_prd_topic['product_st_value']=100*df_final_st_prd_topic['product_brand']+100*df_final_st_prd_topic['product_category']+50*df_final_st_prd_topic['product_entity']
#     df_final_st_prd_topic['topic_product_value_diff']=(df_final_st_prd_topic['topic_total_value']-df_final_st_prd_topic['product_st_value'])/(df_final_st_prd_topic['topic_total_value'])*100
#     df_final_st_prd_topic=df_final_st_prd_topic.sort_values('topic_product_value_diff')
#     df_final_st_prd_topic['product_id']=df_final_st_prd_topic['product_id'].astype(int)

#     df_final_st_prd_topic=pd.merge(df_final_st_prd_topic,df_st_all_prds[['original_search_query','product_id','token_set_ratio']],on=['original_search_query','product_id'],how='left')
#     df_final_st_prd_topic['token_set_ratio']=df_final_st_prd_topic['token_set_ratio'].fillna(0)
#     df_final_st_prd_topic['token_set_ratio_diff']=100-df_final_st_prd_topic['token_set_ratio']
#     df_final_st_prd_topic['final_diff_value']=df_final_st_prd_topic['topic_product_value_diff']+df_final_st_prd_topic['token_set_ratio_diff']

#     #df_final_st_prd_topic['final_diff_value_min_group_by_os']=df_final_st_prd_topic.groupby('original_search_query')['final_diff_value'].transform('min')
#     df_final_st_prd_topic['final_diff_value_max_group_by_os']=df_final_st_prd_topic.groupby('original_search_query')['final_diff_value'].transform('max')
#     #df_final_st_prd_topic['min_max_nor_final_diff_value']=(df_final_st_prd_topic['final_diff_value']-df_final_st_prd_topic['final_diff_value_min_group_by_os'])/ (df_final_st_prd_topic['final_diff_value_max_group_by_os']-df_final_st_prd_topic['final_diff_value_min_group_by_os'])
#     df_final_st_prd_topic['nor_final_diff_value']=(df_final_st_prd_topic['final_diff_value'])/ (df_final_st_prd_topic['final_diff_value_max_group_by_os'])*100

#     print("labelling HR,FR,TR,NR......")
#     df_final_st_prd_topic=df_final_st_prd_topic.query("nor_final_diff_value!='inf' & nor_final_diff_value!='-inf'")
#     df_final_st_prd_topic.loc[df_final_st_prd_topic['nor_final_diff_value']<=20,'relevance_bucket']='HR'
#     df_final_st_prd_topic.loc[(df_final_st_prd_topic['nor_final_diff_value']>20) & (df_final_st_prd_topic['nor_final_diff_value']<=50),'relevance_bucket']='FR'
#     df_final_st_prd_topic.loc[(df_final_st_prd_topic['nor_final_diff_value']>50) & (df_final_st_prd_topic['nor_final_diff_value']<=70),'relevance_bucket']='TR'
#     df_final_st_prd_topic.loc[df_final_st_prd_topic['nor_final_diff_value']>70,'relevance_bucket']='NR'
#     df_final_st_prd_topic=df_final_st_prd_topic.append(df_st_prds_tsr_100[['original_search_query','product_id','token_set_ratio','relevance_bucket']],ignore_index=True,sort=False)
#     df_final_st_prd_topic=pd.merge(df_final_st_prd_topic,df_pm,on=['product_id'],how='left')
#     df_final_st_prd_topic=df_final_st_prd_topic.query("relevance_bucket=='HR'")
#     df_final_st_prd_topic=pd.merge(df_final_st_prd_topic.rename(columns={'original_search_query':'modified_search_query'}),df_osq_msq,on=['modified_search_query'],how='left')
#     df_final_st_prd_topic=df_final_st_prd_topic[['original_search_query','modified_search_query','product_id','topic_brand','topic_category','topic_entity','product_brand','product_category','product_entity','topic_total_value','product_st_value','topic_product_value_diff','token_set_ratio','token_set_ratio_diff','final_diff_value','final_diff_value_max_group_by_os','nor_final_diff_value','relevance_bucket','product_name']]
#     df_final_st_prd_topic['updated_timestamp']=current_timestamp
#     print("pushing data to topic_identification_of_query_delta....")
#     #import_data_in_chunks(df_final_st_prd_topic,100000,"search_term_product_relevance_bucket","warehouse")

