#!/usr/bin/env python
# coding: utf-8

# In[1]:


print("importing required libraries")
import pandas as pd
import numpy as np
import time
import sys,os,json
sys.path.append(os.environ['DATA_SCIENCE_HOME'])
import helper.mysql.get_connection  as SQL
from datetime import datetime, timedelta


# In[2]:


# import get_topics_using_clusters as gtc
import get_topics_using_clusters_v1 as gtc_v1

min_view =sys.argv[2]
max_view =sys.argv[3]
# In[3]:

from datetime import timedelta
import pytz
TZ = "Asia/Kolkata"
midnight = int(datetime.now(pytz.timezone(TZ)).replace(hour=0, minute=0, second=0, microsecond=0).timestamp())
yesterday_midnight = midnight - 86400

query_type =sys.argv[1]
if query_type == 'user_data_topics':
	print("training user_data_topics")
	query="""
	select distinct original_search_query
	from topic_identification_of_query_raw_data where updated_timestamp > yesterday_midnight
	"""
	df_dev_queries=SQL.fetch_data_with_schema(query,"warehouse")
	df_dev_queries = df_dev_queries[int(min_view):int(max_view)]
	print("training for started->>",df_dev_queries.shape)
	df_dev_queries.apply(lambda x: gtc_v1.get_final_topics(x['original_search_query']), axis=1)
else:
	print("training for last 1 day queries")
	query = """select wc.page_type_value as original_search_query,count(distinct wc.identifier) as num_of_view_users,count(*) as num_of_views
	from datos_pocos.page_view as wc
	where wc.event_timestamp between timestamp(DATE_ADD(CURRENT_DATE(), INTERVAL -365 DAY)) and TIMESTAMP(FORMAT_DATE("%Y-%m-%d",CURRENT_DATE()))
	and wc.page_type in ('listing_search') 
	group by page_type_value
	having num_of_views > {} and num_of_views < {}
	order by num_of_views desc
"""
	df_dev_queries=SQL.fetch_data_with_schema(query.format(min_view,max_view),"bigquery")
	print("training for started->>",df_dev_queries.shape)
	df_dev_queries.apply(lambda x: gtc_v1.get_final_topics(x['original_search_query']), axis=1)









