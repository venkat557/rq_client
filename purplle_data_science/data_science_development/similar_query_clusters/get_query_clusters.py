#!/usr/bin/env python
# coding: utf-8

# In[2]:


print("importing required libraries")
import pandas as pd
import numpy as np
import time
import sys,os
sys.path.append(os.environ['DATA_SCIENCE_HOME'])
import helper.mysql.get_connection  as SQL
from datetime import datetime, timedelta
# import helper.lib.utilities as UT
import re
from numpy import asarray
#import tensorflow_hub as hub
from fuzzywuzzy import fuzz 
from fuzzywuzzy import process
from flashtext.keyword import KeywordProcessor
import nltk
import redis
import requests


# In[3]:


start_time=int(time.time())

def get_redis_connection():
    REDIS_HOST=os.environ['REDIS_HOST']
    REDIS_PORT=os.environ['REDIS_PORT']
    redis_connection_object=redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT)
    try:
        redis_connection_object.ping()
        print("Successfully connected to redis")
    except (redis.exceptions.ConnectionError, ConnectionRefusedError):
        print("Redis connection error!")

    return redis_connection_object
redis_connection_object = get_redis_connection()

# In[4]:


# entity-entity group, category,brand details function...
def get_ev_details():
    query_ev_details="""
    select ev.id as entity_value_id,lower(ev.entity_value) as entity_value,ev.entity_group_id,lower(eg.name) as entity_group
    from entity_values ev
    join entity_groups eg on ev.entity_group_id=eg.id
    """
    df_ev_details=SQL.fetch_data_with_schema(query_ev_details,"slave")

    query_cat="""
    select category_id as entity_value_id,lower(category_name) as entity_value,-2 as entity_group_id,'category' as entity_group
    from product_master
    group by category_id
    """
    df_cat=SQL.fetch_data_with_schema(query_cat,"warehouse")

    query_brand="""
    select brand_id as entity_value_id,lower(brand_name) as entity_value,-3 as entity_group_id,'brand' as entity_group
    from product_master
    group by brand_id
    """
    df_brand=SQL.fetch_data_with_schema(query_brand,"warehouse")
    
    df_ev_details=df_ev_details.append([df_cat,df_brand],ignore_index=True)
    df_ev_details['entity_value']=df_ev_details['entity_value'].str.lower()
    #df_ev_details['entity_value']=df_ev_details['entity_value'].str.replace('&','and')
    df_ev_details=df_ev_details[~df_ev_details.entity_value.isnull()]
    return df_ev_details


# In[5]:


#df_ev_details=df_ev_details.query("entity_group!='range'")
df_ev_details=get_ev_details()
list_entity_values=set(df_ev_details['entity_value'].unique())


# In[6]:


query_dev_queries="""
select wc.page_type_value as original_search_query,count(distinct wc.identifier) as num_of_view_users,count(*) as num_of_views
from datos_pocos.page_view as wc
where wc.event_timestamp between timestamp(DATE_ADD(CURRENT_DATE(), INTERVAL -365 DAY)) and TIMESTAMP(FORMAT_DATE("%Y-%m-%d",CURRENT_DATE()))
and wc.page_type in ('listing_search')
group by page_type_value
having num_of_views > 1
"""
df_dev_queries=SQL.fetch_data_with_schema(query_dev_queries,"bigquery")

df_dev_queries['original_search_query']=df_dev_queries['original_search_query'].str.lower()
df_dev_queries=df_dev_queries[~df_dev_queries['original_search_query'].isnull()]
df_dev_queries=df_dev_queries.sort_values('num_of_views',ascending=False)




df_dev_queries['redis_key']="ds_"+df_dev_queries['original_search_query']
df_dev_queries['redis_result']=df_dev_queries['redis_key'].apply(lambda x:redis_connection_object.get(x))


# In[10]:


df_dev_queries=df_dev_queries[~df_dev_queries['redis_result'].isnull()].copy()
df_dev_queries['redis_result']=df_dev_queries['redis_result'].apply(lambda x:eval(x))


# In[11]:


df_dev_queries['final_close_matches']=df_dev_queries['redis_result'].apply(lambda x:set(x['list_matched_entities_from_query']+x['list_matched_entities_from_unknowns']+x['list_matched_entities_from_present_entities']))
df_dev_queries['final_close_matches']=df_dev_queries['final_close_matches'].apply(lambda x:[y[0] for y in x])
df_dev_queries['final_close_matches']=df_dev_queries['final_close_matches'].apply(lambda x:list(set(x).intersection(list_entity_values)))
df_dev_queries['final_close_matches']=df_dev_queries['final_close_matches'].apply(lambda x:sorted(x))


# In[15]:


df_final_clusters=df_dev_queries.groupby(df_dev_queries['final_close_matches'].map(tuple))['original_search_query'].apply(list).reset_index()
df_final_clusters=df_final_clusters.query("final_close_matches!=tuple()")
df_final_clusters['query_cluster']=df_final_clusters['original_search_query'].copy()
df_final_clusters=df_final_clusters.explode('original_search_query')
df_final_clusters['query_clusters'] = df_final_clusters.query_cluster.apply(lambda x: ', '.join([str(i) for i in x]))


# In[16]:


df_final_clusters = df_final_clusters[['original_search_query','query_cluster','query_clusters']]


# In[25]:


def importDataInChuncks(df_data,max_rows,db,table_name):
    end_date = datetime.today() - timedelta(days=0)
    total_count=df_data.shape[0]
    total_splits=int(total_count/max_rows)+1
    df_splits = np.array_split(df_data, total_splits)
    result=True
    count =1
    for df_chunk in df_splits :
        print("pushing chunk :"+db+" in table :"+table_name+".....",str(count)+'/'+str(total_splits))
        SQL.insert_dataframe_to_db(df_chunk,db,table_name)
        count =count+1
    return result


# In[28]:


importDataInChuncks(df_final_clusters,1000,'warehouse','query_clusters_lifetime')


# In[ ]:





# -----
# -----

# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:




