#!/usr/bin/env python
# coding: utf-8

# In[ ]:


print("importing required libraries")
import pandas as pd
import numpy as np
import time
import sys,os
sys.path.append(os.environ['DATA_SCIENCE_HOME'])
import helper.mysql.get_connection  as SQL
from datetime import datetime, timedelta
# import helper.lib.utilities as UT
import re
from fuzzywuzzy import fuzz 
from fuzzywuzzy import process
from flashtext.keyword import KeywordProcessor
import nltk
import redis
nltk.download('stopwords')
start_time=int(time.time())


def get_redis_connection():
    REDIS_HOST=os.environ['REDIS_HOST']
    REDIS_PORT=os.environ['REDIS_PORT']
    redis_connection_object=redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT)
    try:
        redis_connection_object.ping()
        print("Successfully connected to redis")
    except (redis.exceptions.ConnectionError, ConnectionRefusedError):
        print("Redis connection error!")

    return redis_connection_object
redis_connection_object = get_redis_connection()
#exit()
# entity-entity group, category,brand details function...
def get_ev_details():
    query_ev_details="""
    select ev.id as entity_value_id,lower(ev.entity_value) as entity_value,ev.entity_group_id,lower(eg.name) as entity_group
    from entity_values ev
    join entity_groups eg on ev.entity_group_id=eg.id
    """
    df_ev_details=SQL.fetch_data_with_schema(query_ev_details,"slave")

    query_cat="""
    select category_id as entity_value_id,lower(category_name) as entity_value,-2 as entity_group_id,'category' as entity_group
    from product_master
    group by category_id
    """
    df_cat=SQL.fetch_data_with_schema(query_cat,"warehouse")

    query_brand="""
    select brand_id as entity_value_id,lower(brand_name) as entity_value,-3 as entity_group_id,'brand' as entity_group
    from product_master
    group by brand_id
    """
    df_brand=SQL.fetch_data_with_schema(query_brand,"warehouse")
    
    df_ev_details=df_ev_details.append([df_cat,df_brand],ignore_index=True)
    df_ev_details['entity_value']=df_ev_details['entity_value'].str.lower()
    #df_ev_details['entity_value']=df_ev_details['entity_value'].str.replace('&','and')
    df_ev_details=df_ev_details[~df_ev_details.entity_value.isnull()]
    return df_ev_details

#newly added code
#df_ev_details=df_ev_details.query("entity_group!='range'")
df_ev_details=get_ev_details()

df_ev_details['entity_value']=df_ev_details['entity_value'].str.replace("'","")
df_ev_details=df_ev_details.query("entity_group!='color code'")
list_entity_values_without_ranges=list(df_ev_details.query("entity_group!='range'")['entity_value'].unique())
list_entity_values_with_ranges=list(df_ev_details['entity_value'].unique())
df_ev_details_with_ranges=df_ev_details.copy()
df_ev_details=df_ev_details.query("entity_group!='range'")

# getting aliases and removing aliases which are exactly same as other entities
print("getting aliases and removing aliases which are exactly same as other entities")
query_aliases="""
select lower(ea.alias_name) as alias_name,lower(ev.entity_value) as entity_value
from entity_aliases as ea
join entity_values as ev on ea.module_id=ev.id
where ea.module='entity_value'
and ea.alias_name!=ev.entity_value
group by alias_name,entity_value
UNION
select lower(ea.alias_name) as alias_name,lower(ev.name) as entity_value
from entity_aliases as ea
join product_brand as ev on ea.module_id=ev.id
where ea.module='brand'
and ea.alias_name!=ev.name
group by alias_name,entity_value
UNION
select lower(ea.alias_name) as alias_name,lower(ev.name) as entity_value
from entity_aliases as ea
join product_category as ev on ea.module_id=ev.id
where ea.module='category'
and ea.alias_name!=ev.name
group by alias_name,entity_value
"""
df_aliases=SQL.fetch_data_with_schema(query_aliases,"slave")
df_aliases=df_aliases.query("alias_name!='face'")

query_root_cats="""
select lower(category_root_slug) as category_root_slug
from product_master
where category_root_slug!='' and category_root_slug!='0'
group by category_root_slug
"""
df_root_cats=SQL.fetch_data_with_schema(query_root_cats,"warehouse")

alias_entity_collision_list=list(df_ev_details['entity_value'].unique())+list(df_root_cats['category_root_slug'].unique())
df_aliases=df_aliases.query("alias_name not in @alias_entity_collision_list")

df_custom_aliases=df_ev_details[['entity_value']].copy()
df_custom_aliases['alias_name']=df_custom_aliases['entity_value'].str.replace(" ","")
df_custom_aliases=df_custom_aliases.query("entity_value!=alias_name").copy()
df_custom_aliases=df_custom_aliases[['alias_name','entity_value']].copy()
df_aliases=df_aliases.append(df_custom_aliases,ignore_index=True)

# existing product tagging function
def get_product_ranges():
    query_old_tagging="""
    select module_id as product_id,ev.id as entity_value_id,lower(ev.entity_value) as entity_value,ev.entity_group_id as entity_group_id,lower(eg.name) as entity_group
    from entity_products as ep
    join  entity_values ev on ev.id=ep.entity_value_id
    join entity_groups eg on ev.entity_group_id=eg.id
    where module='product'
    and lower(eg.name)='range'
    """
    df_old_tagging=SQL.fetch_data_with_schema(query_old_tagging,"slave")
    
    query_pm="""
    select product_id,lower(category_name) as category_name,lower(brand_name) as brand_name
    from product_master
    group by product_id
    """
    df_pm=SQL.fetch_data_with_schema(query_pm,"warehouse")
    
    df_old_tagging=pd.merge(df_old_tagging,df_pm,on=['product_id'],how='left')
    df_old_tagging['entity_value']=df_old_tagging['entity_value'].str.lower()
    df_old_tagging=df_old_tagging[~df_old_tagging.entity_value.isnull()]
    df_old_tagging=df_old_tagging.drop_duplicates()
    return df_old_tagging

df_ranges=get_product_ranges()
df_ranges=df_ranges.drop_duplicates(subset=['brand_name','category_name','entity_value'])[['brand_name','category_name','entity_value']]

#newly added code
df_ranges['entity_value']=df_ranges['entity_value'].str.replace("'","")
df_ranges['brand_name']=df_ranges['brand_name'].str.replace("'","")
df_ranges['category_name']=df_ranges['category_name'].str.replace("'","")
df_ranges['entity_value']=df_ranges.apply(lambda x:x['entity_value'].replace(str(x['brand_name']),""),axis=1)
df_ranges['entity_value']=df_ranges.apply(lambda x:x['entity_value'].replace(str(x['category_name']),""),axis=1)
df_ranges['entity_value']=df_ranges['entity_value'].str.strip()

list_brand_entity_values=list(df_ev_details.query("entity_group=='brand'")['entity_value'].unique())
list_category_entity_values=list(df_ev_details.query("entity_group=='category'")['entity_value'].unique())

df_custom_stop_words=df_ev_details[['entity_value']].copy()
df_custom_stop_words['entity_value_words']=df_custom_stop_words['entity_value'].str.split()
df_custom_stop_words=df_custom_stop_words.explode('entity_value_words')
df_custom_stop_words=df_custom_stop_words.groupby('entity_value_words').size().reset_index().rename(columns={0:'num_of_entitites'}).sort_values('num_of_entitites',ascending=False)
list_custom_stop_words=list(df_custom_stop_words.query("num_of_entitites>5")['entity_value_words'].unique())

df_matched_entities=pd.read_csv("/purplle_data_science/data_science_development/similar_query_clusters/data/entity_entity_match.csv")
del df_matched_entities['Unnamed: 0']
df_matched_entities['matched_entities']=df_matched_entities['matched_entities'].apply(lambda x:eval(x))
df_matched_entities=df_matched_entities.groupby("entity_value")['matched_entities'].apply(list).reset_index()

df_matched_entities=df_matched_entities.query("entity_value not in @list_custom_stop_words")
dict_matched_entities=df_matched_entities.set_index('entity_value').to_dict()['matched_entities']

# keyword_processor_to_replace_entities
print("keyword_processor_to_replace_aliases")
from flashtext.keyword import KeywordProcessor
keyword_processor_to_replace_aliases = KeywordProcessor()
gc_var=df_aliases.apply(lambda x:keyword_processor_to_replace_aliases.add_keyword(x['alias_name'],x['entity_value']),axis=1)

# keyword_processor_to_replace_entities
print("keyword_processor_to_replace_entities")
from flashtext.keyword import KeywordProcessor
keyword_processor_to_replace_entities = KeywordProcessor()
gc_var=df_ev_details.apply(lambda x:keyword_processor_to_replace_entities.add_keyword(x['entity_value']," "),axis=1)

# keyword_processor_to_extract_entities
print("keyword_processor_to_extract_entities")
from flashtext.keyword import KeywordProcessor
keyword_processor_to_extract_entities = KeywordProcessor()
gc_var=df_ev_details.apply(lambda x:keyword_processor_to_extract_entities.add_keyword(x['entity_value']),axis=1)

list_root_cats=list(df_root_cats['category_root_slug'].unique())
list_eng_stop_words=list(set(nltk.corpus.stopwords.words('english')))
list_stop_words=set(list_custom_stop_words+['for','anti','a','an','combo','of','in','face','and','&','hair','care','products','-','(',')','@','dark','control','sets']+list_root_cats+list_eng_stop_words)

def vecs_custom_with_ranges(input_query):
    try:
        print(input_query)
        if(redis_connection_object.exists("ds_"+input_query)):
            list_matched_entities_from_query=list_matched_entities_from_unknowns=list_matched_entities_from_present_entities=[]
            already_exist=True
            print("exist")
        else:
            already_exist=False
            input_query=keyword_processor_to_replace_aliases.replace_keywords(input_query)
            list_unknown_words=keyword_processor_to_replace_entities.replace_keywords(input_query).split()
            list_unknown_words=set(list_unknown_words)-list_stop_words
            list_present_entities=keyword_processor_to_extract_entities.extract_keywords(input_query)
            #print(list_unknown_words,list_present_entities)
            len_sentence=len(input_query)
            list_matched_entities_from_query=list_matched_entities_from_unknowns=list_matched_entities_from_present_entities=[]
            list_matched_entities_from_query=process.extractBests(input_query,list_entity_values_without_ranges,scorer=fuzz.token_set_ratio,score_cutoff=90,limit=50)
            if len_sentence>1:
                for each_unknown in list_unknown_words:
                    list_matched_entities_from_unknowns=list_matched_entities_from_unknowns+process.extractBests(each_unknown,list_entity_values_without_ranges,scorer=fuzz.token_set_ratio,score_cutoff=85,limit=50)
                for each_present_entitiy in list_present_entities:
                    #list_matched_entities_from_present_entities=list_matched_entities_from_present_entities+process.extractBests(each_present_entitiy,list_entity_values_without_ranges,scorer=fuzz.token_set_ratio,score_cutoff=85,limit=50)
                    if each_present_entitiy in dict_matched_entities:
                        list_matched_entities_from_present_entities=list_matched_entities_from_present_entities+dict_matched_entities[each_present_entitiy]

            list_final_entities=list_matched_entities_from_query+list_matched_entities_from_unknowns+list_matched_entities_from_present_entities

            set_final_entities_for_range=set([x[0] for x in  list_final_entities])
            list_brand_matches=list_category_matches=[]
            list_brand_matches= list(set_final_entities_for_range & set(list_brand_entity_values))
            list_category_matches= list(set_final_entities_for_range & set(list_category_entity_values))

            list_ranges_to_consider=[]
            if list_brand_matches!=[] and list_category_matches!=[]:
                 list_ranges_to_consider=df_ranges.query("brand_name in @list_brand_matches and category_name in @list_category_matches")['entity_value'].drop_duplicates().tolist()
            elif list_brand_matches!=[] and list_category_matches==[]:
                 list_ranges_to_consider=df_ranges.query("brand_name in @list_brand_matches")['entity_value'].drop_duplicates().tolist()
            elif list_brand_matches==[] and list_category_matches!=[]:
                 list_ranges_to_consider=df_ranges.query("category_name in @list_category_matches")['entity_value'].drop_duplicates().tolist()     

            if list_ranges_to_consider!=[]:
                list_matched_entities_from_query=list_matched_entities_from_query+process.extractBests(input_query,list_ranges_to_consider,scorer=fuzz.token_set_ratio,score_cutoff=90,limit=50)
                for each_unknown in list_unknown_words:
                    list_matched_entities_from_unknowns=list_matched_entities_from_unknowns+process.extractBests(each_unknown,list_ranges_to_consider,scorer=fuzz.token_set_ratio,score_cutoff=80,limit=50)

    except:
        already_exist=False
        print("exception for "+input_query)
        list_matched_entities_from_query=list_matched_entities_from_unknowns=list_matched_entities_from_present_entities=[]
    return {'already_exist':already_exist,'list_matched_entities_from_query':list_matched_entities_from_query,'list_matched_entities_from_unknowns':list_matched_entities_from_unknowns,'list_matched_entities_from_present_entities':list_matched_entities_from_present_entities}


def vecs_custom_with_ranges_old(input_query):
    try:
        input_query=keyword_processor_to_replace_aliases.replace_keywords(input_query)
        list_unknown_words=keyword_processor_to_replace_entities.replace_keywords(input_query).split()
        list_unknown_words=set(list_unknown_words)-list_stop_words
        list_present_entities=keyword_processor_to_extract_entities.extract_keywords(input_query)
        #print(list_unknown_words,list_present_entities)
        len_sentence=len(input_query)
        list_matched_entities_from_query=list_matched_entities_from_unknowns=list_matched_entities_from_present_entities=[]
        list_matched_entities_from_query=process.extractBests(input_query,list_entity_values_without_ranges,scorer=fuzz.token_set_ratio,score_cutoff=90,limit=50)
        if len_sentence>1:
            for each_unknown in list_unknown_words:
                list_matched_entities_from_unknowns=list_matched_entities_from_unknowns+process.extractBests(each_unknown,list_entity_values_without_ranges,scorer=fuzz.token_set_ratio,score_cutoff=85,limit=50)
            for each_present_entitiy in list_present_entities:
                #list_matched_entities_from_present_entities=list_matched_entities_from_present_entities+process.extractBests(each_present_entitiy,list_entity_values_without_ranges,scorer=fuzz.token_set_ratio,score_cutoff=85,limit=50)
                if each_present_entitiy in dict_matched_entities:
                    list_matched_entities_from_present_entities=list_matched_entities_from_present_entities+dict_matched_entities[each_present_entitiy]
        
        list_final_entities=list_matched_entities_from_query+list_matched_entities_from_unknowns+list_matched_entities_from_present_entities

        set_final_entities_for_range=set([x[0] for x in  list_final_entities])
        list_brand_matches=list_category_matches=[]
        list_brand_matches= list(set_final_entities_for_range & set(list_brand_entity_values))
        list_category_matches= list(set_final_entities_for_range & set(list_category_entity_values))

        list_ranges_to_consider=[]
        if list_brand_matches!=[] and list_category_matches!=[]:
             list_ranges_to_consider=df_ranges.query("brand_name in @list_brand_matches and category_name in @list_category_matches")['entity_value'].drop_duplicates().tolist()
        elif list_brand_matches!=[] and list_category_matches==[]:
             list_ranges_to_consider=df_ranges.query("brand_name in @list_brand_matches")['entity_value'].drop_duplicates().tolist()
        elif list_brand_matches==[] and list_category_matches!=[]:
             list_ranges_to_consider=df_ranges.query("category_name in @list_category_matches")['entity_value'].drop_duplicates().tolist()     

        if list_ranges_to_consider!=[]:
            list_matched_entities_from_query=list_matched_entities_from_query+process.extractBests(input_query,list_ranges_to_consider,scorer=fuzz.token_set_ratio,score_cutoff=90,limit=50)
            for each_unknown in list_unknown_words:
                list_matched_entities_from_unknowns=list_matched_entities_from_unknowns+process.extractBests(each_unknown,list_ranges_to_consider,scorer=fuzz.token_set_ratio,score_cutoff=80,limit=50)

    except:
        print("exception for "+input_query)
        list_matched_entities_from_query=list_matched_entities_from_unknowns=list_matched_entities_from_present_entities=[]
    return {'list_matched_entities_from_query':list_matched_entities_from_query,'list_matched_entities_from_unknowns':list_matched_entities_from_unknowns,'list_matched_entities_from_present_entities':list_matched_entities_from_present_entities}

query_dev_queries="""
select wc.page_type_value as original_search_query,count(distinct wc.identifier) as num_of_view_users,count(*) as num_of_views
from datos_pocos.page_view as wc
where wc.event_timestamp between timestamp(DATE_ADD(CURRENT_DATE(), INTERVAL -365 DAY)) and TIMESTAMP(FORMAT_DATE("%Y-%m-%d",CURRENT_DATE()))
and wc.page_type in ('listing_search')
group by page_type_value
having num_of_views > {} and num_of_views < {}
"""
df_dev_queries=SQL.fetch_data_with_schema(query_dev_queries.format(sys.argv[1],sys.argv[2]),"bigquery")

df_dev_queries['original_search_query']=df_dev_queries['original_search_query'].str.lower()
df_dev_queries=df_dev_queries[~df_dev_queries['original_search_query'].isnull()].copy()
df_dev_queries=df_dev_queries.sort_values('num_of_views',ascending=False)


def applying_algo_in_chunks(df_data,max_rows):
    total_count=df_data.shape[0]
    total_splits=int(total_count/max_rows)+1
    df_splits = np.array_split(df_data, total_splits)
    result=True
    count=1
    df_final_result=pd.DataFrame()
    for df_chunk in df_splits :
        print("applying algo for chunk"+str(count))
        df_chunk['modified_search_query']=df_chunk['original_search_query'].str.replace("'","")
        df_chunk['close_matches']=df_chunk['modified_search_query'].apply(lambda x:vecs_custom_with_ranges(x))
        df_chunk['redis_key']="ds_"+df_chunk['original_search_query']
        df_chunk['redis_response']=df_chunk.apply(lambda x:redis_connection_object.set(x['redis_key'],str(x['close_matches'])) if x['close_matches']['already_exist'] == False else True,axis=1)
        #df_chunk['redis_response']=df_chunk.apply(lambda x:redis_connection_object.set(x['redis_key'],str(x['close_matches'])),axis=1)
        #df_final_result=df_final_result.append(df_chunk,ignore_index=True)
        count=count+1
    return df_final_result

print(df_dev_queries.copy().shape)

df_final_result=applying_algo_in_chunks(df_dev_queries.copy(),1000)

end_time=int(time.time())
print("completed in "+str(end_time-start_time)+" seconds...")


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# -----
# -----

# In[ ]:


# def vecs_custom(input_query):
#     try:
#         input_query=keyword_processor_to_replace_aliases.replace_keywords(input_query)
#         list_unknown_words=keyword_processor_to_replace_entities.replace_keywords(input_query).split()
#         list_unknown_words=set(list_unknown_words)-list_stop_words
#         list_present_entities=keyword_processor_to_extract_entities.extract_keywords(input_query)
#         #print(list_unknown_words,list_present_entities)
#         len_sentence=len(input_query)
#         list_matched_entities_from_query=list_matched_entities_from_unknowns=list_matched_entities_from_present_entities=[]
#         list_matched_entities_from_query=process.extractBests(input_query,list_entity_values,scorer=fuzz.token_set_ratio,score_cutoff=90,limit=50)
#         if len_sentence>1:
#             for each_unknown in list_unknown_words:
#                 list_matched_entities_from_unknowns=list_matched_entities_from_unknowns+process.extractBests(each_unknown,list_entity_values,scorer=fuzz.token_set_ratio,score_cutoff=80,limit=50)
#             for each_present_entitiy in list_present_entities:
#                 #list_matched_entities_from_present_entities=list_matched_entities_from_present_entities+process.extractBests(each_present_entitiy,list_entity_values,scorer=fuzz.token_set_ratio,score_cutoff=85,limit=50)
#                 if each_present_entitiy in dict_matched_entities:
#                     list_matched_entities_from_present_entities=list_matched_entities_from_present_entities+dict_matched_entities[each_present_entitiy]
#             #list_final_entities=list_matched_entities_from_query+list_matched_entities_from_unknowns+list_matched_entities_from_present_entities
#     except:
#         print("exception for "+input_query)
#         list_matched_entities_from_query=list_matched_entities_from_unknowns=list_matched_entities_from_present_entities=[]
#     return {'list_matched_entities_from_query':list_matched_entities_from_query,'list_matched_entities_from_unknowns':list_matched_entities_from_unknowns,'list_matched_entities_from_present_entities':list_matched_entities_from_present_entities}


# In[ ]:





# In[ ]:





# In[ ]:




