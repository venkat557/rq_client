#!/usr/bin/env python
# coding: utf-8

# In[ ]:


#!/usr/bin/env python
# coding: utf-8

# In[31]:


print("importing required libraries")
import faiss
import pandas as pd
import numpy as np
import time
import sys,os
sys.path.append(os.environ['DATA_SCIENCE_HOME'])
import helper.mysql.get_connection  as SQL
from datetime import datetime, timedelta
# import helper.lib.utilities as UT
import re
from numpy import asarray
#import tensorflow_hub as hub
from fuzzywuzzy import fuzz 
from fuzzywuzzy import process
from flashtext.keyword import KeywordProcessor
import nltk
import redis
import requests
import ast
import swifter


def get_redis_connection():
    REDIS_HOST=os.environ['REDIS_HOST']
    REDIS_PORT=os.environ['REDIS_PORT']
    redis_connection_object=redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT)
    try:
        redis_connection_object.ping()
        print("Successfully connected to redis")
    except (redis.exceptions.ConnectionError, ConnectionRefusedError):
        print("Redis connection error!")

    return redis_connection_object
redis_connection_object = get_redis_connection()
# In[32]:

# entity-entity group, category,brand details function...
def get_ev_details():
    query_ev_details="""
    select ev.id as entity_value_id,lower(ev.entity_value) as entity_value,ev.entity_group_id,lower(eg.name) as entity_group
    from entity_values ev
    join entity_groups eg on ev.entity_group_id=eg.id
    """
    df_ev_details=SQL.fetch_data_with_schema(query_ev_details,"slave")

    query_cat="""
    select category_id as entity_value_id,lower(category_name) as entity_value,-2 as entity_group_id,'category' as entity_group
    from product_master
    group by category_id
    """
    df_cat=SQL.fetch_data_with_schema(query_cat,"warehouse")

    query_brand="""
    select brand_id as entity_value_id,lower(brand_name) as entity_value,-3 as entity_group_id,'brand' as entity_group
    from product_master
    group by brand_id
    """
    df_brand=SQL.fetch_data_with_schema(query_brand,"warehouse")
    
    df_ev_details=df_ev_details.append([df_cat,df_brand],ignore_index=True)
    df_ev_details['entity_value']=df_ev_details['entity_value'].str.lower()
    #df_ev_details['entity_value']=df_ev_details['entity_value'].str.replace('&','and')
    df_ev_details=df_ev_details[~df_ev_details.entity_value.isnull()]
    return df_ev_details


# In[34]:


#df_ev_details=df_ev_details.query("entity_group!='range'")
df_ev_details=get_ev_details()
df_ev_details=df_ev_details.query("entity_group!='color code'")
list_entity_values_without_ranges=list(df_ev_details.query("entity_group!='range'")['entity_value'].unique())
list_entity_values_with_ranges=list(df_ev_details['entity_value'].unique())
df_ev_details_with_ranges=df_ev_details.copy()
df_ev_details=df_ev_details.query("entity_group!='range'")


# In[35]:


# getting aliases and removing aliases which are exactly same as other entities
print("getting aliases and removing aliases which are exactly same as other entities")
query_aliases="""
select lower(ea.alias_name) as alias_name,lower(ev.entity_value) as entity_value
from entity_aliases as ea
join entity_values as ev on ea.module_id=ev.id
where ea.module='entity_value'
and ea.alias_name!=ev.entity_value
group by alias_name,entity_value
UNION
select lower(ea.alias_name) as alias_name,lower(ev.name) as entity_value
from entity_aliases as ea
join product_brand as ev on ea.module_id=ev.id
where ea.module='brand'
and ea.alias_name!=ev.name
group by alias_name,entity_value
UNION
select lower(ea.alias_name) as alias_name,lower(ev.name) as entity_value
from entity_aliases as ea
join product_category as ev on ea.module_id=ev.id
where ea.module='category'
and ea.alias_name!=ev.name
group by alias_name,entity_value
"""
df_aliases=SQL.fetch_data_with_schema(query_aliases,"slave")
df_aliases=df_aliases.query("alias_name!='face'")

query_root_cats="""
select lower(category_root_slug) as category_root_slug
from product_master
where category_root_slug!='' and category_root_slug!='0'
group by category_root_slug
"""
df_root_cats=SQL.fetch_data_with_schema(query_root_cats,"warehouse")

alias_entity_collision_list=list(df_ev_details['entity_value'].unique())+list(df_root_cats['category_root_slug'].unique())
df_aliases=df_aliases.query("alias_name not in @alias_entity_collision_list")

df_custom_aliases=df_ev_details[['entity_value']].copy()
df_custom_aliases['alias_name']=df_custom_aliases['entity_value'].str.replace(" ","")
df_custom_aliases=df_custom_aliases.query("entity_value!=alias_name").copy()
df_custom_aliases=df_custom_aliases[['alias_name','entity_value']].copy()
df_aliases=df_aliases.append(df_custom_aliases,ignore_index=True)


# In[36]:


query_dev_queries="""
select wc.page_type_value as original_search_query
from datos_pocos.page_view as wc
where wc.event_timestamp between timestamp(DATE_ADD(CURRENT_DATE(), INTERVAL -30 DAY)) and TIMESTAMP(FORMAT_DATE("%Y-%m-%d",CURRENT_DATE()))
and wc.page_type in ('listing_search')
group by page_type_value
"""
df_dev_queries=SQL.fetch_data_with_schema(query_dev_queries,"bigquery")

df_dev_queries['original_search_query']=df_dev_queries['original_search_query'].str.lower()
df_dev_queries=df_dev_queries[~df_dev_queries['original_search_query'].isnull()].copy()
df_dev_queries=df_dev_queries[['original_search_query']].drop_duplicates()


# ----
#                 get vectors
# ----

# In[37]:


list_entity_values_for_vectors=list_entity_values_with_ranges.copy()
zero_vector=np.zeros(len(list_entity_values_with_ranges))

def get_vectors(list_matched_entities):
#     list_matched_entities = ast.literal_eval(list_matched_entities)
    list_matched_entities=list(set(list_entity_values_for_vectors).intersection(list_matched_entities))
    indices_common = [list_entity_values_for_vectors.index(x) for x in list_matched_entities]
    input_vector=zero_vector.copy()
    input_vector[indices_common]=1
    return input_vector


# ----
#             spell corrector
# ----

# In[38]:


def spell_corrector(input_query):
    url="https://test.purplle.com/productnew/spell_correction_test?search_term={}".format(input_query)
    r = requests.get(url)
    query=r.content.decode("utf-8")
    return query


# ----
#         cosine similarity
# ----

# In[39]:


def get_cosine_similarity(v1, v2):
    mag1 = np.linalg.norm(v1)
    mag2 = np.linalg.norm(v2)
    if (not mag1) or (not mag2):
        return 0
    return np.dot(v1, v2) / (mag1 * mag2)


# In[40]:


def presents(p1,samp):
    #output = list(map(lambda x:x[0], p1))
    if(list(set(p1) - set(samp)))==[]: 
        return 1
    else:
        return 0

# ----
#             applying algo
# ----

# In[41]:


# keyword_processor_to_replace_entities
print("keyword_processor_to_replace_aliases")
from flashtext.keyword import KeywordProcessor
keyword_processor_to_replace_aliases = KeywordProcessor()
gc_var=df_aliases.apply(lambda x:keyword_processor_to_replace_aliases.add_keyword(x['alias_name'],x['entity_value']),axis=1)




# In[15]:

start_time=int(time.time())
df_dev_queries['redis_key']="ds_"+df_dev_queries['original_search_query']
df_dev_queries['redis_result']=df_dev_queries['redis_key'].apply(lambda x:redis_connection_object.get(x))
end_time=int(time.time())
print("Fetching from redis",end_time- start_time)
start_time=int(time.time())
df_dev_queries=df_dev_queries[~df_dev_queries['redis_result'].isnull()].copy()
df_dev_queries['redis_result']=df_dev_queries['redis_result'].apply(lambda x:eval(x))
df_dev_queries['final_close_matches']=df_dev_queries['redis_result'].apply(lambda x:set(x['list_matched_entities_from_query']+x['list_matched_entities_from_unknowns']+x['list_matched_entities_from_present_entities']))
df_dev_queries['final_close_matches']=df_dev_queries['final_close_matches'].apply(lambda x:[y[0] for y in x])
df_dev_queries['modified_search_query']=df_dev_queries['original_search_query'].apply(lambda x:keyword_processor_to_replace_aliases.replace_keywords(x))
df_dev_queries['modified_query_word_length']=df_dev_queries['modified_search_query'].apply(lambda x:len(x.split()))
#df_dev_queries['modified_search_query']=df_dev_queries['modified_search_query'].apply(lambda x:spell_corrector(x))
df_dev_queries['id'] = df_dev_queries.index
end_time=int(time.time())
print("processing from redis result ",end_time- start_time)
df_dev_queries_new = df_dev_queries
df_dev_queries.to_csv('redis_result.csv', index=False)

print("getting all vectors ",int(time.time()))
start_time=int(time.time())
df_dev_queries_new['vector']=df_dev_queries_new['final_close_matches'].swifter.allow_dask_on_strings(enable=True).apply(lambda x:get_vectors(x))
end_time=int(time.time())
print("getting all vectors",end_time- start_time)

start_time=int(time.time())
n_neighbors = df_dev_queries_new.shape[0]
k=n_neighbors
#  dimensionality
res = np.empty(shape=[1, 3]) #  res initialization
xb = np.array(df_dev_queries_new.vector.tolist()).astype('float32')
d=xb.shape[1] #  dimensionality
print("index addition started ",int(time.time()))
index = faiss.IndexFlatIP(d)
faiss.normalize_L2(xb)
index.add(xb)
faiss.write_index(index, 'index_result')
print("index added and saved ",(time.time())) 



# In[18]:


index.ntotal


# In[15]:


xq = np.array(df_dev_queries_new.vector.tolist()).astype('float32')


# In[16]:


xq.shape


# In[17]:


index.add(xq) #1x16384


# In[ ]:




