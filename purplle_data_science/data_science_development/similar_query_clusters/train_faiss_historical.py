#!/usr/bin/env python
# coding: utf-8

# In[ ]:


#!/usr/bin/env python
# coding: utf-8

print("importing required libraries")
import faiss
import pandas as pd
import numpy as np
import time
import sys,os
sys.path.append(os.environ['DATA_SCIENCE_HOME'])
import helper.mysql.get_connection  as SQL
from datetime import datetime, timedelta
# import helper.lib.utilities as UT
import re
from numpy import asarray
#import tensorflow_hub as hub
from fuzzywuzzy import fuzz 
from fuzzywuzzy import process
from flashtext.keyword import KeywordProcessor
import nltk
import redis
import requests
import ast
import swifter
from operator import is_not
from functools import partial
from itertools import zip_longest

def get_redis_connection():
    REDIS_HOST=os.environ['REDIS_HOST']
    REDIS_PORT=os.environ['REDIS_PORT']
    redis_connection_object=redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT)
    try:
        redis_connection_object.ping()
        print("Successfully connected to redis")
    except (redis.exceptions.ConnectionError, ConnectionRefusedError):
        print("Redis connection error!")

    return redis_connection_object
redis_connection_object = get_redis_connection()
# In[32]:

pipe = redis_connection_object.pipeline()
try:
    redis_connection_object.ping()
    print("Successfully connected to redis")
except (redis.exceptions.ConnectionError, ConnectionRefusedError):
    print("Redis connection error!")


# iterate a list in batches of size n
def batcher(iterable, n):
    args = [iter(iterable)] * n
    return zip_longest(*args)

# in batches of 500 delete keys matching user:*
t = time.time()
count=0
df = pd.DataFrame([])
for keybatch in batcher(redis_connection_object.scan_iter('ds_*'),50000):
    t2 = time.time()
    count = count+1
    keybatch = list(keybatch)
    print("keys length before ",len(keybatch))
    keybatch = list(filter(partial(is_not, None), keybatch))
    print("keys length after",len(keybatch))
    for key in list(keybatch):
        if(key is not None):
            pipe.get(key)
    vals = pipe.execute()
    d = {'redis_key':keybatch,'redis_result':vals}
    t3=time.time()
    df = df.append(pd.DataFrame(d), ignore_index=True)

    print("batch ->>"+str(count),t3-t2)
df['original_search_query'] = df['redis_key'].str[3:]
df = df.drop_duplicates('redis_key', keep='last')
print(df.shape)
t1 = time.time()
print("total time",t1-t)

# entity-entity group, category,brand details function...
def get_ev_details():
    query_ev_details="""
    select ev.id as entity_value_id,lower(ev.entity_value) as entity_value,ev.entity_group_id,lower(eg.name) as entity_group
    from entity_values ev
    join entity_groups eg on ev.entity_group_id=eg.id
    """
    df_ev_details=SQL.fetch_data_with_schema(query_ev_details,"slave")

    query_cat="""
    select category_id as entity_value_id,lower(category_name) as entity_value,-2 as entity_group_id,'category' as entity_group
    from product_master
    group by category_id
    """
    df_cat=SQL.fetch_data_with_schema(query_cat,"warehouse")

    query_brand="""
    select brand_id as entity_value_id,lower(brand_name) as entity_value,-3 as entity_group_id,'brand' as entity_group
    from product_master
    group by brand_id
    """
    df_brand=SQL.fetch_data_with_schema(query_brand,"warehouse")
    
    df_ev_details=df_ev_details.append([df_cat,df_brand],ignore_index=True)
    df_ev_details['entity_value']=df_ev_details['entity_value'].str.lower()
    #df_ev_details['entity_value']=df_ev_details['entity_value'].str.replace('&','and')
    df_ev_details=df_ev_details[~df_ev_details.entity_value.isnull()]
    return df_ev_details


# In[34]:


#df_ev_details=df_ev_details.query("entity_group!='range'")
df_ev_details=get_ev_details()
df_ev_details=df_ev_details.query("entity_group!='color code'")
list_entity_values_without_ranges=list(df_ev_details.query("entity_group!='range'")['entity_value'].unique())
list_entity_values_with_ranges=list(df_ev_details['entity_value'].unique())
df_ev_details_with_ranges=df_ev_details.copy()
df_ev_details=df_ev_details.query("entity_group!='range'")


# In[35]:


# getting aliases and removing aliases which are exactly same as other entities
print("getting aliases and removing aliases which are exactly same as other entities")
query_aliases="""
select lower(ea.alias_name) as alias_name,lower(ev.entity_value) as entity_value
from entity_aliases as ea
join entity_values as ev on ea.module_id=ev.id
where ea.module='entity_value'
and ea.alias_name!=ev.entity_value
group by alias_name,entity_value
UNION
select lower(ea.alias_name) as alias_name,lower(ev.name) as entity_value
from entity_aliases as ea
join product_brand as ev on ea.module_id=ev.id
where ea.module='brand'
and ea.alias_name!=ev.name
group by alias_name,entity_value
UNION
select lower(ea.alias_name) as alias_name,lower(ev.name) as entity_value
from entity_aliases as ea
join product_category as ev on ea.module_id=ev.id
where ea.module='category'
and ea.alias_name!=ev.name
group by alias_name,entity_value
"""
df_aliases=SQL.fetch_data_with_schema(query_aliases,"slave")
df_aliases=df_aliases.query("alias_name!='face'")

query_root_cats="""
select lower(category_root_slug) as category_root_slug
from product_master
where category_root_slug!='' and category_root_slug!='0'
group by category_root_slug
"""
df_root_cats=SQL.fetch_data_with_schema(query_root_cats,"warehouse")

alias_entity_collision_list=list(df_ev_details['entity_value'].unique())+list(df_root_cats['category_root_slug'].unique())
df_aliases=df_aliases.query("alias_name not in @alias_entity_collision_list")

df_custom_aliases=df_ev_details[['entity_value']].copy()
df_custom_aliases['alias_name']=df_custom_aliases['entity_value'].str.replace(" ","")
df_custom_aliases=df_custom_aliases.query("entity_value!=alias_name").copy()
df_custom_aliases=df_custom_aliases[['alias_name','entity_value']].copy()
df_aliases=df_aliases.append(df_custom_aliases,ignore_index=True)

# ----
#                 get vectors
# ----
list_entity_values_for_vectors=list_entity_values_with_ranges.copy()
zero_vector=np.zeros(len(list_entity_values_with_ranges))

def get_vectors(list_matched_entities):
#     list_matched_entities = ast.literal_eval(list_matched_entities)
    list_matched_entities=list(set(list_entity_values_for_vectors).intersection(list_matched_entities))
    indices_common = [list_entity_values_for_vectors.index(x) for x in list_matched_entities]
    input_vector=zero_vector.copy()
    input_vector[indices_common]=1
    return input_vector


# ----
#             spell corrector
# ----

# In[38]:


def spell_corrector(input_query):
    url="https://test.purplle.com/productnew/spell_correction_test?search_term={}".format(input_query)
    r = requests.get(url)
    query=r.content.decode("utf-8")
    return query


# ----
#         cosine similarity
# ----

# In[39]:


def get_cosine_similarity(v1, v2):
    mag1 = np.linalg.norm(v1)
    mag2 = np.linalg.norm(v2)
    if (not mag1) or (not mag2):
        return 0
    return np.dot(v1, v2) / (mag1 * mag2)


# In[40]:


def presents(p1,samp):
    #output = list(map(lambda x:x[0], p1))
    if(list(set(p1) - set(samp)))==[]: 
        return 1
    else:
        return 0

# ----
#             applying algo
# ----

# In[41]:


# keyword_processor_to_replace_entities
print("keyword_processor_to_replace_aliases")
from flashtext.keyword import KeywordProcessor
keyword_processor_to_replace_aliases = KeywordProcessor()
gc_var=df_aliases.apply(lambda x:keyword_processor_to_replace_aliases.add_keyword(x['alias_name'],x['entity_value']),axis=1)
df_dev_queries =df
start_time=int(time.time())
print("step 1",time.time())
df_dev_queries=df_dev_queries[~df_dev_queries['redis_result'].isnull()].copy()
print("step 1 done",time.time())
df_dev_queries['redis_result']=df_dev_queries['redis_result'].apply(lambda x:eval(x))
print("step 2 done",time.time())
df_dev_queries['final_close_matches']=df_dev_queries['redis_result'].apply(lambda x:set(x['list_matched_entities_from_query']+x['list_matched_entities_from_unknowns']+x['list_matched_entities_from_present_entities']))
print("step 3 done",time.time())
df_dev_queries['final_close_matches']=df_dev_queries['final_close_matches'].apply(lambda x:[y[0] for y in x])
print("step 4 done",time.time())
df_dev_queries['modified_search_query']=df_dev_queries['original_search_query'].apply(lambda x:keyword_processor_to_replace_aliases.replace_keywords(x))
print("step 4 done",time.time())
df_dev_queries['modified_query_word_length']=df_dev_queries['modified_search_query'].apply(lambda x:len(x.split()))
print("step 5 done",time.time())
#df_dev_queries['modified_search_query']=df_dev_queries['modified_search_query'].apply(lambda x:spell_corrector(x))
df_dev_queries['id'] = df_dev_queries.index
end_time=int(time.time())
print("processing from redis result ",end_time- start_time)


print("getting all vectors ",int(time.time()))
start_time=int(time.time())
df_dev_queries['vector']=df_dev_queries['final_close_matches'].apply(lambda x:get_vectors(x))
end_time=int(time.time())
print("getting all vectors",end_time- start_time)

start_time=int(time.time())
n_neighbors = df_dev_queries.shape[0]
k=n_neighbors
#  dimensionality
res = np.empty(shape=[1, 3]) #  res initialization
xb = np.array(df_dev_queries.vector.tolist()).astype('float32')
d=xb.shape[1] #  dimensionality
print("index addition started ",int(time.time()))
index = faiss.IndexFlatIP(d)
faiss.normalize_L2(xb)
index.add(xb)
faiss.write_index(index, 'faiss_index')
end_time=int(time.time())
print("index added and saved ",(time.time())) 
print("time taken ",end_time - start_time)


from gcloud import storage
from oauth2client.service_account import ServiceAccountCredentials
import os
credentials_dict = {
          "type": "service_account",
            "project_id": "datapipelineproduction",
              "private_key_id": "8521e6e33aff73b9e03fee675d8b577a9bfa0500",
                "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDNoVKoI774QaWb\nxSIaxHh/J4Zwo3GRvPzp8WeeSEkItg5uo/FhJXtavcSpWuLpmOSVvs2eDj+wsq10\nQgiba7EbDxzASU2PqPvl9ERfJfzhylmuzOy46B33L0AukHMr+lyA3m/igOjOT0V3\nmU2AJLS/8LnR9KiPHWG79lRUeA6bcS02mUIdpq2CeyVVLVPe4dv4+Cz/f4yXo6YJ\nu6pjApvVwk4RZvbfqE37Ukdna2f+YXdWIW67UE20hmlngxuN9jS1UuQjdmGj8Kfc\neGGwDZNOePjQWVE5ojaqd2usvNckfcSy3oJppyonk2S3ALezvwaWF+53E04wpz8D\naBVS0ebzAgMBAAECggEAM1RzZSxzJdaBSBaYElV7/MW5jhGGrooeGincI0tJC+ci\nSCUdgpIQLlETr/DTlttHKEg2XJ+S74FSwiQfsOhbj/UgSLGjrlpQ8xCcJqI9c4GH\nDJbz+p5aqJL/cwA098Pp5nNgO4rRUnrATQAUNAo1onotcIchsoPzmy980o8bmlpd\n1r4QBXQUFc9FGcPhPQIEpEpHvkm0MYKthg+aofGmkDgKKVP5T+nCi0eoe/030Jqy\nYF1ObmjiIep883SYA2MSABw/Rf0FWACwlk1/n++JwQKhA3OfrwjPnPL8cMRMRSpf\n1DUfvADOEDWR/ZmjGSjNJNsggNci7jC2loIrRnLlGQKBgQD0jTUUzlYM6wMl5oRN\n/a/XHPCL87cOVUFqm+g5OL0hMyLgRGSu4no0CjBQ7AC6rGapJ560Cxa/VTKQ5ahv\nhmOlVuIZcaWGhGmvKVj/99qD1lNC6Dd9fsA5mh/dqNICHYQxtlQ8tr2Mv2F/Z7nc\nSV4o9wu0VRY2tbFe3guAO21fCQKBgQDXQardUKC7GhCSMrQmH/0H+f1pAA43BkG6\n2aY1ExN3de3oUnDRvl3EXDYlJrFvMOgZGPG+AKET1Th+kaaKMrK8Itr90HFnQROu\nNKKeP6JBRwVfbrTLcxSF0aco7jWRfURHm1tB7vZyDYqofU/4HC1Urf7dlLBxDfGR\nYlLkPzkZGwKBgQCgMYed83OmzqOnRhNQy2dOMCTaaHF3MB3D6hD6utIZV1f3NJYH\ngU+vmyT/PTlVDu3IzK1HHyRtfS+8ftQRCiVn/zvokuts7HwWQ62LLx6y0ciEwXzn\n9gmRMgbBun6m6BLaRad0hfgwpIUHYjj7tgu6JepC3KHKgMaJcEZ0X3lLkQKBgQCR\nuhNC4WLvSCYP0+PCK8XRsJyuiSsWdVXqeY7cP53eMihuL+HRHV+9n+/IFvnonOOE\nmi6Owz5JfwBQL6rfxJLca3x2eLdNf/Ei7t9o4wulxb8NbJVZZv9t2RGh1CLP7E5d\na//ME0sky6YjizKoder6eREiUZdh8l6oXjhf7a1qIQKBgAK06gBMm+8qBq2Gb7Ue\nr2ddrOtiQe0yDnRekRdhtvvXbLWrjxyBz0pTfykHZzV9kiRRnoLeTvsvWf6ty5xc\nAoIUS0QknEtUdtIHIGkc52TgrzT/2ds2UF7+4aygDgD9YwXVZ7kT9PT4B48EQfhp\nuEC2hzpBorGuaIU0IxY0NMFo\n-----END PRIVATE KEY-----\n",
                  "client_email": "462171070671-compute@developer.gserviceaccount.com",
                    "client_id": "100514718544661117145",
                      "auth_uri": "https://accounts.google.com/o/oauth2/auth",
                        "token_uri": "https://accounts.google.com/o/oauth2/token",
                          "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
                            "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/462171070671-compute%40developer.gserviceaccount.com"
            }
credentials = ServiceAccountCredentials.from_json_keyfile_dict(
            credentials_dict
            )
client = storage.Client(credentials=credentials, project='datapipelineproduction')
bucket = client.get_bucket('faiss_index')
blob = bucket.blob('search_faiss_index')
blob.upload_from_filename('search_faiss_index')
