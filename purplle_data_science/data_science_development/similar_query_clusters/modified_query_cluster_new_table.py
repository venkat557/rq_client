#!/usr/bin/env python
# coding: utf-8

# In[14]:


print("importing required libraries")
import pandas as pd
import numpy as np
import time
import sys,os
sys.path.append(os.environ['DATA_SCIENCE_HOME'])
import helper.mysql.get_connection  as SQL
from datetime import datetime, timedelta
# import helper.lib.utilities as UT
import re
from numpy import asarray
#import tensorflow_hub as hub
from fuzzywuzzy import fuzz 
from fuzzywuzzy import process
from flashtext.keyword import KeywordProcessor
import nltk
import redis
import requests


# In[15]:


def get_redis_connection():
    REDIS_HOST=os.environ['REDIS_HOST']
    REDIS_PORT=os.environ['REDIS_PORT']
    redis_connection_object=redis.StrictRedis(host='10.77.0.35', port='6379')
    try:
        redis_connection_object.ping()
        print("Successfully connected to redis")
    except (redis.exceptions.ConnectionError, ConnectionRefusedError):
        print("Redis connection error!")

    return redis_connection_object
redis_connection_object = get_redis_connection()


# In[ ]:





# In[16]:


# entity-entity group, category,brand details function...
def get_ev_details():
    query_ev_details="""
    select ev.id as entity_value_id,lower(ev.entity_value) as entity_value,ev.entity_group_id,lower(eg.name) as entity_group
    from entity_values ev
    join entity_groups eg on ev.entity_group_id=eg.id
    """
    df_ev_details=SQL.fetch_data_with_schema(query_ev_details,"slave")

    query_cat="""
    select category_id as entity_value_id,lower(category_name) as entity_value,-2 as entity_group_id,'category' as entity_group
    from product_master
    group by category_id
    """
    df_cat=SQL.fetch_data_with_schema(query_cat,"warehouse")

    query_brand="""
    select brand_id as entity_value_id,lower(brand_name) as entity_value,-3 as entity_group_id,'brand' as entity_group
    from product_master
    group by brand_id
    """
    df_brand=SQL.fetch_data_with_schema(query_brand,"warehouse")
    
    df_ev_details=df_ev_details.append([df_cat,df_brand],ignore_index=True)
    df_ev_details['entity_value']=df_ev_details['entity_value'].str.lower()
    #df_ev_details['entity_value']=df_ev_details['entity_value'].str.replace('&','and')
    df_ev_details=df_ev_details[~df_ev_details.entity_value.isnull()]
    return df_ev_details


# In[ ]:





# In[17]:


def spell_corrector(input_query):
    st = int(time.time())
    url="http://10.160.0.11/productnew/spell_correction_test?search_term={}".format(input_query)
    r = requests.get(url)
    query=r.content.decode("utf-8")
    et = int(time.time())
#     print("time taken->>",str(et-st)+" =>> "+query)
    return query


# In[ ]:





# In[18]:


df_ev_details=get_ev_details()
list_entity_values=set(df_ev_details['entity_value'].unique())


# In[ ]:





# In[19]:


query_dev_queries="""
select wc.page_type_value as original_search_query,count(distinct wc.identifier) as num_of_view_users,count(*) as num_of_views
from datos_pocos.page_view as wc
where wc.event_timestamp between timestamp(DATE_ADD(CURRENT_DATE(), INTERVAL -30 DAY)) and TIMESTAMP(FORMAT_DATE("%Y-%m-%d",CURRENT_DATE()))
and wc.page_type in ('listing_search')
group by page_type_value
having num_of_views > 1
"""
df_dev_queries=SQL.fetch_data_with_schema(query_dev_queries,"bigquery")

df_dev_queries['original_search_query']=df_dev_queries['original_search_query'].str.lower()
df_dev_queries=df_dev_queries[~df_dev_queries['original_search_query'].isnull()]
df_dev_queries=df_dev_queries.sort_values('num_of_views',ascending=False)


# In[ ]:





# In[20]:


# df_dev_queries_backup = df_dev_queries
# df_dev_queries = df_dev_queries.head(1000)


# In[ ]:





# In[21]:


def split_dataframe_into_chunks(df, chunk_size): 
    chunks = list()
    num_chunks = len(df) // chunk_size + 1
    for i in range(num_chunks):
        chunks.append(df[i*chunk_size:(i+1)*chunk_size])
    return chunks


# In[ ]:





# In[22]:


dataframe_chunks = split_dataframe_into_chunks(df_dev_queries,10000)


# In[ ]:





# In[23]:


print("this data frame contains " + str(len(dataframe_chunks)) + " chunks")
chunk_count = 0
for chunk in dataframe_chunks:
    chunk_count = chunk_count + 1;
    chunk['modified_search_query']=chunk['original_search_query'].apply(lambda x:spell_corrector(x))
    print("the " + str(chunk_count)  +" chunk of data frame executed successfully")


# In[34]:


df_dev_queries = pd.concat(dataframe_chunks)


# In[ ]:





# In[35]:


df_dev_queries['redis_key']="ds_"+df_dev_queries['original_search_query']
df_dev_queries['redis_result']=df_dev_queries['redis_key'].apply(lambda x:redis_connection_object.get(x))


# In[ ]:





# In[36]:


df_dev_queries=df_dev_queries[~df_dev_queries['redis_result'].isnull()].copy()
df_dev_queries['redis_result']=df_dev_queries['redis_result'].apply(lambda x:eval(x))


# In[ ]:





# In[37]:


df_dev_queries['final_close_matches']=df_dev_queries['redis_result'].apply(lambda x:set(x['list_matched_entities_from_query']+x['list_matched_entities_from_unknowns']+x['list_matched_entities_from_present_entities']))
df_dev_queries['final_close_matches']=df_dev_queries['final_close_matches'].apply(lambda x:[y[0] for y in x])
df_dev_queries['final_close_matches']=df_dev_queries['final_close_matches'].apply(lambda x:list(set(x).intersection(list_entity_values)))
df_dev_queries['final_close_matches']=df_dev_queries['final_close_matches'].apply(lambda x:sorted(x))


# In[ ]:





# In[38]:


df_dev_queries['final_close_matches'] = df_dev_queries['final_close_matches'].map(tuple)


# In[ ]:





# In[39]:


df_clusters=df_dev_queries.groupby(['final_close_matches'])


# In[ ]:





# In[40]:


df_clusters_modified_queries = df_clusters['modified_search_query'].apply(list).reset_index()


# In[ ]:





# In[41]:


df_clusters_original_queries = df_clusters['original_search_query'].apply(list).reset_index()


# In[ ]:





# In[42]:


df_clusters_num_of_views  = df_clusters['num_of_views'].apply(list).reset_index()


# In[ ]:





# In[43]:


df_clusters_final = df_clusters_modified_queries


# In[ ]:





# In[44]:


df_clusters_final['original_search_query'] = df_clusters_original_queries['original_search_query']


# In[ ]:





# In[45]:


df_clusters_final['num_of_views'] = df_clusters_num_of_views['num_of_views']


# In[ ]:





# In[46]:


df_clusters_final=df_clusters_final.query("final_close_matches!=tuple()")


# In[ ]:





# In[47]:


df_clusters_final['query_cluster']=df_clusters_final['modified_search_query'].copy()


# In[ ]:





# In[48]:


df_clusters_final = df_clusters_final.drop(['final_close_matches'], axis=1)


# In[ ]:





# In[ ]:





# In[49]:


df_clusters_final= df_clusters_final.set_index(['query_cluster']).apply(pd.Series.explode).reset_index()


# In[ ]:





# In[ ]:





# In[50]:


df_clusters_final['modified_queries_cluster'] = df_clusters_final.query_cluster.apply(lambda x: ', '.join([str(i) for i in x]))


# In[ ]:





# In[ ]:





# In[51]:


df_clusters_final_columns = df_clusters_final[['original_search_query','modified_search_query','modified_queries_cluster','num_of_views']]


# In[ ]:





# In[52]:


df_clusters_final_columns = df_clusters_final_columns.drop_duplicates()


# In[ ]:





# In[ ]:





# In[ ]:





# In[53]:


def importDataInChuncks(df_data,max_rows,db,table_name):
    end_date = datetime.today() - timedelta(days=0)
    total_count=df_data.shape[0]
    total_splits=int(total_count/max_rows)+1
    df_splits = np.array_split(df_data, total_splits)
    result=True
    count =1
    for df_chunk in df_splits :
        print("pushing chunk :"+db+" in table :"+table_name+".....",str(count)+'/'+str(total_splits))
        SQL.insert_dataframe_to_db(df_chunk,db,table_name)
        count =count+1
    return result


# In[ ]:





# In[56]:


importDataInChuncks(df_clusters_final_columns,1000,'warehouse','query_cluster_modified_30_days')


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:




