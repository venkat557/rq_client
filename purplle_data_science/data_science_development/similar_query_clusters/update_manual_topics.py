#!/usr/bin/env python
# coding: utf-8

# In[1]:


print("importing required libraries")
print("importing required libraries")
import pandas as pd
import numpy as np
import time
import sys,os
sys.path.append(os.environ['DATA_SCIENCE_HOME'])
import helper.mysql.get_connection  as SQL
from datetime import datetime, timedelta
import re
import time
from fuzzywuzzy import fuzz
import get_topics_using_clusters as gtc

# In[2]:


def get_ev_details():
    query_ev_details="""
    select ev.id as entity_value_id,lower(ev.entity_value) as entity_value,ev.entity_group_id,lower(eg.name) as entity_group
    from entity_values ev
    join entity_groups eg on ev.entity_group_id=eg.id
    """
    df_ev_details=SQL.fetch_data_with_schema(query_ev_details,"slave")

    query_cat="""
    select id as entity_value_id,lower(name) as entity_value,-2 as entity_group_id,'category' as entity_group
    from product_category
    group by entity_value_id
    """
    df_cat=SQL.fetch_data_with_schema(query_cat,"slave")

    query_brand="""
    select id as entity_value_id,lower(name) as entity_value,-3 as entity_group_id,'brand' as entity_group
    from product_brand
    group by entity_value_id
    """
    df_brand=SQL.fetch_data_with_schema(query_brand,"slave")

    df_ev_details=df_ev_details.append([df_cat,df_brand],ignore_index=True)
    df_ev_details['entity_value']=df_ev_details['entity_value'].str.lower()
    #df_ev_details['entity_value']=df_ev_details['entity_value'].str.replace('&','and')
    df_ev_details=df_ev_details[~df_ev_details.entity_value.isnull()]
    return df_ev_details

print("getting entity details")
df_ev=get_ev_details()
df_entity_values=df_ev[['entity_value','entity_group']].drop_duplicates()
df_entity_values['str_len']=df_entity_values.entity_value.str.len()
df_entity_values=df_entity_values.sort_values('str_len',ascending=False)


# In[3]:


def get_manual_topics(sheet_name,sub_sheet_name):
    import gspread
    import gspread_dataframe as gd
    from oauth2client.service_account import ServiceAccountCredentials

    # use creds to create a client to interact with the Google Drive API
    #scope = ['https://spreadsheets.google.com/feeds']
    scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
    creds = ServiceAccountCredentials.from_json_keyfile_name('/purplle_data_science/helper/credential/client_secret.json', scope)
    client = gspread.authorize(creds)

    sheet_getting_worksheet_names = client.open(sheet_name)
    worksheet_input=sheet_getting_worksheet_names.worksheet(sub_sheet_name)
    input_data = worksheet_input.get_all_records()

    df_brand_ids_for_filter=pd.DataFrame(input_data)
    return df_brand_ids_for_filter


print("getting manual topics")
sheet_name='manual_topics'
sub_sheet_name='manual_topics_info'
df_manual_topics=get_manual_topics(sheet_name,sub_sheet_name)
df_manual_topics.apply(lambda x: gtc.get_final_topics(x['original_search_query']), axis=1)