#!/usr/bin/env python
# coding: utf-8

# In[1]:


print("importing required libraries")
import pandas as pd
import numpy as np
import time
import sys,os,json
sys.path.append(os.environ['DATA_SCIENCE_HOME'])
import helper.mysql.get_connection  as SQL
from datetime import datetime, timedelta
# import helper.lib.utilities as UT
import re
from numpy import asarray
#import tensorflow_hub as hub
from fuzzywuzzy import fuzz 
from fuzzywuzzy import process
from flashtext.keyword import KeywordProcessor
import nltk
import redis
import requests
nltk.download('stopwords')
import itertools
# In[2]:


start_time=int(time.time())
list_exact_brand_exceptions=["Khadi",'Iba']

list_brand_stop_words=[
"Clay",
"Bio",
"organic",
"Beauty",
"Nature",
"Herbal",
"Tea",
"love",
"Wash",
"Plus",
"Fair",
"Lovely",
"Acnes",
"Set",
"Wet",
"Colour",
"Wild",
"Royal",
"Old",
"Magic",
"Clean",
"Clear",
"Swiss",
"cool",
"gold",
"",
"skin",
"Ayurveda",
"Paris",
"Essences",
"Guy",
"london",
"body",
"shop",
"hot",
"ice",
"Line",
"Aloe",
"Herbals",
"Science",
"Hair",
"Care",
"Face",
"Facial",
"Hand",
"Nail",
"Foot",
"Gel",
"Lip",
"Eye",
"Cream",
"Secret",
"Temptation",
"good",
"Organics",
"Rose",
"charm",
"glow",
"crazy",
"Blue",
"Nectar",
"simply",
"straight",
"earth",
"Facial Wipes",
"Hello",
"Fragrance",
"Golden",
"Step",
"real",
"Baby",
"brush",
"man",
"style",
"Essentials",
"Makeup",
"Men",
"India",
"grooming",
"club",
"Accessories",
"Charcoal",
"active",
"ultra",
"beard",
"Pure",
"Bag",
"Happily",
"safe",
"Fresh",
"Eraser",
"Cosmetics",
"Star",
"sweet",
"sensitive",
"Touch",
"Mom",
"Cucumber",
"Vegan",
"Fragrances",
"vibes",
"Research",
"make up",
"la",
"When",
"Thing",
"Therapy",
"Cosmetic",
"Life",
"Himalayan",
"Element",
"Wellness",
"Aroma",
"Nature's",
"Roots",
"Herbs",
"The",
"mask",
"supply",
"Revolution",
"Pro",
"Super",
"Bender",
"Pretty",
"Only",
"Heart",
"oils",
"Naturals",
"advanced",
"Lotus",
"me",
"natures",
"essence",
"wear",
"perfumes",
"west",
"oil",
"all",
"stay",
"color",
"just",
"Wipes",
"bliss",
"Essential",
"colors",
"shaving",
"brown",
"palette",
"Dr.",
"natural",
"Rose"
]
list_cat_stop_words =[
"Face",
"Facial",
"Hand",
"Nail",
"Foot",
"Body",
"Gel",
"Lip",
"Eye",
"Cream",
"Hair",
"Women",
"care",
"Makeup",
"Men",
"skin"
]

list_brand_stop_words = [x.lower() for x in list_brand_stop_words]
list_cat_stop_words = [x.lower() for x in list_cat_stop_words]
list_brand_cat_stop_words =list_brand_stop_words+list_cat_stop_words
list_exact_brand_exceptions =[x.lower() for x in list_exact_brand_exceptions]

def get_redis_connection():
    REDIS_HOST=os.environ['REDIS_HOST']
    REDIS_PORT=os.environ['REDIS_PORT']
    redis_connection_object=redis.StrictRedis(host='10.77.0.35', port='6379')
    try:
        redis_connection_object.ping()
        print("Successfully connected to redis")
    except (redis.exceptions.ConnectionError, ConnectionRefusedError):
        print("Redis connection error!")

    return redis_connection_object
redis_connection_object = get_redis_connection()

# entity-entity group, category,brand details function...
def get_ev_details():
    query_ev_details="""
    select ev.id as entity_value_id,lower(ev.entity_value) as entity_value,ev.entity_group_id,lower(eg.name) as entity_group
    from entity_values ev
    join entity_groups eg on ev.entity_group_id=eg.id
    """
    df_ev_details=SQL.fetch_data_with_schema(query_ev_details,"slave")

    query_cat="""
    select id as entity_value_id,lower(name) as entity_value,-2 as entity_group_id,'category' as entity_group
    from product_category where is_active =1
    group by id
    """
    df_cat=SQL.fetch_data_with_schema(query_cat,"slave")

    query_brand="""
    select id as entity_value_id,lower(name) as entity_value,-3 as entity_group_id,'brand' as entity_group
    from product_brand where active =1
    group by id 
    """
    df_brand=SQL.fetch_data_with_schema(query_brand,"slave")
    
    df_ev_details=df_ev_details.append([df_cat,df_brand],ignore_index=True)
    df_ev_details['entity_value']=df_ev_details['entity_value'].str.lower()
    #df_ev_details['entity_value']=df_ev_details['entity_value'].str.replace('&','and')
    df_ev_details=df_ev_details[~df_ev_details.entity_value.isnull()]
    return df_ev_details

#newly added code
print("Getting Entity details")
df_ev_details_all=get_ev_details()
df_ev_details_all['entity_value'] = df_ev_details_all['entity_value'].str.strip()
# df_ev_details_all['entity_value']=df_ev_details_all['entity_value'].str.replace("'","")
df_ev_details_all=df_ev_details_all.query("entity_group!='color code'")
df_ev_details = df_ev_details_all[(df_ev_details_all['entity_group']!='brand') & (df_ev_details_all['entity_group']!='category')]
df_ev_details_brand_cat = df_ev_details_all[(df_ev_details_all['entity_group']=='brand') | (df_ev_details_all['entity_group']=='category')]
df_ev_details_brand = df_ev_details_all[df_ev_details_all['entity_group']=='brand']
df_ev_details_cat = df_ev_details_all[df_ev_details_all['entity_group']=='category']

list_entity_values_without_ranges=list(df_ev_details.query("entity_group!='range'")['entity_value'].unique())
list_entity_values_with_ranges=list(df_ev_details['entity_value'].unique())
list_entity_values_brand = list(df_ev_details_brand['entity_value'].unique())
list_entity_values_cat = list(df_ev_details_cat['entity_value'].unique())
list_entity_values_brand_cat = list(df_ev_details_brand_cat['entity_value'].unique())
df_ev_details_with_ranges=df_ev_details.copy()
df_ev_details=df_ev_details.query("entity_group!='range'")

df_cats_with_special_char = df_ev_details_all[(
    ((df_ev_details_all["entity_group"] == "category") | (df_ev_details_all["entity_group"] == "brand"))
    & 
    ((df_ev_details_all["entity_value"].str.contains("`")) | (df_ev_details_all["entity_value"].str.contains("^")) |(df_ev_details_all["entity_value"].str.contains(".")) |(df_ev_details_all["entity_value"].str.contains("&")) | (df_ev_details_all["entity_value"].str.contains(",")) | (df_ev_details_all["entity_value"].str.contains("-")) | (df_ev_details_all["entity_value"].str.contains("'")))
    )]
df_cats_with_special_char['entity_value'] = df_cats_with_special_char['entity_value'].str.replace(",","")
df_cats_with_special_char['entity_value'] = df_cats_with_special_char['entity_value'].str.replace("&","")
df_cats_with_special_char['entity_value'] = df_cats_with_special_char['entity_value'].str.replace("-","")
df_cats_with_special_char['entity_value'] = df_cats_with_special_char['entity_value'].str.replace("'","")
df_cats_with_special_char['entity_value'] = df_cats_with_special_char['entity_value'].str.replace("`","")
df_cats_with_special_char['entity_value'] = df_cats_with_special_char['entity_value'].str.replace("^","")
df_cats_with_special_char['entity_value'] = df_cats_with_special_char['entity_value'].str.replace(".","")
df_ev_details_all_for_user_data_topics = df_ev_details_all.append(df_cats_with_special_char)

def get_aliases():
    # getting aliases and removing aliases which are exactly same as other entities
    print("getting aliases and removing aliases which are exactly same as other entities")
    query_aliases="""
    select lower(ea.alias_name) as alias_name,lower(ev.entity_value) as entity_value
    from entity_aliases as ea
    join entity_values as ev on ea.module_id=ev.id
    where ea.module='entity_value'
    and ea.alias_name!=ev.entity_value
    group by alias_name,entity_value
    UNION
    select lower(ea.alias_name) as alias_name,lower(ev.name) as entity_value
    from entity_aliases as ea
    join product_brand as ev on ea.module_id=ev.id
    where ea.module='brand'
    and ea.alias_name!=ev.name
    group by alias_name,entity_value
    UNION
    select lower(ea.alias_name) as alias_name,lower(ev.name) as entity_value
    from entity_aliases as ea
    join product_category as ev on ea.module_id=ev.id
    where ea.module='category'
    -- and ea.alias_name!=ev.name
    group by alias_name,entity_value
    """
    df_aliases=SQL.fetch_data_with_schema(query_aliases,"slave")
    return df_aliases

# existing product tagging function
def get_product_ranges():
    query_old_tagging="""
    select module_id as product_id,ev.id as entity_value_id,lower(ev.entity_value) as entity_value,ev.entity_group_id as entity_group_id,lower(eg.name) as entity_group
    from entity_products as ep
    join  entity_values ev on ev.id=ep.entity_value_id
    join entity_groups eg on ev.entity_group_id=eg.id
    where module='product'
    and lower(eg.name)='range'
    """
    df_old_tagging=SQL.fetch_data_with_schema(query_old_tagging,"slave")
    
    query_pm="""
    select product_id,lower(category_name) as category_name,lower(brand_name) as brand_name
    from product_master
    group by product_id
    """
    df_pm=SQL.fetch_data_with_schema(query_pm,"warehouse")
    
    df_old_tagging=pd.merge(df_old_tagging,df_pm,on=['product_id'],how='left')
    df_old_tagging['entity_value']=df_old_tagging['entity_value'].str.lower()
    df_old_tagging=df_old_tagging[~df_old_tagging.entity_value.isnull()]
    df_old_tagging=df_old_tagging.drop_duplicates()
    return df_old_tagging

print("Getting Aliases")
df_aliases =get_aliases()



df_aliases = df_aliases.query("alias_name!='face'")

query_root_cats="""
select lower(category_root_slug) as category_root_slug
from product_master
where category_root_slug!='' and category_root_slug!='0'
group by category_root_slug
"""
df_root_cats=SQL.fetch_data_with_schema(query_root_cats,"warehouse")

# alias_entity_collision_list=list(df_ev_details_all.query("(entity_group != 'color code') or (entity_group != 'range')")['entity_value'].unique())+list(df_root_cats['category_root_slug'].unique())
alias_entity_collision_list=list(df_root_cats['category_root_slug'].unique())
df_aliases=df_aliases.query("alias_name  not in @alias_entity_collision_list")

df_custom_aliases=df_ev_details_all[['entity_value']].copy()

df_custom_aliases['alias_name']=df_custom_aliases['entity_value'].str.replace(" ","")

df_custom_aliases=df_custom_aliases.query("entity_value!=alias_name").copy()

df_custom_aliases=df_custom_aliases[['alias_name','entity_value']].copy()

df_aliases=df_aliases.append(df_custom_aliases,ignore_index=True)

print("Getting product ranges")
df_ranges=get_product_ranges()
df_ranges=df_ranges.drop_duplicates(subset=['brand_name','category_name','entity_value'])[['brand_name','category_name','entity_value']]
#newly added code
df_ranges['entity_value']=df_ranges['entity_value'].str.replace("'","")
df_ranges['brand_name']=df_ranges['brand_name'].str.replace("'","")
df_ranges['category_name']=df_ranges['category_name'].str.replace("'","")
df_ranges['entity_value']=df_ranges.apply(lambda x:x['entity_value'].replace(str(x['brand_name']),""),axis=1)
df_ranges['entity_value']=df_ranges.apply(lambda x:x['entity_value'].replace(str(x['category_name']),""),axis=1)
df_ranges['entity_value']=df_ranges['entity_value'].str.strip()

list_brand_entity_values=list(df_ev_details_all.query("entity_group=='brand'")['entity_value'].unique())
list_category_entity_values=list(df_ev_details_all.query("entity_group=='category'")['entity_value'].unique())
df_custom_stop_words=df_ev_details_all[['entity_value']].copy()
df_custom_stop_words['entity_value_words']=df_custom_stop_words['entity_value'].str.split()

df_custom_stop_words=df_custom_stop_words.explode('entity_value_words')

df_custom_stop_words=df_custom_stop_words.groupby('entity_value_words').size().reset_index().rename(columns={0:'num_of_entitites'}).sort_values('num_of_entitites',ascending=False)
list_custom_stop_words=list(df_custom_stop_words.query("num_of_entitites>10")['entity_value_words'].unique())

df_matched_entities=pd.read_csv("/purplle_data_science/data_science_development/similar_query_clusters/data/entity_entity_match.csv")
del df_matched_entities['Unnamed: 0']

df_matched_entities['matched_entities']=df_matched_entities['matched_entities'].apply(lambda x:eval(x))
df_matched_entities=df_matched_entities.groupby("entity_value")['matched_entities'].apply(list).reset_index()

df_matched_entities=df_matched_entities.query("entity_value not in @list_custom_stop_words")
dict_matched_entities=df_matched_entities.set_index('entity_value').to_dict()['matched_entities']

# keyword_processor_to_replace_entities
print("keyword_processor_to_replace_aliases")
from flashtext.keyword import KeywordProcessor
keyword_processor_to_replace_aliases = KeywordProcessor()
gc_var=df_aliases.apply(lambda x:keyword_processor_to_replace_aliases.add_keyword(x['alias_name'],x['entity_value']),axis=1)


print("keyword_processor_to_replace_brand")
from flashtext.keyword import KeywordProcessor
keyword_processor_to_replace_brand_entities = KeywordProcessor()
gc_var=df_ev_details_brand.apply(lambda x:keyword_processor_to_replace_brand_entities.add_keyword(x['entity_value']," "),axis=1)

print("keyword_processor_to_replace_cat")
from flashtext.keyword import KeywordProcessor
keyword_processor_to_replace_cat_entities = KeywordProcessor()
gc_var=df_ev_details_cat.apply(lambda x:keyword_processor_to_replace_cat_entities.add_keyword(x['entity_value']," "),axis=1)

print("keyword_processor_to_replace_brand_cat")
from flashtext.keyword import KeywordProcessor
keyword_processor_to_replace_brand_cat_entities = KeywordProcessor()
gc_var=df_ev_details_brand_cat.apply(lambda x:keyword_processor_to_replace_brand_cat_entities.add_keyword(x['entity_value']," "),axis=1)


print("keyword_processor_for_unknowns")
from flashtext.keyword import KeywordProcessor
keyword_processor_for_unknowns = KeywordProcessor()
gc_var=df_ev_details_brand_cat.apply(lambda x:keyword_processor_for_unknowns.add_keyword(x['entity_value']," "),axis=1)


print("keyword_processor_to_replace_entities")
from flashtext.keyword import KeywordProcessor
keyword_processor_to_replace_entities = KeywordProcessor()
gc_var=df_ev_details.apply(lambda x:keyword_processor_to_replace_entities.add_keyword(x['entity_value']," "),axis=1)

# keyword_processor_to_extract_entities
print("keyword_processor_to_extract_entities")
from flashtext.keyword import KeywordProcessor
keyword_processor_to_extract_entities = KeywordProcessor()
gc_var=df_ev_details.apply(lambda x:keyword_processor_to_extract_entities.add_keyword(x['entity_value']),axis=1)


# keyword_processor_to_extract_brand_categoryentities
print("keyword_processor_to_extract_brand_entities")
from flashtext.keyword import KeywordProcessor
keyword_processor_to_extract_brand_entities = KeywordProcessor()
gc_var=df_ev_details_brand.apply(lambda x:keyword_processor_to_extract_brand_entities.add_keyword(x['entity_value']),axis=1)

print("keyword_processor_to_extract_category_entities")
from flashtext.keyword import KeywordProcessor
keyword_processor_to_extract_category_entities = KeywordProcessor()
gc_var=df_ev_details_cat.apply(lambda x:keyword_processor_to_extract_category_entities.add_keyword(x['entity_value']),axis=1)

print("keyword_processor_to_extract_brand_category_entities")
from flashtext.keyword import KeywordProcessor
keyword_processor_to_extract_brand_category_entities = KeywordProcessor()
gc_var=df_ev_details_brand_cat.apply(lambda x:keyword_processor_to_extract_brand_category_entities.add_keyword(x['entity_value']),axis=1)


list_root_cats=list(df_root_cats['category_root_slug'].unique())
list_eng_stop_words=list(set(nltk.corpus.stopwords.words('english')))
list_stop_words=set(list_custom_stop_words+['for','anti','a','an','of','in','and','&','products','-','(',')','@','dark','control','sets']+list_root_cats+list_eng_stop_words)

def spell_corrector(input_query):
    st = int(time.time())
    url="http://10.160.0.11/productnew/spell_correction_test?search_term={}".format(input_query)
    r = requests.get(url)
    query=r.content.decode("utf-8")
    et = int(time.time())
    return query

def create_symantic_topics_v1(input_query):
    list_matched_entities_from_query=list_matched_entities_from_unknowns=[]
    list_matched_entities_from_unknowns_brand=[]
    list_matched_entities_from_unknowns_cat=[]
    list_matched_entities_from_query_brand_category=[]

    input_query = input_query.lower()
    # print("INPUT QUERY : ",input_query)
    input_query =spell_corrector(input_query)
    # print("INPUT QUERY AFTER SPELL CORRECTOR : ",input_query)
    input_query=keyword_processor_to_replace_aliases.replace_keywords(input_query)
    # print("INPUT QUERY AFTER ALIAS REPLACEMENT : ",input_query)
    input_query = ' '.join(list(dict.fromkeys(input_query.split(" ")).keys()))
    # print("INPUT QUERY AFTER ALIAS REPLACEMENT : ",input_query)
    original_input_query=input_query
    
    #Check ON BRAND_CATEGORY DICTINORY
    list_present_entities_brand_cat=keyword_processor_to_extract_brand_category_entities.extract_keywords(original_input_query)
    list_unknown_words_brand_cat=keyword_processor_to_replace_brand_cat_entities.replace_keywords(original_input_query).split()
    list_unknown_words_brand_cat = list(dict.fromkeys(x for x in list_unknown_words_brand_cat if x not in list_eng_stop_words).keys())
    
    #GETTING TOPICS FROM BRAND_CATEGORY
    list_matched_entities_from_query_brand_cat =[(x,100,'exact') for x in list_present_entities_brand_cat]
    # print("STEP 1 AFTER (BRAND_CATEGORY) UNDERSTOOD WORDS : ",list_matched_entities_from_query_brand_cat)
    # print("STEP 1 AFTER (BRAND_CATEGORY) UNKNOWN WORDS : ",list_unknown_words_brand_cat)
    
    input_query_to_match = ' '.join(list_unknown_words_brand_cat)
    input_query_to_match =input_query_to_match.strip()
    len_sentence=len(input_query_to_match)
    
    # print("INPUT QUERY AFTER (BRAND_CATEGORY) REPLACEMENT ",input_query_to_match+"<- LEN ->"+str(len_sentence))

    list_unknown_words= list_unknown_words_brand_cat
    list_all_unknown_words_combination=[]
    len_list_unknown_words = len(list_unknown_words)
    if(len_list_unknown_words > 1):
        list_all_unknown_words_combination =list(itertools.combinations(list_unknown_words, 2))


    #GETTING TOPICS FROM CATEGORY
    list_words_in_unknowns_understood_cat =[]
    list_words_unknowns_not_understood_after_combo_match=[]
    list_unknowns_from_cat_dict=[]
    list_knowns_from_cat_dict=[]

    if len_sentence>1:
        if len_list_unknown_words == 1:
            for each_unknown in list_unknown_words:
                if each_unknown in list_cat_stop_words:
                    list_unknowns_from_cat_dict.append(each_unknown)
                else:
                    if(len(each_unknown) > 1):
                        matched_on_dict_token_set_ratio = process.extractBests(each_unknown,list_entity_values_cat,scorer=fuzz.token_set_ratio,score_cutoff=85,limit=50)
                        matched_on_dict_partial_ratio = process.extractBests(each_unknown,list_entity_values_cat,scorer=fuzz.partial_ratio,score_cutoff=100,limit=50)
                        matched_on_dict_partial_token_sort_ratio = process.extractBests(each_unknown,list_entity_values_cat,scorer=fuzz.partial_token_sort_ratio,score_cutoff=100,limit=50)
                        matched_on_dict = list(set(matched_on_dict_token_set_ratio+matched_on_dict_partial_ratio+matched_on_dict_partial_token_sort_ratio))
                        len_matched_on_dict = len(matched_on_dict)
#                             print("matched_cat from unknown PARTIAL TOKEN SORT Ratio : "+each_unknown,matched_on_dict)
                        if(len_matched_on_dict > 0):
                            list_matched_entities_from_unknowns_cat=list_matched_entities_from_unknowns_cat+matched_on_dict
                            list_knowns_from_cat_dict.append(each_unknown)
                        else:
                            list_unknowns_from_cat_dict.append(each_unknown)
        else:
            for each_combo in list_all_unknown_words_combination:
                combined_combo_query = ' '.join(list(each_combo))
                
                if(len(combined_combo_query) > 1):
                    matched_on_dict = process.extractBests(combined_combo_query,list_entity_values_cat,scorer=fuzz.token_set_ratio,score_cutoff=85,limit=50)
                    # print("matched_cat from Combo TOKEN SET Ratio : "+combined_combo_query,matched_on_dict)
                    len_matched_on_dict = len(matched_on_dict)
                    if len_matched_on_dict < 1:
                        matched_on_dict = process.extractBests(combined_combo_query,list_entity_values_cat,scorer=fuzz.partial_ratio,score_cutoff=100,limit=50)
                        # print("matched_cat from Combo PARTIAL Ratio : "+combined_combo_query,matched_on_dict)
                        len_matched_on_dict = len(matched_on_dict)
                    if len_matched_on_dict < 1:
                        matched_on_dict = process.extractBests(combined_combo_query,list_entity_values_cat,scorer=fuzz.partial_token_sort_ratio,score_cutoff=100,limit=50)
                        # print("matched_cat from Combo PARTIAL TOKEN SET Ratio : "+combined_combo_query,matched_on_dict)
                        len_matched_on_dict = len(matched_on_dict)
                    if len_matched_on_dict > 0:
                        list_matched_entities_from_unknowns_cat=list_matched_entities_from_unknowns_cat+matched_on_dict
                        list_words_in_unknowns_understood_cat = list_words_in_unknowns_understood_cat + list(each_combo)

            list_words_in_unknowns_understood_cat = list(set(list_words_in_unknowns_understood_cat))
            list_words_unknowns_after_combo_match_cat = list(set(list_unknown_words)-set(list_words_in_unknowns_understood_cat))
            # print("list_words_unknowns_after_combo_match_cat->",list_words_unknowns_after_combo_match_cat)
            if len(list_words_unknowns_after_combo_match_cat) > 0:
                for each_unknown in list_words_unknowns_after_combo_match_cat:
                    if each_unknown in list_cat_stop_words:
                        list_unknowns_from_cat_dict.append(each_unknown)
                    else:
                        if(len(each_unknown) > 1):
                            matched_on_dict_token_set_ratio = process.extractBests(each_unknown,list_entity_values_cat,scorer=fuzz.token_set_ratio,score_cutoff=85,limit=50)
                            matched_on_dict_partial_ratio = process.extractBests(each_unknown,list_entity_values_cat,scorer=fuzz.partial_ratio,score_cutoff=100,limit=50)
                            matched_on_dict_partial_token_sort_ratio = process.extractBests(each_unknown,list_entity_values_cat,scorer=fuzz.partial_token_sort_ratio,score_cutoff=100,limit=50)
                            matched_on_dict = list(set(matched_on_dict_token_set_ratio+matched_on_dict_partial_ratio+matched_on_dict_partial_token_sort_ratio))
                            len_matched_on_dict = len(matched_on_dict)
                            if(len_matched_on_dict > 0):
                                list_matched_entities_from_unknowns_cat=list_matched_entities_from_unknowns_cat+matched_on_dict
                                list_knowns_from_cat_dict.append(each_unknown)
                            else:
                                list_unknowns_from_cat_dict.append(each_unknown)     
    list_matched_entities_from_unknowns_cat=list(set([x+('partial',) for x in  list_matched_entities_from_unknowns_cat]))

    # print("list_unknowns_from_cat_dict",list_unknowns_from_cat_dict)
    # print("list_knowns_from_cat_dict",list_knowns_from_cat_dict)
    # print("STEP 2 (CATEGORY) UNDERSTOOD WORDS : ",list_matched_entities_from_unknowns_cat)



    list_words_in_unknowns_understood_brand =[]
    list_words_unknowns_not_understood_after_combo_match=[]
    list_unknowns_from_brand_dict=[]
    list_knowns_from_brand_dict=[]
    if len_sentence>1:
        if len_list_unknown_words == 1:
            for each_unknown in list_unknown_words:
                if each_unknown in list_brand_stop_words:
                    list_unknowns_from_brand_dict.append(each_unknown)
                else:
                    if(len(each_unknown) > 1):
                        matched_on_dict = process.extractBests(each_unknown,list_entity_values_brand,scorer=fuzz.token_set_ratio,score_cutoff=85,limit=50)
                        len_matched_on_dict = len(matched_on_dict)
                        # print("matched_brand from UNKNOWN TOKEN SET Ratio: "+each_unknown,matched_on_dict)
#                         if len_matched_on_dict < 1:
#                             matched_on_dict = process.extractBests(each_unknown,list_entity_values_brand,scorer=fuzz.partial_ratio,score_cutoff=100,limit=50)
#                             print("matched_brand from UNKNOWN PARTIAL  Ratio : "+each_unknown,matched_on_dict)
#                             len_matched_on_dict = len(matched_on_dict)
                        if(len_matched_on_dict > 0):
                            list_matched_entities_from_unknowns_brand=matched_on_dict
                            list_knowns_from_brand_dict.append(each_unknown)
                        else:
                            list_unknowns_from_brand_dict.append(each_unknown)
        else:
            for each_combo in list_all_unknown_words_combination:
                combined_combo_query = ' '.join(list(each_combo))
                if(len(combined_combo_query) > 1):
                    matched_on_dict = process.extractBests(combined_combo_query,list_entity_values_brand,scorer=fuzz.token_set_ratio,score_cutoff=85,limit=50)
                    len_matched_on_dict = len(matched_on_dict)
                    # print("matched_brand from Combo TOKEN SET Ratio : "+combined_combo_query,matched_on_dict)
                    # if len_matched_on_dict < 1:
                    #     matched_on_dict = process.extractBests(combined_combo_query,list_entity_values_brand,scorer=fuzz.partial_ratio,score_cutoff=100,limit=50)
                    #     # print("matched_brand from Combo PARTIAL  Ratio : "+combined_combo_query,matched_on_dict)
                    #     len_matched_on_dict = len(matched_on_dict)
                    if len_matched_on_dict > 0:
                        list_matched_entities_from_unknowns_brand=list_matched_entities_from_unknowns_brand+matched_on_dict
                        list_words_in_unknowns_understood_brand = list_words_in_unknowns_understood_brand + list(each_combo)

            list_words_in_unknowns_understood_brand = list(set(list_words_in_unknowns_understood_brand))
            list_words_unknowns_after_combo_match_brand = list(set(list_unknown_words)-set(list_words_in_unknowns_understood_brand))
            # print("list_words_unknowns_after_combo_match_brand->",list_words_unknowns_after_combo_match_brand)
            if len(list_words_unknowns_after_combo_match_brand) > 0:
                for each_unknown in list_words_unknowns_after_combo_match_brand:
                    if each_unknown in list_brand_stop_words:
                        list_unknowns_from_cat_dict.append(each_unknown)
                    else:
                        if(len(each_unknown) > 1):
                            matched_on_dict = process.extractBests(each_unknown,list_entity_values_brand,scorer=fuzz.token_set_ratio,score_cutoff=85,limit=50)
                            len_matched_on_dict = len(matched_on_dict)
                            # print("matched_brand from UNKNOWN TOKEN SET Ratio: "+each_unknown,matched_on_dict)
#                             if len_matched_on_dict < 1:
#                                 matched_on_dict = process.extractBests(each_unknown,list_entity_values_brand,scorer=fuzz.partial_ratio,score_cutoff=100,limit=50)
#                                 print("matched_brand from UNKNOWN PARTIAL  Ratio : "+each_unknown,matched_on_dict)
#                                 len_matched_on_dict = len(matched_on_dict)
                            if(len_matched_on_dict > 0):
                                list_matched_entities_from_unknowns_brand=matched_on_dict
                                list_knowns_from_brand_dict.append(each_unknown)
                            else:
                                list_unknowns_from_brand_dict.append(each_unknown)   
    list_matched_entities_from_unknowns_brand=list(set([x+('partial',) for x in  list_matched_entities_from_unknowns_brand]))
    # print("list_unknowns_from_brand_dict",list_unknowns_from_brand_dict)
    # print("list_knowns_from_brand_dict",list_knowns_from_brand_dict)
    # print("STEP 3 (BRAND) UNDERSTOOD WORDS",list_matched_entities_from_unknowns_brand)

    #Check ON ENtity DICTINORY
    list_present_entities=keyword_processor_to_extract_entities.extract_keywords(input_query)
    # print("list_present_entities",list_present_entities)
    list_unknown_words_entities=keyword_processor_to_replace_entities.replace_keywords(input_query).split()
    # print("list_unknown_words_entities",list_unknown_words_entities)
    list_unknown_words_entities=set(list_unknown_words_entities)- list_stop_words
    # print("list_unknown_words_entities",list_unknown_words_entities)


    len_sentence=len(input_query)
    #GETTING TOPICS FROM ENTITIES
    list_matched_entities_from_query=process.extractBests(input_query,list_entity_values_without_ranges,scorer=fuzz.token_set_ratio,score_cutoff=90,limit=50)
    list_unknown_entities=[]
    if len_sentence>1:
        for each_unknown in list_unknown_words_entities:
            matched_entities = list_matched_entities_from_unknowns+process.extractBests(each_unknown,list_entity_values_without_ranges,scorer=fuzz.token_set_ratio,score_cutoff=85,limit=50)
            len_matched_entities = len(matched_entities)
            if(len_matched_entities < 1):
                list_unknown_entities.append(each_unknown)
            list_matched_entities_from_unknowns=list_matched_entities_from_unknowns+matched_entities
#         for each_present_entitiy in list_present_entities:
#             if each_present_entitiy in dict_matched_entities:
#                 list_matched_entities_from_present_entities=list_matched_entities_from_present_entities+dict_matched_entities[each_present_entitiy]

    list_final_entities=list_matched_entities_from_query+list_matched_entities_from_unknowns

    list_matched_entities_from_query_names = [y[0] for y in list_matched_entities_from_query]
    list_exact_entities= list(set(list_present_entities)&set(list_matched_entities_from_query_names))
    list_exact_entities=[(x,100) for x in list_exact_entities]
    list_partial_entities= list(set(list_present_entities)^set(list_matched_entities_from_query_names))
    list_partial_entities=[(x,100) for x in list_partial_entities]

    set_final_entities_for_range=set([x[0] for x in  list_final_entities])
    list_brand_matches=list_category_matches=[]
    list_brand_matches= list(set_final_entities_for_range & set(list_brand_entity_values))
    list_category_matches= list(set_final_entities_for_range & set(list_category_entity_values))

    list_ranges_to_consider=[]
    if list_brand_matches!=[] and list_category_matches!=[]:
         list_ranges_to_consider=df_ranges.query("brand_name in @list_brand_matches and category_name in @list_category_matches")['entity_value'].drop_duplicates().tolist()
    elif list_brand_matches!=[] and list_category_matches==[]:
         list_ranges_to_consider=df_ranges.query("brand_name in @list_brand_matches")['entity_value'].drop_duplicates().tolist()
    elif list_brand_matches==[] and list_category_matches!=[]:
         list_ranges_to_consider=df_ranges.query("category_name in @list_category_matches")['entity_value'].drop_duplicates().tolist()     

    if list_ranges_to_consider!=[]:
        list_matched_entities_from_query=list_matched_entities_from_query+process.extractBests(input_query,list_ranges_to_consider,scorer=fuzz.token_set_ratio,score_cutoff=90,limit=50)
        for each_unknown in list_unknown_words_entities:
            list_matched_entities_from_unknowns=list_matched_entities_from_unknowns+process.extractBests(each_unknown,list_ranges_to_consider,scorer=fuzz.token_set_ratio,score_cutoff=80,limit=50)

    list_matched_entities_from_query=list(set([x+('exact',) for x in  list_matched_entities_from_query]))

    list_exact_entities_from_query=list(set([x+('exact',) for x in  list_exact_entities]))
    list_partial_entities_from_query=list(set([x+('partial',) for x in  list_partial_entities]))
    list_matched_entities_from_query = list_exact_entities_from_query+list_partial_entities_from_query
    list_matched_entities_from_unknowns=list(set([x+('partial',) for x in  list_matched_entities_from_unknowns]))

    # print("STEP 4 (ENTITY QUERY) UNDERSTOOD WORDS",list_matched_entities_from_query)
    # print("STEP 4 (ENTITY UNKONWN) UNDERSTOOD WORDS",list_matched_entities_from_unknowns)

    final_list_unknowns =list(set(list_unknowns_from_cat_dict+list_unknowns_from_brand_dict) - set(list_knowns_from_cat_dict+ list_knowns_from_brand_dict) - set(list_words_in_unknowns_understood_cat+ list_words_in_unknowns_understood_brand))

    # print("FINAL UNKNOWN WORDS->>",final_list_unknowns)

    is_puq = 1 if len(final_list_unknowns) < 1 and (len(list_matched_entities_from_query_brand_cat) >0 or len(list_matched_entities_from_unknowns_brand) >0 or len(list_matched_entities_from_unknowns_cat) >0) else 0
    
    obj_semantic_tpoics={
     'list_unknown_brand_cat':final_list_unknowns,
     'is_puq':is_puq,
     'list_matched_entities_from_query':list_matched_entities_from_query,
     'list_matched_entities_from_unknowns':list_matched_entities_from_unknowns,
#      'list_matched_entities_from_present_entities':list_matched_entities_from_present_entities,
    #  'list_matched_entities_from_query_brand':list_matched_entities_from_query_brand,
     'list_matched_entities_from_unknowns_brand':list_matched_entities_from_unknowns_brand,
    #  'list_matched_entities_from_present_entities_brand':list_matched_entities_from_present_entities_brand,
    #  'list_matched_entities_from_query_cat':list_matched_entities_from_query_cat,
     'list_matched_entities_from_unknowns_cat':list_matched_entities_from_unknowns_cat,
    #  'list_matched_entities_from_present_entities_cat':list_matched_entities_from_present_entities_cat,
     'list_matched_entities_from_query_brand_cat':list_matched_entities_from_query_brand_cat
    }
#     print("obj_semantic_tpoics",obj_semantic_tpoics)
    return obj_semantic_tpoics


def applying_algo_in_chunks(df_data,max_rows):
    total_count=df_data.shape[0]
    total_splits=int(total_count/max_rows)+1
    df_splits = np.array_split(df_data, total_splits)
    result=True
    count=1
    df_final_result=pd.DataFrame()
    for df_chunk in df_splits :
#         print(df_chunk)
        df_chunk['modified_search_query']=df_chunk['original_search_query'].str.replace("'","")
        df_chunk['close_matches']=df_chunk['modified_search_query'].apply(lambda x:create_symantic_topics_v1(x))
        df_chunk['redis_key']="v1_semantic_topics_"+df_chunk['original_search_query']
        df_chunk['redis_response']=df_chunk.apply(lambda x:redis_connection_object.set(x['redis_key'],str(x['close_matches'])),axis=1)
        df_final_result=df_final_result.append(df_chunk,ignore_index=True)
        count=count+1
    return df_final_result


def get_topics_data():
    query_topics_info="""
    select *
    from topic_identification_of_query_raw_data
    """
    df_topics_info=SQL.fetch_data_with_schema(query_topics_info,"warehouse")
    return df_topics_info


# In[11]:

def get_query_clusters():
    query_query_clusters="""
    select * from query_clusters_30_days
    """
    df_query_clusters=SQL.fetch_data_with_schema(query_query_clusters,"warehouse")

    df_query_clusters['query_cluster']=df_query_clusters['query_cluster'].str.split(',')
    df_query_clusters['original_search_query']=df_query_clusters['query_cluster'].copy()
    df_query_clusters=df_query_clusters.explode('original_search_query')[['original_search_query','query_cluster']]

    dict_query_clusters=df_query_clusters.set_index('original_search_query')['query_cluster'].to_dict()
    return dict_query_clusters

print("Getting topics data ")
df_topics_info = get_topics_data()
print("Getting query clusters data ")
dict_query_clusters = get_query_clusters()

def get_semantic_topics(input_query,is_train=False):
    redis_result=redis_connection_object.get("test_v1_semantic_topics_"+input_query)
    if (redis_result and  (not is_train)):
        redis_result=eval(redis_result)
        df_final_result['final_close_matches_entities']=df_final_result['close_matches'].apply(lambda x:list(set(x['list_matched_entities_from_query']+x['list_matched_entities_from_unknowns'])))
        df_final_result['final_close_matches_brand']=df_final_result['close_matches'].apply(lambda x:list(x['list_matched_entities_from_unknowns_brand']))
        df_final_result['final_close_matches_cat']=df_final_result['close_matches'].apply(lambda x:list(x['list_matched_entities_from_unknowns_cat']))
        df_final_result['final_close_matches_brand_cat']=df_final_result['close_matches'].apply(lambda x:list(x['list_matched_entities_from_query_brand_cat']))
        
        df_final_result['final_close_matches_entities_entity']=df_final_result['final_close_matches_entities'].apply(lambda x:[y[0] for y in x])
        df_final_result['final_close_matches_entities_type']=df_final_result['final_close_matches_entities'].apply(lambda x:[y[2] for y in x])
        df_final_result['final_close_match_entities_percentage']=df_final_result['final_close_matches_entities'].apply(lambda x:[y[1] for y in x])
        
        df_final_result['final_close_matches_brand_entity']=df_final_result['final_close_matches_brand'].apply(lambda x:[y[0] for y in x])
        df_final_result['final_close_matches_brand_type']=df_final_result['final_close_matches_brand'].apply(lambda x:[y[2] for y in x])
        df_final_result['final_close_match_brand_percentage']=df_final_result['final_close_matches_brand'].apply(lambda x:[y[1] for y in x])
        
        
        df_final_result['final_close_matches_cat_entity']=df_final_result['final_close_matches_cat'].apply(lambda x:[y[0] for y in x])
        df_final_result['final_close_matches_cat_type']=df_final_result['final_close_matches_cat'].apply(lambda x:[y[2] for y in x])
        df_final_result['final_close_match_cat_percentage']=df_final_result['final_close_matches_cat'].apply(lambda x:[y[1] for y in x])
        
        
        df_final_result['final_close_matches_brand_cat_entity']=df_final_result['final_close_matches_brand_cat'].apply(lambda x:[y[0] for y in x])
        df_final_result['final_close_matches_brand_cat_type']=df_final_result['final_close_matches_brand_cat'].apply(lambda x:[y[2] for y in x])
        df_final_result['final_close_match_brand_cat_percentage']=df_final_result['final_close_matches_brand_cat'].apply(lambda x:[y[1] for y in x])
        
#         df_final_result['modified_search_query']=df_final_result['original_search_query'].apply(lambda x:keyword_processor_to_replace_aliases.replace_keywords(x))
#         df_final_result['modified_query_word_length']=df_final_result['modified_search_query'].apply(lambda x:len(x.split()))
        
        df_text_topics_entities=pd.DataFrame({'entity_value':df_final_result['final_close_matches_entities_entity'][0],'match_type':df_final_result['final_close_matches_entities_type'][0],'match_percentage':df_final_result['final_close_match_entities_percentage'][0]})
        df_text_topics_brand=pd.DataFrame({'entity_value':df_final_result['final_close_matches_brand_entity'][0],'match_type':df_final_result['final_close_matches_brand_type'][0],'match_percentage':df_final_result['final_close_match_brand_percentage'][0]})
        df_text_topics_cat=pd.DataFrame({'entity_value':df_final_result['final_close_matches_cat_entity'][0],'match_type':df_final_result['final_close_matches_cat_type'][0],'match_percentage':df_final_result['final_close_match_cat_percentage'][0]})
        df_text_topics_brand_cat=pd.DataFrame({'entity_value':df_final_result['final_close_matches_brand_cat_entity'][0],'match_type':df_final_result['final_close_matches_brand_cat_type'][0],'match_percentage':df_final_result['final_close_match_brand_cat_percentage'][0]})
        df_text_topics_entities=pd.merge(df_text_topics_entities,df_ev_details_with_ranges,on=['entity_value'],how='left')
        df_text_topics_brand=pd.merge(df_text_topics_brand,df_ev_details_brand,on=['entity_value'],how='left')
        df_text_topics_cat=pd.merge(df_text_topics_cat,df_ev_details_cat,on=['entity_value'],how='left')
        df_text_topics_brand_cat=pd.merge(df_text_topics_brand_cat,df_ev_details_brand_cat,on=['entity_value'],how='left')
        
#         df_text_topics =pd.concat([df_text_topics_entities, df_text_topics_brand,df_text_topics_cat], ignore_index=True)
        df_text_topics =pd.concat([df_text_topics_entities, df_text_topics_brand,df_text_topics_cat,df_text_topics_brand_cat], ignore_index=True)
        df_text_topics['orders_contrib']=0
        df_text_topics['topic_type']='text_topic'
        is_puq=df_final_result['close_matches'][0]['is_puq']
        list_unknowns=df_final_result['close_matches'][0]['list_unknown_brand_cat']
        df_text_topics = df_text_topics.drop_duplicates(subset=['entity_value_id','topic_type'], keep="first")
    else:
        df_text_topics=pd.DataFrame()
        data = {'original_search_query' : input_query}
        df_text_topics = pd.DataFrame(data, index=[0])
        df_text_topics['original_search_query']=df_text_topics['original_search_query'].str.lower()
        df_final_result = applying_algo_in_chunks(df_text_topics,1000)
        df_final_result['final_close_matches_entities']=df_final_result['close_matches'].apply(lambda x:list(set(x['list_matched_entities_from_query']+x['list_matched_entities_from_unknowns'])))
        df_final_result['final_close_matches_brand']=df_final_result['close_matches'].apply(lambda x:list(x['list_matched_entities_from_unknowns_brand']))
        df_final_result['final_close_matches_cat']=df_final_result['close_matches'].apply(lambda x:list(x['list_matched_entities_from_unknowns_cat']))
        df_final_result['final_close_matches_brand_cat']=df_final_result['close_matches'].apply(lambda x:list(x['list_matched_entities_from_query_brand_cat']))
        
        df_final_result['final_close_matches_entities_entity']=df_final_result['final_close_matches_entities'].apply(lambda x:[y[0] for y in x])
        df_final_result['final_close_matches_entities_type']=df_final_result['final_close_matches_entities'].apply(lambda x:[y[2] for y in x])
        df_final_result['final_close_match_entities_percentage']=df_final_result['final_close_matches_entities'].apply(lambda x:[y[1] for y in x])
        
        df_final_result['final_close_matches_brand_entity']=df_final_result['final_close_matches_brand'].apply(lambda x:[y[0] for y in x])
        df_final_result['final_close_matches_brand_type']=df_final_result['final_close_matches_brand'].apply(lambda x:[y[2] for y in x])
        df_final_result['final_close_match_brand_percentage']=df_final_result['final_close_matches_brand'].apply(lambda x:[y[1] for y in x])
        
        
        df_final_result['final_close_matches_cat_entity']=df_final_result['final_close_matches_cat'].apply(lambda x:[y[0] for y in x])
        df_final_result['final_close_matches_cat_type']=df_final_result['final_close_matches_cat'].apply(lambda x:[y[2] for y in x])
        df_final_result['final_close_match_cat_percentage']=df_final_result['final_close_matches_cat'].apply(lambda x:[y[1] for y in x])
        
        
        df_final_result['final_close_matches_brand_cat_entity']=df_final_result['final_close_matches_brand_cat'].apply(lambda x:[y[0] for y in x])
        df_final_result['final_close_matches_brand_cat_type']=df_final_result['final_close_matches_brand_cat'].apply(lambda x:[y[2] for y in x])
        df_final_result['final_close_match_brand_cat_percentage']=df_final_result['final_close_matches_brand_cat'].apply(lambda x:[y[1] for y in x])
        
#         df_final_result['modified_search_query']=df_final_result['original_search_query'].apply(lambda x:keyword_processor_to_replace_aliases.replace_keywords(x))
#         df_final_result['modified_query_word_length']=df_final_result['modified_search_query'].apply(lambda x:len(x.split()))
        
        df_text_topics_entities=pd.DataFrame({'entity_value':df_final_result['final_close_matches_entities_entity'][0],'match_type':df_final_result['final_close_matches_entities_type'][0],'match_percentage':df_final_result['final_close_match_entities_percentage'][0]})
        df_text_topics_brand=pd.DataFrame({'entity_value':df_final_result['final_close_matches_brand_entity'][0],'match_type':df_final_result['final_close_matches_brand_type'][0],'match_percentage':df_final_result['final_close_match_brand_percentage'][0]})
        df_text_topics_cat=pd.DataFrame({'entity_value':df_final_result['final_close_matches_cat_entity'][0],'match_type':df_final_result['final_close_matches_cat_type'][0],'match_percentage':df_final_result['final_close_match_cat_percentage'][0]})
        df_text_topics_brand_cat=pd.DataFrame({'entity_value':df_final_result['final_close_matches_brand_cat_entity'][0],'match_type':df_final_result['final_close_matches_brand_cat_type'][0],'match_percentage':df_final_result['final_close_match_brand_cat_percentage'][0]})
        df_text_topics_entities=pd.merge(df_text_topics_entities,df_ev_details_with_ranges,on=['entity_value'],how='left')
        df_text_topics_brand=pd.merge(df_text_topics_brand,df_ev_details_brand,on=['entity_value'],how='left')
        df_text_topics_cat=pd.merge(df_text_topics_cat,df_ev_details_cat,on=['entity_value'],how='left')
        df_text_topics_brand_cat=pd.merge(df_text_topics_brand_cat,df_ev_details_brand_cat,on=['entity_value'],how='left')
        
#         df_text_topics =pd.concat([df_text_topics_entities, df_text_topics_brand,df_text_topics_cat], ignore_index=True)
        df_text_topics =pd.concat([df_text_topics_entities, df_text_topics_brand,df_text_topics_cat,df_text_topics_brand_cat], ignore_index=True)
        df_text_topics['orders_contrib']=0
        df_text_topics['topic_type']='text_topic'
        is_puq=df_final_result['close_matches'][0]['is_puq']
        list_unknowns=df_final_result['close_matches'][0]['list_unknown_brand_cat']
        df_text_topics = df_text_topics.drop_duplicates(subset=['entity_value_id','topic_type'], keep="first")
    return df_text_topics,is_puq,list_unknowns
    

def get_user_data_topics(input_query):
    df_user_data_topic_info=df_topics_info.query("original_search_query in @input_query").drop_duplicates().copy()
    df_user_data_topic_info.entity_value = df_user_data_topic_info.entity_value.replace('\s+', ' ', regex=True)
    #if manual topic is there override topics
    if len(df_user_data_topic_info) > 0 and int(df_user_data_topic_info['orders_contrib'].sum()) == 0:
        print("In manual topic flow")
        total_num_of_orders=df_user_data_topic_info.drop_duplicates(subset='original_search_query')['st_num_of_orders'].sum()
        df_user_data_topic_info=df_user_data_topic_info.groupby(['entity_value','entity_group']).agg({'num_of_orders':sum}).reset_index()
        df_user_data_topic_info['cluster_num_of_orders']=total_num_of_orders
        df_user_data_topic_info['orders_contrib']=(df_user_data_topic_info['num_of_orders']/df_user_data_topic_info['cluster_num_of_orders'])*100
        df_user_data_topic_info=pd.merge(df_user_data_topic_info,df_ev_details_all_for_user_data_topics,on=['entity_value','entity_group'],how='left')
        df_user_data_topic_info['topic_type']='user_data_topic'
        df_user_data_topic_info['match_type']='exact'
        df_user_data_topic_info["orders_contrib"].fillna(0, inplace=True)
    else:
        if input_query in dict_query_clusters:
            print("In query cluster topic flow")
            list_query_cluster=dict_query_clusters[input_query]
            df_user_data_topic_info=df_topics_info.query("original_search_query in @list_query_cluster").drop_duplicates().copy()
            df_user_data_topic_info.entity_value = df_user_data_topic_info.entity_value.replace('\s+', ' ', regex=True)
            total_num_of_orders=df_user_data_topic_info.drop_duplicates(subset='original_search_query')['st_num_of_orders'].sum()
            df_user_data_topic_info=df_user_data_topic_info.groupby(['entity_value','entity_group']).agg({'num_of_orders':sum}).reset_index()
            df_user_data_topic_info['cluster_num_of_orders']=total_num_of_orders
            df_user_data_topic_info['orders_contrib']=(df_user_data_topic_info['num_of_orders']/df_user_data_topic_info['cluster_num_of_orders'])*100
            df_user_data_topic_info=pd.merge(df_user_data_topic_info,df_ev_details_all_for_user_data_topics,on=['entity_value','entity_group'],how='left')
            df_user_data_topic_info['topic_type']='user_data_topic'
            df_user_data_topic_info['match_type']='exact'
            df_user_data_topic_info["orders_contrib"].fillna(0, inplace=True)
            df_user_data_topic_info=df_user_data_topic_info.query("orders_contrib>40")
        else:
            print("In Normal topic flow")
            df_user_data_topic_info=df_topics_info.query("original_search_query in @input_query").drop_duplicates().copy()
            df_user_data_topic_info.entity_value = df_user_data_topic_info.entity_value.replace('\s+', ' ', regex=True)
            total_num_of_orders=df_user_data_topic_info.drop_duplicates(subset='original_search_query')['st_num_of_orders'].sum()
            df_user_data_topic_info=df_user_data_topic_info.groupby(['entity_value','entity_group']).agg({'num_of_orders':sum}).reset_index()
            df_user_data_topic_info['cluster_num_of_orders']=total_num_of_orders

            df_user_data_topic_info['orders_contrib']=(df_user_data_topic_info['num_of_orders']/df_user_data_topic_info['cluster_num_of_orders'])*100
            df_user_data_topic_info=pd.merge(df_user_data_topic_info,df_ev_details_all_for_user_data_topics,on=['entity_value','entity_group'],how='left')
            df_user_data_topic_info['topic_type']='user_data_topic'
            df_user_data_topic_info['match_type']='exact'
            df_user_data_topic_info["orders_contrib"].fillna(0, inplace=True)
    return df_user_data_topic_info
count =0 
def get_final_topics(input_query):
    st= datetime.now()
    global count
    count = count+1
    #user_data_topics
    input_query = input_query.lower()
    df_user_data_topic_info = get_user_data_topics(input_query)
#     print("USER DATA Topic ->>>>",df_user_data_topic_info)
    #text_topics
    df_text_topics,is_puq,list_unknowns = get_semantic_topics(input_query)
#     print("SEMANTIC Topic ->>>>",df_text_topics)
    
    is_symantic_topics=0
    is_user_data_topics=0
    
    if not df_user_data_topic_info.empty:
        is_user_data_topics=1
    if not df_text_topics.empty:
        is_symantic_topics=1
    #unknowns
    list_unknown_words=list_unknowns
    df_user_data_topic_info=df_user_data_topic_info[['entity_value','entity_group','entity_value_id','entity_group_id','topic_type','orders_contrib','match_type']]
    df_text_topics=df_text_topics[['entity_value','entity_group','entity_value_id','entity_group_id','topic_type','orders_contrib','match_type']]
    #final_topics
    
        
    df_final_topics =pd.concat([df_user_data_topic_info, df_text_topics], ignore_index=True)
    if df_final_topics.empty:
        final_topics_response = json.dumps([])
    else:
        df_final_topics=df_final_topics.append(pd.DataFrame({'entity_value':list_unknown_words,'entity_group':'unknown','topic_type':'text_topic','orders_contrib':0,'match_type':'unknown'}))[['entity_value','entity_group','entity_value_id','entity_group_id','topic_type','orders_contrib','match_type']]
        df_final_topics[['entity_value_id','entity_group_id']]=df_final_topics[['entity_value_id','entity_group_id']].fillna(-1).astype(int)
        df_final_topics['original_search_query']=input_query
        df_final_topics.dropna(how='any', inplace=True) 
        df_final_topics = df_final_topics.drop_duplicates(subset=['entity_value_id','topic_type'], keep="first")
        df_final_topics['match_type'] = np.where(df_final_topics['entity_value'].isin(list_exact_brand_exceptions), 
                           'partial',
                           df_final_topics['match_type'])

        df_cbe=df_final_topics.copy()
        df_cbe['entity_group_title']=np.where(df_cbe['entity_group'].isin(['brand','category','unknown']),np.where(df_cbe['topic_type'].isin(['user_data_topic']),df_cbe['entity_group'],np.where(df_cbe['topic_type'].isin(['text_topic']),'semantic_'+df_cbe['entity_group'],'semantic_entity')),'entity')
        #creating json object for each row
        df_cbe['final_object']=df_cbe.apply(lambda x:{"id":x['entity_value_id'],"name":x['entity_value'],"score":x['orders_contrib'],"topic_type":x['topic_type'],"entity_group":x['entity_group'],"match_type":x['match_type']},axis=1)
        #creating list of same group objects
        df_cbe_json=df_cbe.groupby(['original_search_query','entity_group_title'])['final_object'].apply(list).reset_index(name='final_object')
        #getting enities list group wise
        df_cbe_ids_list=df_cbe.groupby(['original_search_query','entity_group_title'])['entity_value_id'].apply(list).reset_index(name='ids_list')
        df_cbe_ids_list_match=df_cbe.groupby(['original_search_query','entity_group_title','match_type'])['entity_value_id'].apply(list).reset_index(name='ids_list_match')
        #joining entites list and cbe json
        df_cbe_json=pd.merge(df_cbe_json,df_cbe_ids_list,on=['original_search_query','entity_group_title'],how='left')
        #creating nested json for socres and ids list
        df_cbe_json['final_object']=df_cbe_json.apply(lambda x:{"scores":x['final_object'],"ids_list":x['ids_list']},axis=1)
        #creating final json list where key is entity group
        df_cbe_json=df_cbe_json.groupby("original_search_query").apply(lambda x: {i:j for i, j in zip(x["entity_group_title"], x["final_object"])}).reset_index(name='final_object')
    #             df_cbe_json['final_object']=df_cbe_json['final_object'].astype(str).str.replace("'",'"')
        semantic_brand_exact = df_cbe_ids_list_match.query("entity_group_title =='semantic_brand' and match_type=='exact'")['ids_list_match'].values
        semantic_brand_partial = df_cbe_ids_list_match.query("entity_group_title =='semantic_brand' and match_type=='partial'")['ids_list_match'].values
        semantic_category_exact = df_cbe_ids_list_match.query("entity_group_title =='semantic_category' and match_type=='exact'")['ids_list_match'].values
        semantic_category_partial = df_cbe_ids_list_match.query("entity_group_title =='semantic_category' and match_type=='partial'")['ids_list_match'].values
        entity_list = df_cbe_ids_list_match.query("entity_group_title =='entity'")['ids_list_match'].values
        
        if(len(df_cbe_json['final_object']) > 0):
            final_topics_response = df_cbe_json['final_object'][0]
            final_topics_response["is_symantic_topics"] = is_symantic_topics
            final_topics_response["is_user_data_topics"] = is_user_data_topics
            final_topics_response["semantic_brand_exact"] = semantic_brand_exact[0] if len(semantic_brand_exact) > 0 else []
            final_topics_response["semantic_brand_partial"] = semantic_brand_partial[0] if len(semantic_brand_partial) > 0 else []
            final_topics_response["semantic_category_exact"] = semantic_category_exact[0] if len(semantic_category_exact) > 0 else []
            final_topics_response["semantic_category_partial"] = semantic_category_partial[0] if len(semantic_category_partial) > 0 else []
            final_topics_response["is_puq"] = is_puq
            final_topics_response["updated_time"]=datetime.now().strftime("%d %B %Y %H:%M:%S")
            final_topics_response["updated_timestamp"]=int(time.time())
#             print("final_topics_response",final_topics_response)
            final_topics_response = json.dumps(final_topics_response)
#             print(final_topics_response)
            redis_connection_object.set("final_topics_"+input_query,final_topics_response)
        else:
            final_topics_response = json.dumps({})
    et= datetime.now()
    difference = (et - st).total_seconds()
    print("TRAINING FOR ID : {} : TERM : {} : IS_PUQ : {} : TIME TAKEN : {}".format(count,input_query,is_puq,difference))
    return final_topics_response