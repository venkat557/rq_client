#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import time
import sys,os
sys.path.append(os.environ['DATA_SCIENCE_HOME'])
import helper.mysql.get_connection  as SQL
from datetime import datetime, timedelta
# import helper.lib.utilities as UT
# import helper.mail.send_mail as EA
import common as com
from math import sqrt


## added new packages to store json data in redis
import json
import redis

# In[ ]:

def get_redis_connection():
    REDIS_HOST=os.environ['REDIS_HOST']
    REDIS_PORT=os.environ['REDIS_PORT']
    redis_connection_object=redis.StrictRedis(host='10.77.0.35', port='6379')
    try:
        redis_connection_object.ping()
        print("Successfully connected to redis")
    except (redis.exceptions.ConnectionError, ConnectionRefusedError):
        print("Redis connection error!")

    return redis_connection_object

## calling redis function and create object

redis_connection_object = get_redis_connection()


attrib_days=7
type='search'
def get_and_process_data(type):
    three_day_weightage=0.3
    seven_day_weightage=0.3
    thirty_day_weightage=0.3
    lifetime_weightage=0.1
    #Added 3 hours code for AUGUST Sell
    end_dates =[0]
    # end_dates =[174,173]
    for end_date in end_dates:
#         start_date = end_date+1/8
        day3 = end_date+3
        day7 = end_date+7
        day30 = end_date+30
        day120 = end_date+60

#         print("getting 3 hour details.......")
#         df_3_hour=com.get_keyword_product_stat(start_date,end_date,attrib_days,1,0,type)

        print("getting 3 day details.......")
        df_3_day=com.get_keyword_product_stat(end_date+3,end_date,attrib_days,5,0,type)
        # df_3_day = df_3_day.head(100000)
        print("getting 7 day details.......")
        df_7_day=com.get_keyword_product_stat(end_date+7,end_date,attrib_days,10,0,type)

        # df_7_day = df_7_day.head(100000)

        print("getting 30 day details.......")
        df_30_day=com.get_keyword_product_stat(end_date+30,end_date,attrib_days,10,1,type)

        # df_30_day = df_30_day.head(100000)

        print("getting 120 day(lifetime) details.......")
        df_120_day=com.get_keyword_product_stat(end_date+60,end_date,attrib_days,10,1,type)

        # df_120_day = df_120_day.head(100000)


        print("merging all days results....")

        def get_min_clicks_to_find_tail_queries(df_final_result):
            df = df_final_result[['keyword',str(day30)+'_num_of_clicks',str(day30)+'_num_of_orders']]
            df_clicks = df.groupby("keyword")[str(day30)+'_num_of_clicks'].agg("sum").reset_index()
            df_orders = df.groupby("keyword")[str(day30)+'_num_of_orders'].agg("sum").reset_index()
            df_final= pd.merge(df_clicks,df_orders,on=['keyword'],how="left")
            df_final = df_final.sort_values(str(day30)+"_num_of_orders",ascending=False)
            df_final_30_3 = df_final[(df_final[str(day30)+'_num_of_orders'] > 3) & (df_final[str(day30)+'_num_of_clicks'] > 10)]
            mean = df_final_30_3[str(day30)+'_num_of_clicks'].mean()
            std = df_final_30_3[str(day30)+'_num_of_clicks'].values.std(ddof=1)
            import numpy as np
            from scipy.stats import norm
            y = norm.interval(0.05, loc=mean, scale=std) # for example
            min_num_of_clicks = round(y[0])
            print("range with 95% confidence ->",y)
            df_final_30_3['is_head'] = np.where(df_final_30_3[str(day30)+'_num_of_clicks'] > min_num_of_clicks, 1, 0)
            return df_final_30_3[['keyword','is_head']]

        if type == 'search':
            #Added 3 hours code for AUGUST Sell
#             df_final_result=pd.merge(df_3_hour,df_3_day,on=['keyword','product_id','group_id','is_visible','ean_code'],how='outer')
#             df_final_result=pd.merge(df_final_result,df_7_day,on=['keyword','product_id','group_id','is_visible','ean_code'],how='outer')
            df_final_result=pd.merge(df_3_day,df_7_day,on=['keyword','product_id','group_id','is_visible','ean_code'],how='outer')
            del df_3_day
            del df_7_day
            df_final_result=pd.merge(df_final_result,df_30_day,on=['keyword','product_id','group_id','is_visible','ean_code'],how='outer')
            del df_30_day
            df_final_result=pd.merge(df_final_result,df_120_day,on=['keyword','product_id','group_id','is_visible','ean_code'],how='outer')
            del df_120_day
            print("claculating final score.....")    
            for each_column in df_final_result.columns.drop(['product_id']):
                df_final_result[each_column]=df_final_result[each_column].fillna(0)

            #Added 3 hours code for AUGUST Sell
#             df_final_result = com.normalize_search_data(df_final_result,'keyword',str(start_date)+'_group_score')
            df_final_result = com.normalize_search_data(df_final_result,'keyword',str(day3)+'_group_score')
            df_final_result = com.normalize_search_data(df_final_result,'keyword',str(day7)+'_group_score')
            df_final_result = com.normalize_search_data(df_final_result,'keyword',str(day30)+'_group_score')
            df_final_result = com.normalize_search_data(df_final_result,'keyword',str(day120)+'_group_score')
            #Added 3 hours code for AUGUST Sell

            df_final_30_3 = get_min_clicks_to_find_tail_queries(df_final_result)
            df_final_result = pd.merge(df_final_result,df_final_30_3,on=["keyword"],how="left")
            df_final_result['is_head'] = df_final_result['is_head'].fillna(0).astype(int)

            del df_final_30_3

            df_final_result['group_score'] = np.where(df_final_result['is_head'] == 1, ((three_day_weightage*df_final_result[str(day3)+'_group_score'])+(seven_day_weightage*df_final_result[str(day7)+'_group_score'])+(thirty_day_weightage*df_final_result[str(day30)+'_group_score'])+(lifetime_weightage*df_final_result[str(day120)+'_group_score'])), ((0.1*df_final_result[str(day3)+'_group_score'])+(0.1*df_final_result[str(day7)+'_group_score'])+(0.1*df_final_result[str(day30)+'_group_score'])+(0.7*df_final_result[str(day120)+'_group_score'])))
            #Added 3 hours code for AUGUST Sell
#             df_final_result['group_score'] = np.where(df_final_result['is_head'] == 1, 0.5*(df_final_result[str(start_date)+'_group_score'])+0.5*((three_day_weightage*df_final_result[str(day3)+'_group_score'])+(seven_day_weightage*df_final_result[str(day7)+'_group_score'])+(thirty_day_weightage*df_final_result[str(day30)+'_group_score'])+(lifetime_weightage*df_final_result[str(day120)+'_group_score'])), 0.5*(df_final_result[str(start_date)+'_group_score'])+0.5*((0.1*df_final_result[str(day3)+'_group_score'])+(0.1*df_final_result[str(day7)+'_group_score'])+(0.1*df_final_result[str(day30)+'_group_score'])+(0.7*df_final_result[str(day120)+'_group_score'])))

            df_final_result=df_final_result.query("group_score!=inf")
            df_final_result=df_final_result.drop_duplicates()

            df_final_result['final_score'] = np.where(df_final_result['is_head'] == 1, ((three_day_weightage*df_final_result[str(day3)+'_indivisual_score'])+(seven_day_weightage*df_final_result[str(day7)+'_indivisual_score'])+(thirty_day_weightage*df_final_result[str(day30)+'_indivisual_score'])+(lifetime_weightage*df_final_result[str(day120)+'_indivisual_score'])), ((0.1*df_final_result[str(day3)+'_indivisual_score'])+(0.1*df_final_result[str(day7)+'_indivisual_score'])+(0.1*df_final_result[str(day30)+'_indivisual_score'])+(0.7*df_final_result[str(day120)+'_indivisual_score'])))
            #Added 3 hours code for AUGUST Sell
#             df_final_result['final_score'] = np.where(df_final_result['is_head'] == 1, 0.5*(df_final_result[str(start_date)+'_indivisual_score'])+0.5*((three_day_weightage*df_final_result[str(day3)+'_indivisual_score'])+(seven_day_weightage*df_final_result[str(day7)+'_indivisual_score'])+(thirty_day_weightage*df_final_result[str(day30)+'_indivisual_score'])+(lifetime_weightage*df_final_result[str(day120)+'_indivisual_score'])), 0.5*(df_final_result[str(start_date)+'_indivisual_score'])+0.5*((0.1*df_final_result[str(day3)+'_indivisual_score'])+(0.1*df_final_result[str(day7)+'_indivisual_score'])+(0.1*df_final_result[str(day30)+'_indivisual_score'])+(0.7*df_final_result[str(day120)+'_indivisual_score'])))

        #       df_final_result['indivisual_score']=((three_day_weightage*df_final_result['3_indivisual_score'])+(seven_day_weightage*df_final_result['7_indivisual_score'])+(thirty_day_weightage*df_final_result['30_indivisual_score'])+(lifetime_weightage*df_final_result['60_indivisual_score']))
            df_final_result=df_final_result.query("final_score!=inf")
            df_final_result=df_final_result.drop_duplicates()

            #Added 3 hours code for AUGUST Sell
#             df_final_result.columns=df_final_result.columns.str.replace(str(start_date)+'_','three_hr_')
    #         df_final_result.columns=df_final_result.columns.str.replace('1_','one_')
            df_final_result.columns=df_final_result.columns.str.replace(str(day3)+'_','three_')
            df_final_result.columns=df_final_result.columns.str.replace(str(day7)+'_','seven_')
            df_final_result.columns=df_final_result.columns.str.replace(str(day30)+'_','thirty_')
            df_final_result.columns=df_final_result.columns.str.replace(str(day120)+'_','lifetime_')
            df_final_result.columns=df_final_result.columns.str.lower()
            df_final_result = com.normalize_search_data(df_final_result,'keyword','group_score')
            df_final_result['group_score'] = df_final_result['group_score'].fillna(0)
            df_final_result = com.normalize_search_data(df_final_result,'keyword','final_score')
            df_final_result['final_score'] = df_final_result['final_score'].fillna(0)
            del df_final_result['is_head']
            df_final_result['id'] = df_final_result.groupby(['keyword']).ngroup()

            #newly added code for HR buckets
            print("getting relevant products.....")
            query_relevant_product="""select original_search_query as keyword,product_id,1 as is_relevant from search_term_high_relevant_products where relevance_bucket='HR'"""
            df_relevant_product=SQL.fetch_data_with_schema(query_relevant_product,'warehouse')
            del query_relevant_product
            df_final_result=pd.merge(df_final_result,df_relevant_product,on=['keyword','product_id'],how='outer')
            df_final_result['is_relevant'] = df_final_result['is_relevant'].fillna(0).astype(int)

            df_final_result = com.normalize_search_data(df_final_result,'keyword','final_score')
            # df_final_result['final_score'] = np.where( ( (df_final_result['is_relevant'] == 1) & (df_final_result['final_score']<11) ),(11+(df_final_result['final_score'])),(df_final_result['final_score']))
            # df_final_result['group_score'] = np.where( ( (df_final_result['is_relevant'] == 1) & (df_final_result['group_score']<11) ),(11+(df_final_result['group_score'])),(df_final_result['group_score']))
            df_final_result['final_score'] = np.where( ( (df_final_result['is_relevant'] == 1) ),(11+(df_final_result['final_score'])),(df_final_result['final_score']))
            df_final_result['group_score'] = np.where( ( (df_final_result['is_relevant'] == 1)  ),(11+(df_final_result['group_score'])),(df_final_result['group_score']))
            df_final_result['group_score'] = np.where( ( (df_final_result['is_relevant'] == 1) & (df_final_result['group_score']<=0) ),(df_final_result['final_score']),(df_final_result['group_score']))

            df_final_result=df_final_result.drop_duplicates()
            df_final_result = df_final_result.fillna(0)

            df_final_result = com.normalize_search_data(df_final_result,'keyword','final_score')
            df_final_result = com.normalize_search_data(df_final_result,'keyword','group_score')
            print(df_final_result.head(10))
            df_final_result['final_score'] = np.where( ( (df_final_result['is_relevant'] == 1) & (df_final_result['final_score']<11) ),(11),(df_final_result['final_score']))
            df_final_result=df_final_result.sort_values(['group_score','final_score','thirty_num_of_orders','seven_num_of_orders','three_num_of_orders'],ascending=[False,False,False,False,False]).groupby(['keyword', 'product_id', 'is_visible', 'group_id','id','is_relevant']).head(1).copy()
            for each_column in ['three', 'thirty', 'lifetime', 'seven']:
                df_final_result[each_column+'_nmv']=df_final_result[each_column+'_final_nmv']-df_final_result[each_column+'_feature_nmv']

            df_search_term_production_columns=SQL.fetch_data_with_schema("select * from search_term_wise_product_ranking limit 1","slave")
            df_final_result=df_final_result[list(df_search_term_production_columns.columns)].drop_duplicates()
            print(df_final_result.shape)
            return df_final_result
    
def importDataInChuncks(df_data,max_rows,db,table_name):
    end_date = datetime.today() - timedelta(days=0)
    total_count=df_data.shape[0]
    total_splits=int(total_count/max_rows)+1
    df_splits = np.array_split(df_data, total_splits)
    result=True
    count =1
    for df_chunk in df_splits :
        print("pushing chunk :"+db+" in table :"+table_name+".....",str(count)+'/'+str(total_splits))
        SQL.insert_dataframe_to_db(df_chunk,db,table_name)
        count =count+1
    return result

def insertIntoUpdateIndex():
    query_update_index_products="select distinct product_id from product_update_index"
    df_up =SQL.fetch_data_with_schema(query_update_index_products,"slave")
    query_products= "select product_id from product_master"
    df_p =SQL.fetch_data_with_schema(query_products,"warehouse")
    f_df = df_p.merge(df_up,how="left",on="product_id",indicator=True)
    df_data = f_df.query("_merge=='left_only'").drop('_merge', 1)
    df_update_index = pd.DataFrame({'product_id':df_data.product_id.unique(),'time_stamp':int(time.time()) },index=None)
    SQL.insert_dataframe_to_db(df_update_index,"master","product_update_index")
    
def insertDataIntoDb(df_final_result,table_name="search_term_wise_product_ranking"):
    SQL.update_data("truncate table {}".format(table_name),"","warehouse")
    if_done = False
    if_done = importDataInChuncks(df_final_result,10000,"warehouse",table_name)
    COLUMNS =df_final_result.columns
    df_final_result = pd.DataFrame(columns=COLUMNS)
    if if_done:
        query_search_data = "select * from search_term_wise_product_ranking"
        df_final_result=SQL.fetch_data_with_schema(query_search_data,"warehouse")
#         del df_final_result['is_relevant']
#         del df_final_result['product_final_score']
        if(len(df_final_result) > 0):
            SQL.update_data("truncate table search_term_wise_product_ranking","","master")
            if_done = importDataInChuncks(df_final_result,10000,"master",'search_term_wise_product_ranking')
            if if_done:
#                 df_update_index = pd.DataFrame({'product_id':df_final_result.product_id.unique(),'time_stamp':int(time.time()) },index=None)
                insertIntoUpdateIndex()


def store_search_ranking_by_group_data_in_redis(df_final_result):
    df_final_result_limit_columns = df_final_result[['product_id','keyword','id','group_score','final_score']]
    json_string = df_final_result_limit_columns.to_json(orient="records")
    json_object = json.loads(json_string)
    dict_obj = {}
    for val in json_object:
        dict_obj['search_ranking_group_by_product_'+str(val['product_id'])] = []
    for val in json_object:
        product_id = val['product_id']
        val.pop('product_id',None)
        dict_obj['search_ranking_group_by_product_'+str(product_id)].append(val)
    for key, value in dict_obj.items():
        print("the keys is >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
        print(key)
        redis_connection_object.set(key,str(value))
    
    # call the insertIntoUpdateIndex() function to update new product id from product_master to product_update_index
    # insertIntoUpdateIndex()
    
    return True

                
# def main():
#     print("step 1 ->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> get_and_process_data starts")
#     df_final_result = get_and_process_data('search')
#     df_final_result = df_final_result.fillna(0)
#     # print("step 2 ->> insertDataIntoDb starts")
#     # insertDataIntoDb(df_final_result,"search_term_wise_product_ranking")

#     print("step 2 ->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> insertDataInto Redis starts")
#     df_final_result_sample = df_final_result.head(1000)
#     store_search_ranking_by_group_data_in_redis(df_final_result_sample)
#     print("step 3 ->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> DONE")
#     # return df_final_result
#     return True

# try :
#     df_final_result=main()
#     is_alright = True
# except :
#     EA.send_mail(sys.exc_info(),"script","SearchRanking","Production")
#     import helper.lib.utilities as UT
#     UT.send_sms("SearchRanking", sys.exec_info())
# main()


# In[ ]:


print("step 1 ->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> get_and_process_data starts")
df_final_result = get_and_process_data('search')
df_final_result = df_final_result.fillna(0)
# print("step 2 ->> insertDataIntoDb starts")
# insertDataIntoDb(df_final_result,"search_term_wise_product_ranking")

print("step 2 ->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> insertDataInto Redis starts")
df_final_result_sample = df_final_result.head(1000)
# store_search_ranking_by_group_data_in_redis(df_final_result_sample)
# print("step 3 ->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> DONE")

