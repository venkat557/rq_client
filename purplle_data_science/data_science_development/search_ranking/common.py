#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import pandas as pd
import numpy as np
import time
import sys,os
sys.path.append(os.environ['DATA_SCIENCE_HOME'])
import helper.mysql.get_connection  as SQL
from datetime import datetime, timedelta
# import helper.lib.utilities as UT
# import helper.mail.send_mail as EA
from math import sqrt
ctr_weightage=0
num_of_orders_weightage=1
sale_start_date = '2021-02-08'
sale_end_date = '2021-02-12'

def get_impressions(start_date,end_date,impressions_threshold,type='search'):
    print("getting impressions info.....")
    if type =='search':
        query_st_product_impressions="""
        select IF(page_type = 'listing_search',lower(page_type_value),lower(page_title)) as keyword,targetentityid as product_id,count(*) as num_of_impressions
        from `datos_pocos.listing_impression` 
        where event_timestamp between '{}' and '{}'
        and page_type in('listing_search','listing_brand','listing_category')
        group by keyword,targetentityid
        having num_of_impressions>{}
        order by num_of_impressions desc
        """.format(start_date,end_date,impressions_threshold)
    if type == 'product':
        query_st_product_impressions="""
        select targetentityid as product_id,count(*) as num_of_impressions
        from `datos_pocos.listing_impression` 
        where event_timestamp between '{}' and '{}'
        and event_timestamp not between '{}' and '{}'
        group by targetentityid
        having num_of_impressions>{}
        order by num_of_impressions desc
        """.format(start_date,end_date,sale_start_date,sale_end_date,impressions_threshold)
    
    df_st_product_impressions=SQL.fetch_data_with_schema(query_st_product_impressions,"bigquery")
    return df_st_product_impressions

def get_clicks(start_date,end_date,type='search'):
    print("getting clicks info.....")
    if type =='search':
        query_st_product_clicks="""
        select IF(page_type = 'listing_search',lower(page_type_value),lower(page_title)) as keyword,targetentityid as product_id,count(*) as num_of_clicks
        from `datos_pocos.listing_click` 
        where event_timestamp between '{}' and '{}'
        and page_type in('listing_search','listing_brand','listing_category')
        group by keyword,targetentityid
        order by num_of_clicks desc
        """.format(start_date,end_date)
    if type == 'product':
        query_st_product_clicks="""
        select targetentityid as product_id,count(*) as num_of_clicks
        from `datos_pocos.listing_click` 
        where event_timestamp between '{}' and '{}'
        and event_timestamp not between '{}' and '{}'
        group by targetentityid
        order by num_of_clicks desc
        """.format(start_date,end_date,sale_start_date,sale_end_date)
    df_st_product_clicks=SQL.fetch_data_with_schema(query_st_product_clicks,"bigquery")
    return df_st_product_clicks

def get_performance(start_date,end_date,attrib_days,type,start_previous_days):
    print("fetching retailer query")
    retailer_query="""select distinct cl.contact_id from customer_label_new cl"""
    df_retailer_query = SQL.fetch_dataframe(retailer_query, 'warehouse')
    print("retailer query completed")
    retailer="','".join(str(e) for e in  list(df_retailer_query["contact_id"].fillna(0).astype(int)))
    retailer_order_query = """select distinct track_id from shop_order where contact_id in ('{}')
    and time_stamp between unix_timestamp('{}') and unix_timestamp('{}')""".format(retailer,start_date,end_date)
    df_retailer_order_query = SQL.fetch_dataframe(retailer_order_query, 'slave')
    retailer="','".join(str(e) for e in  list(df_retailer_order_query["track_id"].fillna(0)))

    print("getting listig search- product performance info.....")
    if type =='search':
        query_st_rp="""
        select keyword,targetentityid as product_id,sum(search_nmv) as nmv,sum(num_of_orders) as num_of_orders 
        from(
              select IF(page_type = 'listing_search',lower(page_type_value),IF(page_type = 'listing_category',lower(category_name),lower(brand_name))) as keyword
              ,targetentityid,sum(quantity) as quantity,our_price,(sum(quantity)*our_price) as search_nmv
              ,count(distinct order_id) as num_of_orders
              from `datos_pocos.recommendation_performance` 
              where buy_eventtime between unix_seconds(TIMESTAMP('{}')) and unix_seconds(TIMESTAMP('{}'))
              and activity_eventtime between unix_seconds(TIMESTAMP('{}'))-7*86400 and unix_seconds(TIMESTAMP('{}'))
              and page_type in('listing_search','listing_brand','listing_category')
              and buy_eventtime between activity_eventtime and activity_eventtime+{}*86400
              and identifier not in ('{}') and our_price > 0
              group by keyword,targetentityid,our_price,order_id
            ) as dt
          group by keyword,targetentityid
          order by num_of_orders desc
        """.format(start_date,end_date,start_date,end_date,attrib_days,retailer)
        df_st_rp=SQL.fetch_data_with_schema(query_st_rp,"bigquery")
    if type == 'product':
        if start_previous_days >= 1:
            query_st_rp="""
            select targetentityid as product_id,sum(num_of_orders) as num_of_orders,sum(product_nmv) as nmv
            from(
              select targetentityid,count(distinct order_id) as num_of_orders,(sum(quantity)*our_price) as product_nmv
              from `datos_pocos.recommendation_performance` 
              where buy_eventtime between unix_seconds(TIMESTAMP('{}')) and unix_seconds(TIMESTAMP('{}'))
              and buy_eventtime not between unix_seconds(TIMESTAMP('{}')) and unix_seconds(TIMESTAMP('{}'))
              and activity_eventtime between unix_seconds(TIMESTAMP('{}'))-7*86400 and unix_seconds(TIMESTAMP('{}'))
              and buy_eventtime between activity_eventtime and activity_eventtime+{}*86400
              and identifier not in ('{}') and our_price > 0
              group by targetentityid,our_price,order_id

            ) as dt
            group by targetentityid
            order by num_of_orders desc
            """.format(start_date,end_date,sale_start_date,sale_end_date,start_date,end_date,attrib_days,retailer)
        else:
            query_st_rp="""select targetentityid as product_id,sum(num_of_orders) as num_of_orders,sum(product_nmv) as nmv from
                (
                select `datos_pocos.buy`.targetentityid,count(distinct `datos_pocos.buy`.order_id) as num_of_orders,(sum(quantity)*our_price) as product_nmv from `datos_pocos.buy` 
                join `datos_pocos.order_status` ON  `datos_pocos.buy`.order_id= `datos_pocos.order_status`.order_id
                where `datos_pocos.buy`.event_timestamp between timestamp('{}') and TIMESTAMP('{}')
                and `datos_pocos.buy`.our_price > 0 
                and identifier not in ('{}')
                and `datos_pocos.order_status`.order_status in ('Verified','In Process','Complete','Partly Shipped','Shipped')
                group by `datos_pocos.buy`.targetentityid,`datos_pocos.buy`.our_price,`datos_pocos.buy`.order_id
                ) as dt 
                group by targetentityid order by num_of_orders desc
                """.format(start_date,end_date,retailer)
        df_st_rp=SQL.fetch_data_with_schema(query_st_rp,"bigquery")
        print(df_st_rp.shape)
    return df_st_rp

def get_product_groups():
    print("getting products Group variants.....")
    query_product_group="""SELECT po.product_id,po.group_id,pg.is_visible FROM product_productvariants_options po join product_productvariants_group pg 
                        on po.group_id = pg.group_id"""
    df_product_group=SQL.fetch_data_with_schema(query_product_group,"slave")
    return df_product_group

def get_feature_clicks(start_previous_days,type='search'):
    print("getting feature clicks on Group variants.....")
    if type =='search':
        query="select keyword,product_id,no_of_feature_clicks,days from search_group_feature_values where days = {}".format(start_previous_days)
    if type == 'product':
        query="select product_id,no_of_feature_clicks,days from product_group_feature_values where days = {}".format(start_previous_days)
    df_group_clicks=SQL.fetch_data_with_schema(query,"warehouse")
    del df_group_clicks['days']
    return df_group_clicks

def get_feature_nmv(start_previous_days,type='search'):
    print("getting feature nmv on Group variants.....")
    if type =='search':
        query="select keyword,product_id,nmv as feature_nmv,days from search_group_feature_values where days = {}".format(start_previous_days)
    if type == 'product':
        query="select product_id,nmv as feature_nmv,days from product_group_feature_values where days = {}".format(start_previous_days)
    df_group_clicks=SQL.fetch_data_with_schema(query,"warehouse")
    del df_group_clicks['days']
    return df_group_clicks

def get_lifetime_stat():
    print("getting Lifetime stats.....")
    query="select * from lifetime_search_term_wise_ranking_p2 WHERE 60_final_score > 0"
    df_lifetime=SQL.fetch_data_with_schema(query,"warehouse")
    return df_lifetime


def get_product_ean_code():
    query="select ean_code,id product_id from product_product where `group` in(0,2)"
    df_ean = SQL.fetch_data_with_schema(query,"slave")
    return df_ean

df_ean = get_product_ean_code()

def get_hr_products():
    print("getting hr products....")
    query_relevant_product="""select original_search_query as keyword,product_id,1 as is_relevant 
    from search_term_high_relevant_products 
    where relevance_bucket='HR'"""
    df_relevant_product=SQL.fetch_data_with_schema(query_relevant_product,'warehouse')
    return df_relevant_product

def get_product_ranking_attribs(start_previous_days):
    string_to_replace_dict={3:'three_',7:'seven_',30:'thirty_',60:'lifetime_',120:'lifetime_'}
    string_to_replace_temp=(string_to_replace_dict[start_previous_days])
    columns_to_select="product_id,is_visible,group_id,{}num_of_impressions,{}num_of_clicks,{}nmv,{}num_of_orders,{}no_of_feature_clicks,{}feature_nmv,{}final_nmv".format(string_to_replace_temp,string_to_replace_temp,string_to_replace_temp,string_to_replace_temp,string_to_replace_temp,string_to_replace_temp,string_to_replace_temp)
    
    print("getting _product_ranking_attribs.....",columns_to_select)
    query_product_ranking_attribs="""
    SELECT {}
    FROM product_ranking
    """.format(columns_to_select)
    df_product_ranking_attribs=SQL.fetch_data_with_schema(query_product_ranking_attribs,"warehouse")
    df_product_ranking_attribs.columns=df_product_ranking_attribs.columns.str.replace(string_to_replace_dict[start_previous_days],'')
    return df_product_ranking_attribs

def get_hr_products_all_metrics(start_previous_days,df_keyword_pool_final):
    df_relevant_product=get_hr_products()
    df_product_groups=get_product_groups()
    df_product_ranking_attribs=get_product_ranking_attribs(start_previous_days)
    #getting relevant products group ids and is visible flag
    df_relevant_product=pd.merge(df_relevant_product,df_product_groups,on=['product_id'],how='left')
    df_relevant_product=pd.merge(df_ean,df_relevant_product,on=['product_id'],how='left')
    df_relevant_product['group_id']=np.where(df_relevant_product['group_id'].isnull(),df_relevant_product['product_id'],df_relevant_product['group_id']).astype(int)
    df_relevant_product['is_visible']=df_relevant_product['is_visible'].fillna(0).astype(int)
    #getting relevant products ranking attribs
    df_relevant_product=pd.merge(df_relevant_product,df_product_ranking_attribs,on=['product_id','is_visible','group_id'],how='left')
    #getting existing products is_parent flag
    df_is_parent=df_keyword_pool_final.groupby(['keyword','group_id','is_visible']).agg({'is_parent':'max'}).reset_index()
    #making existing is_parent if present otherwise corresponding products as is_parent
    df_relevant_product=pd.merge(df_relevant_product,df_is_parent,on=['keyword','group_id','is_visible'],how='left')
    df_relevant_product['is_parent']=df_relevant_product['is_parent'].fillna(0).astype(int)
    df_relevant_product['is_parent']=1-df_relevant_product['is_parent']
    
    for each_column in ['num_of_impressions','num_of_clicks','num_of_orders','nmv','group_id','is_visible','no_of_feature_clicks','feature_nmv','is_parent']:
        df_relevant_product[each_column]=df_relevant_product[each_column].fillna(0).astype(int)
    
    df_relevant_product=df_relevant_product[df_keyword_pool_final.columns]
    
    for each_column in ['num_of_impressions','num_of_clicks','num_of_orders','nmv','no_of_feature_clicks','feature_nmv','final_nmv']:
        df_relevant_product[each_column]=(0.4*df_relevant_product[each_column]).fillna(0).astype(int)
    return df_relevant_product

def normalize_product_data(df_final_result,normalize_on_key):
    df_final_result[normalize_on_key] = (df_final_result[normalize_on_key] - df_final_result[normalize_on_key].min())/(df_final_result[normalize_on_key].max() - df_final_result[normalize_on_key].min())*100
    df_final_result[normalize_on_key] = df_final_result[normalize_on_key].fillna(0)
    return df_final_result

def normalize_search_data(df_final_result,group_by_key,normalize_on_key):
    grouper = df_final_result.groupby(group_by_key)[normalize_on_key]
    maxes = grouper.transform('max')
    mins = grouper.transform('min') 
    df_final_result[normalize_on_key] = (df_final_result[normalize_on_key] - mins)/(maxes - mins)*100
    df_final_result[normalize_on_key] = df_final_result[normalize_on_key].fillna(0)
    return df_final_result


def confidence(clicks, impressions):
    #n = clicks + impressions
    if impressions > clicks:
        n = impressions
    else : 
        n = clicks
        
    if n == 0: return 0
    z = 1.96 #1.96 -> 95% confidence
    if n == 0:
        return 0

    #z = 1.0 #1.44 = 85%, 1.96 = 95%
    phat = float(clicks) / n
    return ((phat + z*z/(2*n) - z * sqrt((phat*(1-phat)+z*z/(4*n))/n))/(1+z*z/n))

def wilson(x,y):
    if y == 0 or x == 0:
        return 0
    else:
        return confidence(x, y)
    

    
def process_product_data(df_keyword_pool_final,start_previous_days):
        df_keyword_pool_parent = df_keyword_pool_final[df_keyword_pool_final['group_id'] > 0].sort_values('num_of_impressions',ascending=True).groupby(['product_id','group_id']).first().reset_index()
        df_keyword_pool_parent['is_parent'] =1
        df_keyword_pool_parent = df_keyword_pool_parent[['group_id','product_id','is_parent']]
        df_keyword_pool_final = pd.merge(df_keyword_pool_final,df_keyword_pool_parent,on=['group_id','product_id'],how='left')
        for each_column in ['num_of_impressions','num_of_clicks','num_of_orders','nmv','group_id','is_visible','no_of_feature_clicks','feature_nmv','is_parent']:
            df_keyword_pool_final[each_column]=df_keyword_pool_final[each_column].fillna(0).astype(int)
        df_keyword_pool_final=df_keyword_pool_final.assign(group_id=(df_keyword_pool_final.group_id).where(df_keyword_pool_final.group_id > 0,df_keyword_pool_final.product_id))

        df_keyword_pool_final=df_keyword_pool_final.assign(num_of_impressions=(df_keyword_pool_final.num_of_impressions).where(df_keyword_pool_final.is_parent > 0,(df_keyword_pool_final.num_of_impressions+df_keyword_pool_final.num_of_clicks)))
        df_keyword_pool_final=df_keyword_pool_final.assign(num_of_clicks=(df_keyword_pool_final.num_of_clicks+df_keyword_pool_final.no_of_feature_clicks).where(df_keyword_pool_final.is_parent > 0,df_keyword_pool_final.no_of_feature_clicks))
        df_keyword_pool_final['final_nmv']=df_keyword_pool_final['nmv']+df_keyword_pool_final['feature_nmv']
        
        df_keyword_pool_final = df_keyword_pool_final.assign(num_of_impressions=df_keyword_pool_final.groupby(['ean_code'])['num_of_impressions'].transform('sum'))
        df_keyword_pool_final = df_keyword_pool_final.assign(num_of_clicks=df_keyword_pool_final.groupby(['ean_code'])['num_of_clicks'].transform('sum'))
        df_keyword_pool_final = df_keyword_pool_final.assign(final_nmv=df_keyword_pool_final.groupby(['ean_code'])['final_nmv'].transform('sum'))
        df_keyword_pool_final = df_keyword_pool_final.assign(num_of_orders=df_keyword_pool_final.groupby(['ean_code'])['num_of_orders'].transform('sum'))
        
        
        
        df_keyword_pool_final = df_keyword_pool_final.assign(total_group_impressions=df_keyword_pool_final.groupby(['group_id','is_visible'])['num_of_impressions'].transform('sum'))
        df_keyword_pool_final = df_keyword_pool_final.assign(total_group_clicks=df_keyword_pool_final.groupby(['is_visible','group_id'])['num_of_clicks'].transform('sum'))
        df_keyword_pool_final = df_keyword_pool_final.assign(total_group_nmv=df_keyword_pool_final.groupby(['is_visible','group_id'])['final_nmv'].transform('sum'))
        df_keyword_pool_final = df_keyword_pool_final.assign(total_group_orders=df_keyword_pool_final.groupby(['is_visible','group_id'])['num_of_orders'].transform('sum'))
        df_keyword_pool_final['nmv_per_impression_order_w']=df_keyword_pool_final.apply(lambda x: wilson(x['final_nmv'], x['num_of_impressions']), axis=1)*(df_keyword_pool_final['num_of_orders'])
        df_keyword_pool_final['group_nmv_per_impression_order_w']=df_keyword_pool_final.apply(lambda x: wilson(x['total_group_nmv'], x['total_group_impressions']), axis=1)*(df_keyword_pool_final['total_group_orders'])
        df_keyword_pool_final=df_keyword_pool_final.assign(group_score=(num_of_orders_weightage*df_keyword_pool_final['group_nmv_per_impression_order_w']).where(df_keyword_pool_final.is_visible == 1,num_of_orders_weightage*df_keyword_pool_final['nmv_per_impression_order_w']))
        df_keyword_pool_final['indivisual_score']=num_of_orders_weightage*df_keyword_pool_final['nmv_per_impression_order_w']
        df_keyword_pool_final=df_keyword_pool_final.fillna(0)
        del df_keyword_pool_final['is_parent']
        df_keyword_pool_final.columns=str(start_previous_days)+"_"+df_keyword_pool_final.columns
        df_keyword_pool_final.rename(columns={str(start_previous_days)+'_product_id':'product_id',str(start_previous_days)+'_group_id':'group_id',str(start_previous_days)+'_is_visible':'is_visible',str(start_previous_days)+'_ean_code':'ean_code'},inplace=True)
#         df_keyword_pool_final=df_keyword_pool_final.drop_duplicates()
        return df_keyword_pool_final
    
def process_search_data(df_keyword_pool_final,start_previous_days):
    df_keyword_pool_parent = df_keyword_pool_final[df_keyword_pool_final['group_id'] > 0].sort_values('num_of_impressions',ascending=True).groupby(['keyword','group_id']).first().reset_index()
    df_keyword_pool_parent['is_parent'] =1
    df_keyword_pool_parent = df_keyword_pool_parent[['keyword','group_id','product_id','is_parent']]
    df_keyword_pool_final = pd.merge(df_keyword_pool_final,df_keyword_pool_parent,on=['keyword','group_id','product_id'],how='left')
    for each_column in ['num_of_impressions','num_of_clicks','num_of_orders','nmv','group_id','is_visible','no_of_feature_clicks','feature_nmv','is_parent']:
        df_keyword_pool_final[each_column]=df_keyword_pool_final[each_column].fillna(0).astype(int)
    df_keyword_pool_final=df_keyword_pool_final.assign(group_id=(df_keyword_pool_final.group_id).where(df_keyword_pool_final.group_id > 0,df_keyword_pool_final.product_id))
    df_keyword_pool_final=df_keyword_pool_final.assign(num_of_impressions=(df_keyword_pool_final.num_of_impressions).where(df_keyword_pool_final.is_parent > 0,(df_keyword_pool_final.num_of_impressions+df_keyword_pool_final.num_of_clicks)))
    df_keyword_pool_final=df_keyword_pool_final.assign(num_of_clicks=(df_keyword_pool_final.num_of_clicks+df_keyword_pool_final.no_of_feature_clicks).where(df_keyword_pool_final.is_parent > 0,df_keyword_pool_final.no_of_feature_clicks))
    df_keyword_pool_final['final_nmv']=df_keyword_pool_final['nmv']+df_keyword_pool_final['feature_nmv']
    df_keyword_pool_final=df_keyword_pool_final.drop_duplicates()
    #getting hr products all metrics and add 40% of this metrics to search contiribution
    df_hr_products_all_metrics=get_hr_products_all_metrics(start_previous_days,df_keyword_pool_final)
    df_hr_products_all_metrics=df_hr_products_all_metrics.drop_duplicates()
    df_keyword_pool_final=df_keyword_pool_final.append(df_hr_products_all_metrics,ignore_index=True)
    print("completed df_hr_products_all_metrics....")
    #df_keyword_pool_final=df_keyword_pool_final.groupby(['keyword','ean_code','product_id','is_visible','is_parent','group_id']).agg({'num_of_impressions':'sum','num_of_clicks':'sum','num_of_orders':'sum','no_of_feature_clicks':'sum','feature_nmv':'sum','final_nmv':'sum'}).reset_index()

    df_keyword_pool_final = df_keyword_pool_final.assign(num_of_impressions=df_keyword_pool_final.groupby(['keyword','ean_code'])['num_of_impressions'].transform('sum'))
    df_keyword_pool_final = df_keyword_pool_final.assign(num_of_clicks=df_keyword_pool_final.groupby(['keyword','ean_code'])['num_of_clicks'].transform('sum'))
    df_keyword_pool_final = df_keyword_pool_final.assign(final_nmv=df_keyword_pool_final.groupby(['keyword','ean_code'])['final_nmv'].transform('sum'))
    df_keyword_pool_final = df_keyword_pool_final.assign(num_of_orders=df_keyword_pool_final.groupby(['keyword','ean_code'])['num_of_orders'].transform('sum'))

    #calculating group scores
    df_keyword_pool_final = df_keyword_pool_final.assign(total_group_impressions=df_keyword_pool_final.groupby(['keyword','group_id','is_visible'])['num_of_impressions'].transform('sum'))
    df_keyword_pool_final = df_keyword_pool_final.assign(total_group_clicks=df_keyword_pool_final.groupby(['keyword','group_id','is_visible'])['num_of_clicks'].transform('sum'))
    df_keyword_pool_final = df_keyword_pool_final.assign(total_group_nmv=df_keyword_pool_final.groupby(['keyword','group_id','is_visible'])['final_nmv'].transform('sum'))
    df_keyword_pool_final = df_keyword_pool_final.assign(total_group_orders=df_keyword_pool_final.groupby(['keyword','group_id','is_visible'])['num_of_orders'].transform('sum'))
    df_keyword_pool_final['nmv_per_impression_order_w']=df_keyword_pool_final.apply(lambda x: wilson(x['final_nmv'], x['num_of_impressions']), axis=1)*(df_keyword_pool_final['num_of_orders'])
    df_keyword_pool_final['group_nmv_per_impression_order_w']=df_keyword_pool_final.apply(lambda x: wilson(x['total_group_nmv'], x['total_group_impressions']), axis=1)*(df_keyword_pool_final['total_group_orders'])
    df_keyword_pool_final=df_keyword_pool_final.assign(group_score=(num_of_orders_weightage*df_keyword_pool_final['group_nmv_per_impression_order_w']).where(df_keyword_pool_final.is_visible == 1,num_of_orders_weightage*df_keyword_pool_final['nmv_per_impression_order_w']))
    df_keyword_pool_final['indivisual_score']=num_of_orders_weightage*df_keyword_pool_final['nmv_per_impression_order_w']
    df_keyword_pool_final=df_keyword_pool_final.fillna(0)
    del df_keyword_pool_final['is_parent']
    df_keyword_pool_final.columns=str(start_previous_days)+"_"+df_keyword_pool_final.columns
    df_keyword_pool_final.rename(columns={str(start_previous_days)+'_keyword':'keyword',str(start_previous_days)+'_product_id':'product_id',str(start_previous_days)+'_group_id':'group_id',str(start_previous_days)+'_is_visible':'is_visible',str(start_previous_days)+'_ean_code':'ean_code'},inplace=True)
    df_keyword_pool_final=df_keyword_pool_final.drop_duplicates()
    return df_keyword_pool_final


def get_keyword_pool(count_threshold=50):
    query_and_keywords="""
    SELECT keyword
    from search_expressions 
    where query_by='and' and count > {}
    """.format(count_threshold)
    df_and_keywords=SQL.fetch_data_with_schema(query_and_keywords,'slave')

    query_or_keywords="""
    SELECT keyword 
    from search_expressions 
    where query_by='or' and intent='intent_by' and count > {}
    """.format(count_threshold)
    df_or_keywords=SQL.fetch_data_with_schema(query_or_keywords,'slave')
    
    query_and_keywords="""select lower(selected_search_type_text) as keyword,count(*) as count from `datos_pocos.suggestion` where 
                        intent in ('search','select') group by selected_search_type_text having count>{}""".format(count_threshold)
    
    df_search_keywords=SQL.fetch_data_with_schema(query_and_keywords,'bigquery')
    del df_search_keywords['count']
    
    query_pm="""
    select lower(brand_name) as brand_name,lower(category_name) as category_name
    from product_master
    group by category_id,category_name
    """
    df_pm=SQL.fetch_data_with_schema(query_pm,'warehouse')

    df_brand_keywords=df_pm[['brand_name']].rename(columns={'brand_name':'keyword'})
    df_category_keywords=df_pm[['category_name']].rename(columns={'category_name':'keyword'})

    df_keyword_pool=df_keywords=pd.concat([df_and_keywords,df_or_keywords,df_brand_keywords,df_search_keywords,df_category_keywords])
    df_keywords['keyword']=df_keywords['keyword'].str.lower()
    df_keywords['keyword']=df_keywords['keyword'].str.rstrip()
    df_keywords['keyword']=df_keywords['keyword'].str.lstrip()
    return df_keywords
    
    
def remove_sale_dates(start_date,end_date):
    import datetime
    date_array = (start_date + datetime.timedelta(days=x) for x in range(0, (end_date-start_date).days))
    days_in_between =0
    days_not_in_between =0
    
    for date_object in date_array:
        print('date_object->',date_object.strftime("%Y-%m-%d"))
        if sale_start_date <= date_object.strftime("%Y-%m-%d") <= sale_end_date:
            days_in_between= days_in_between+1
            print("Yes!")
        else:
            days_not_in_between= days_not_in_between+1
            print("No!")
    new_start_date = (start_date - datetime.timedelta(days=days_in_between)).strftime("%Y-%m-%d")
    new_end_date = (end_date - datetime.timedelta(days=days_in_between)).strftime("%Y-%m-%d")
    print('days_in_between->',days_in_between)
    print('New start_date',new_start_date)
    print('New end_date',new_end_date)
    return new_start_date,new_end_date
    
    
def get_keyword_product_stat(start_previous_days,end_previous_days,attrib_days,impressions_threshold,order_threshold,type):
    ctr_weightage=0
    num_of_orders_weightage=1
    #Added 3 hours code for AUGUST Sell
    start_date = datetime.utcnow() - timedelta(days=start_previous_days)
    if(start_previous_days > 1):
        start_date=start_date.strftime('%Y-%m-%d')
    end_date = datetime.utcnow() - timedelta(days=end_previous_days)
    if(start_previous_days > 1):
        end_date=end_date.strftime('%Y-%m-%d')
    print("start_date :",start_date)
    print("end_date :",end_date)
#     if(start_previous_days > 1):
#         start_date,end_date = remove_sale_dates(start_date,end_date)
    print("getting impressions info.....")
    df_st_product_impressions=get_impressions(start_date,end_date,impressions_threshold,type)
    print("getting clicks info.....")
    df_st_product_clicks = get_clicks(start_date,end_date,type)
    print("getting listig search- product performance info.....")
    df_st_rp=get_performance(start_date,end_date,attrib_days,type,start_previous_days)
    print("performance data",df_st_rp.shape)
    df_product_group = get_product_groups()
    df_group_clicks = get_feature_clicks(start_previous_days,type)
    df_group_nmv = get_feature_nmv(start_previous_days,type)
    if type == 'product':
        df_keyword_pool_final=pd.merge(df_st_product_impressions,df_st_rp,on=['product_id'],how='left')
        df_keyword_pool_final=pd.merge(df_keyword_pool_final,df_st_product_clicks,on=['product_id'],how='left')
        df_keyword_pool_final=pd.merge(df_keyword_pool_final,df_group_clicks,on=['product_id'],how='left')
        df_keyword_pool_final=pd.merge(df_keyword_pool_final,df_group_nmv,on=['product_id'],how='left')
        df_keyword_pool_final=pd.merge(df_keyword_pool_final,df_product_group,on=['product_id'],how='left')
        df_keyword_pool_final=pd.merge(df_ean,df_keyword_pool_final,on=['product_id'],how='left')
        df_keyword_pool_final['product_id']=df_keyword_pool_final['product_id'].astype(int)
        df_keyword_pool_final = process_product_data(df_keyword_pool_final,start_previous_days)
    if type == 'search':
        df_keyword_pool = get_keyword_pool()
        df_keyword_pool_final=pd.merge(df_keyword_pool,df_st_product_impressions,on=['keyword'])
        df_keyword_pool_final=pd.merge(df_keyword_pool_final,df_st_product_clicks,on=['keyword','product_id'],how='left')
        df_keyword_pool_final=pd.merge(df_keyword_pool_final,df_st_rp,on=['keyword','product_id'],how='left')
        df_keyword_pool_final=pd.merge(df_keyword_pool_final,df_group_clicks,on=['keyword','product_id'],how='left')
        df_keyword_pool_final=pd.merge(df_keyword_pool_final,df_group_nmv,on=['keyword','product_id'],how='left')
        df_keyword_pool_final=pd.merge(df_keyword_pool_final,df_product_group,on=['product_id'],how='left')
        df_keyword_pool_final=pd.merge(df_ean,df_keyword_pool_final,on=['product_id'],how='left')
        df_keyword_pool_final['product_id']=df_keyword_pool_final['product_id'].astype(int)
        print(df_keyword_pool_final.head(10))
        df_keyword_pool_final = process_search_data(df_keyword_pool_final,start_previous_days)
    return df_keyword_pool_final

# def get_min_clicks_to_find_tail_queries(df_final_result):
#     df = df_final_result[['keyword','30_num_of_clicks','30_num_of_orders']]
#     df_clicks = df.groupby("keyword")['30_num_of_clicks'].agg("sum").reset_index()
#     df_orders = df.groupby("keyword")['30_num_of_orders'].agg("sum").reset_index()
#     df_final= pd.merge(df_clicks,df_orders,on=['keyword'],how="left")
#     df_final = df_final.sort_values("30_num_of_orders",ascending=False)
#     df_final_30_3 = df_final[(df_final['30_num_of_orders'] > 3) & (df_final['30_num_of_clicks'] > 10)]
#     mean = df_final_30_3['30_num_of_clicks'].mean()
#     std = df_final_30_3['30_num_of_clicks'].values.std(ddof=1)
#     import numpy as np
#     from scipy.stats import norm
#     y = norm.interval(0.05, loc=mean, scale=std) # for example
#     min_num_of_clicks = round(y[0])
#     print("range with 95% confidence ->",y)
#     df_final_30_3['is_head'] = np.where(df_final_30_3['30_num_of_clicks'] > min_num_of_clicks, 1, 0)
#     return df_final_30_3[['keyword','is_head']]


def get_min_clicks_to_find_tail_queries(df_final_result):
    df = df_final_result[['keyword',str(day30)+'_num_of_clicks',str(day30)+'_num_of_orders']]
    df_clicks = df.groupby("keyword")[str(day30)+'_num_of_clicks'].agg("sum").reset_index()
    df_orders = df.groupby("keyword")[str(day30)+'_num_of_orders'].agg("sum").reset_index()
    df_final= pd.merge(df_clicks,df_orders,on=['keyword'],how="left")
    df_final = df_final.sort_values(str(day30)+"_num_of_orders",ascending=False)
    df_final_30_3 = df_final[(df_final[str(day30)+'_num_of_orders'] > 3) & (df_final[str(day30)+'_num_of_clicks'] > 10)]
    mean = df_final_30_3[str(day30)+'_num_of_clicks'].mean()
    std = df_final_30_3[str(day30)+'_num_of_clicks'].values.std(ddof=1)
    import numpy as np
    from scipy.stats import norm
    y = norm.interval(0.05, loc=mean, scale=std) # for example
    min_num_of_clicks = round(y[0])
    print("range with 95% confidence ->",y)
    df_final_30_3['is_head'] = np.where(df_final_30_3[str(day30)+'_num_of_clicks'] > min_num_of_clicks, 1, 0)
    return df_final_30_3[['keyword','is_head']]



