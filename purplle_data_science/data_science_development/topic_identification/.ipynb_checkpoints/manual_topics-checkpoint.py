#!/usr/bin/env python
# coding: utf-8

# In[1]:


print("importing required libraries")
print("importing required libraries")
import pandas as pd
import numpy as np
import time
import sys,os
sys.path.append(os.environ['DATA_SCIENCE_HOME'])
import helper.mysql.get_connection  as SQL
from datetime import datetime, timedelta
import helper.lib.utilities as UT
import re
import time
from fuzzywuzzy import fuzz


# In[2]:


def get_ev_details():
    query_ev_details="""
    select ev.id as entity_value_id,lower(ev.entity_value) as entity_value,ev.entity_group_id,lower(eg.name) as entity_group
    from entity_values ev
    join entity_groups eg on ev.entity_group_id=eg.id
    """
    df_ev_details=SQL.fetch_data_with_schema(query_ev_details,"slave")

    query_cat="""
    select id as entity_value_id,lower(name) as entity_value,-2 as entity_group_id,'category' as entity_group
    from product_category
    group by entity_value_id
    """
    df_cat=SQL.fetch_data_with_schema(query_cat,"slave")

    query_brand="""
    select id as entity_value_id,lower(name) as entity_value,-3 as entity_group_id,'brand' as entity_group
    from product_brand
    group by entity_value_id
    """
    df_brand=SQL.fetch_data_with_schema(query_brand,"slave")

    df_ev_details=df_ev_details.append([df_cat,df_brand],ignore_index=True)
    df_ev_details['entity_value']=df_ev_details['entity_value'].str.lower()
    #df_ev_details['entity_value']=df_ev_details['entity_value'].str.replace('&','and')
    df_ev_details=df_ev_details[~df_ev_details.entity_value.isnull()]
    return df_ev_details

print("getting entity details")
df_ev=get_ev_details()
df_entity_values=df_ev[['entity_value','entity_group']].drop_duplicates()
df_entity_values['str_len']=df_entity_values.entity_value.str.len()
df_entity_values=df_entity_values.sort_values('str_len',ascending=False)


# In[3]:


def get_manual_topics(sheet_name,sub_sheet_name):
    import gspread
    import gspread_dataframe as gd
    from oauth2client.service_account import ServiceAccountCredentials

    # use creds to create a client to interact with the Google Drive API
    #scope = ['https://spreadsheets.google.com/feeds']
    scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
    creds = ServiceAccountCredentials.from_json_keyfile_name('/home/ubuntu/purplle_data_science/helper/credential/client_secret.json', scope)
    client = gspread.authorize(creds)

    sheet_getting_worksheet_names = client.open(sheet_name)
    worksheet_input=sheet_getting_worksheet_names.worksheet(sub_sheet_name)
    input_data = worksheet_input.get_all_records()

    df_brand_ids_for_filter=pd.DataFrame(input_data)
    return df_brand_ids_for_filter


# In[4]:


print("getting manual topics")
sheet_name='manual_topics'
sub_sheet_name='manual_topics_info'
df_manual_topics=get_manual_topics(sheet_name,sub_sheet_name)

df_manual_topics['is_send_to_client']=1
df_manual_topics['modified_search_query']=df_manual_topics['original_search_query']
for each_col in ['num_of_clicks','num_of_orders','st_num_of_orders','st_num_of_clicks','orders_contrib','clicks_contrib']:
    df_manual_topics[each_col]=0
    
# applying token set ratio
#token set ratio
print("applying token set ratio")
df_manual_topics['token_set_ratio']=df_manual_topics.apply(lambda x:fuzz.token_set_ratio(x['modified_search_query'],x['entity_value']),axis=1)


# In[5]:


print("getting parents ingradients info...")
query_parent_child_entities="""
select * from parent_entity_value_childs
"""
df_parent_child_entities=SQL.fetch_data_with_schema(query_parent_child_entities,"slave")

df_parent_child_entities['entity_group']='active ingredients'
df_parent_child_entities=pd.merge(df_parent_child_entities,df_ev[['entity_value_id','entity_value','entity_group']].rename(columns={'entity_value':'parent_entity'}),on=['entity_value_id','entity_group'])
df_parent_child_entities=pd.merge(df_parent_child_entities,df_ev[['entity_value_id','entity_value','entity_group']].rename(columns={'entity_value':'child_entity','entity_value_id':'child_entity_value_id'}),on=['child_entity_value_id','entity_group'])
df_parent_child_entities=df_parent_child_entities[['child_entity','parent_entity']].drop_duplicates()
df_parent_child_entities['entity_group']='active ingredients'


# In[6]:


df_cat_brand_entity=df_manual_topics.copy()
df_ev_details=df_ev.drop_duplicates(subset=['entity_value','entity_group','entity_value_id'])


# In[7]:


#replacing child entities with parent entities
df_cbe=pd.merge(df_cat_brand_entity,df_parent_child_entities.rename(columns={'child_entity':'entity_value'}),on=['entity_value','entity_group'],how='left')
df_cbe['entity_value']=np.where(df_cbe['parent_entity'].isnull(),df_cbe['entity_value'],df_cbe['parent_entity'])

df_cbe=pd.merge(df_cbe,df_ev_details,on=['entity_value','entity_group'])
df_cbe=df_cbe.drop_duplicates(subset=['original_search_query','entity_value','entity_group','entity_value_id'])
df_cbe['entity_value']=df_cbe['entity_value'].str.replace("'","#######")

for each_column in ['entity_value_id','entity_group_id']:
    df_cbe[each_column]=df_cbe[each_column].astype(int)

#replacing all entities groups as one group 'entity'
print("creating cbe json ...")
df_cbe['entity_group']=np.where(df_cbe['entity_group'].isin(['brand','category']),df_cbe['entity_group'],'entity')
#creating json object for each row
df_cbe['final_object']=df_cbe.apply(lambda x:{"id":x['entity_value_id'],"name":x['entity_value'],"score":x['orders_contrib']},axis=1)
#creating list of same group objects
df_cbe_json=df_cbe.groupby(['original_search_query','entity_group'])['final_object'].apply(list).reset_index(name='final_object')
#getting enities list group wise
df_cbe_ids_list=df_cbe.groupby(['original_search_query','entity_group'])['entity_value_id'].apply(list).reset_index(name='ids_list')
#joining entites list and cbe json
df_cbe_json=pd.merge(df_cbe_json,df_cbe_ids_list,on=['original_search_query','entity_group'],how='left')
#creating nested json for socres and ids list
df_cbe_json['final_object']=df_cbe_json.apply(lambda x:{"scores":x['final_object'],"ids_list":x['ids_list']},axis=1)
#creating final json list where key is entity group
df_cbe_json=df_cbe_json.groupby("original_search_query").apply(lambda x: {i:j for i, j in zip(x["entity_group"], x["final_object"])}).reset_index(name='final_object')
df_cbe_json['final_object']=df_cbe_json['final_object'].astype(str).str.replace("'",'"')
df_cbe_json['final_object']=df_cbe_json['final_object'].astype(str).str.replace("#######","'")


# In[8]:


df_raw_data_columns=SQL.fetch_data_with_schema("select * from topic_identification_of_query_raw_data limit 1","warehouse")
df_cbe_json_columns=SQL.fetch_data_with_schema("select * from topic_identification_of_query limit 1","warehouse")

current_timestamp=int(time.time())
df_cat_brand_entity['updated_timestamp']=current_timestamp
df_cbe_json['updated_timestamp']=current_timestamp

df_cat_brand_entity=df_cat_brand_entity[df_raw_data_columns.columns]
df_cbe_json=df_cbe_json[df_cbe_json_columns.columns]


# In[9]:


existed_queries_cat_brand_entity="','".join(df_cat_brand_entity.original_search_query.unique())
existed_queries_cat_brand_entity="'"+existed_queries_cat_brand_entity+"'"

existed_queries_cbe_json="','".join(df_cbe_json.original_search_query.unique())
existed_queries_cbe_json="'"+existed_queries_cbe_json+"'"

def import_data_in_chunks(df_data,max_rows,table_name,db):
    total_count=df_data.shape[0]
    total_splits=int(total_count/max_rows)+1
    df_splits = np.array_split(df_data, total_splits)
    result=False
    for df_chunk in df_splits :
        print("importing chunk:",df_chunk.shape)
        SQL.insert_dataframe_to_db(df_chunk,db,table_name)
        result=True
    return result

print("pushing data to topic_identification_of_query_delta....")
import_data_in_chunks(df_cbe_json,100000,"topic_identification_of_query_delta","warehouse")

print("pushing data to topic_identification_of_query....")
import_data_in_chunks(df_cbe_json,100000,"topic_identification_of_query","warehouse")

print("pushing data to topic_identification_of_query_raw_data....")
import_data_in_chunks(df_cat_brand_entity,100000,"topic_identification_of_query_raw_data","warehouse")

delete_query_for_topic_identification_of_query_raw_data ="""
delete from topic_identification_of_query_raw_data 
where updated_timestamp<{}-60 and original_search_query in ({})""".format(current_timestamp,existed_queries_cat_brand_entity)

delete_query_for_topic_identification_of_query ="""
delete from topic_identification_of_query 
where updated_timestamp<{}-60 and original_search_query in ({})""".format(current_timestamp,existed_queries_cbe_json)

print("delete_query_for_topic_identification_of_query_raw_data....")
SQL.update_data(delete_query_for_topic_identification_of_query_raw_data,"","warehouse")
print("delete_query_for_topic_identification_of_query....")
SQL.update_data(delete_query_for_topic_identification_of_query,"","warehouse")


# ----
# rough work
# ----
