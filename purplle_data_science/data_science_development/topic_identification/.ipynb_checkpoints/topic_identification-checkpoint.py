#!/usr/bin/env python
# coding: utf-8

# importing required libraries

# In[1]:


import pandas as pd
import numpy as np
import time
import sys,os
sys.path.append(os.environ['DATA_SCIENCE_HOME'])
import helper.mysql.get_connection  as SQL
from datetime import datetime, timedelta
import helper.lib.utilities as UT
import re

from nltk.corpus import stopwords 
import nltk, re, pprint, sys,os,json
from nltk.corpus.reader import ConllChunkCorpusReader 
from nltk import conlltags2tree, tree2conlltags
from nltk import word_tokenize, pos_tag, ne_chunk
from nltk.stem.wordnet import WordNetLemmatizer 
from fuzzywuzzy import fuzz


# setting threshold values and num of previous days for user data

# In[3]:


start_previous_days=180
end_previous_days=0
clicks_start_previous_days=start_previous_days+7
start_date = datetime.today() - timedelta(days=start_previous_days)
start_date=start_date.strftime('%Y-%m-%d')
clicks_start_date = datetime.today() - timedelta(days=clicks_start_previous_days)
clicks_start_date=clicks_start_date.strftime('%Y-%m-%d')
end_date = datetime.today() - timedelta(days=end_previous_days)
end_date=end_date.strftime('%Y-%m-%d')

num_of_clicks_threshold=10
num_of_orders_threshold=3
final_score_threshold_in_search_term_wise_product_ranking=10
thirty_num_of_orders_threshold=2

cat_token_set_ratio=60
cat_higher_order_contribution_threshold=70
cat_lower_order_contribution_threshold=5

brand_token_set_ratio=50
brand_order_contribution_threshold=70

entity_token_set_ratio=60
entity_order_contribution_threshold=65


# getting retailers info(contact ids and their trackids....)

# In[ ]:


#retailer identifiers.....
query_retailer_contact_ids="""select distinct cl.contact_id from customer_label_new cl"""
df_retailer_contact_ids = SQL.fetch_dataframe(query_retailer_contact_ids, 'warehouse')
retailer_contact_ids="','".join(str(e) for e in  list(df_retailer_contact_ids["contact_id"].fillna(0).astype(int)))
print("retailer_contact_ids query completed")
query_retailer_track_ids = """select distinct track_id from shop_order where contact_id in ('{}')""".format(retailer_contact_ids)
df_retailer_track_ids = SQL.fetch_dataframe(query_retailer_track_ids, 'delayed_slave')
df_retailer_track_ids['track_id']=df_retailer_track_ids['track_id'].fillna('null')
df_retailer_track_ids=df_retailer_track_ids[df_retailer_track_ids.track_id.apply(lambda x: x.isnumeric())]
retailer_track_ids="'"+"','".join(str(e) for e in  list(df_retailer_track_ids["track_id"].fillna(0)))+"'"


# existing product tagging function

# In[ ]:


def get_old_tagging(product_id_list):
    product_ids=",".join(str(x) for x in product_id_list)
    query_old_tagging="""
    select module_id as product_id,ev.id as entity_value_id,lower(ev.entity_value) as entity_value,ev.entity_group_id as entity_group_id,lower(eg.name) as entity_group
    from entity_products as ep
    join  entity_values ev on ev.id=ep.entity_value_id
    join entity_groups eg on ev.entity_group_id=eg.id
    where module='product'
    and module_id in ({})
    """.format(product_ids)
    df_old_tagging=SQL.fetch_data_with_schema(query_old_tagging,"slave")
    
    query_cat="""
    select product_id,category_id as entity_value_id,lower(category_name) as entity_value,-2 as entity_group_id,'category' as entity_group
    from product_master
    where product_id in ({})
    group by product_id,category_id
    """.format(product_ids)
    df_cat=SQL.fetch_data_with_schema(query_cat,"warehouse")

    query_brand="""
    select product_id,brand_id as entity_value_id,lower(brand_name) as entity_value,-3 as entity_group_id,'brand' as entity_group
    from product_master
    where product_id in ({})
    group by product_id,brand_id
    """.format(product_ids)
    df_brand=SQL.fetch_data_with_schema(query_brand,"warehouse")
    
#     query_aliases="""
#     select ep.module_id as product_id,ev.id as entity_value_id,lower(ea.alias_name) as entity_value,ev.entity_group_id as entity_group_id,lower(eg.name) as entity_group
#     from entity_products as ep
#     join  entity_values ev on ev.id=ep.entity_value_id
#     join entity_groups eg on ev.entity_group_id=eg.id
#     join entity_aliases ea on ea.module_id=ev.id and ea.module='entity_value'
#     where ep.module='product'
#     and ep.module_id in ({})
#     """.format(product_ids)
#     df_aliases=SQL.fetch_data_with_schema(query_aliases,"slave")
    
    #df_old_tagging=df_old_tagging.append([df_cat,df_brand,df_aliases],ignore_index=True)
    df_old_tagging=df_old_tagging.append([df_cat,df_brand],ignore_index=True)
    df_old_tagging['entity_value']=df_old_tagging['entity_value'].str.lower()
    #df_old_tagging['entity_value']=df_old_tagging['entity_value'].str.replace('&','and')
    df_old_tagging=df_old_tagging[~df_old_tagging.entity_value.isnull()]
    df_old_tagging=df_old_tagging.drop_duplicates()
    return df_old_tagging


# entity-entity group, category,brand details function...

# In[ ]:


def get_ev_details():
    query_ev_details="""
    select ev.id as entity_value_id,lower(ev.entity_value) as entity_value,ev.entity_group_id,lower(eg.name) as entity_group
    from entity_values ev
    join entity_groups eg on ev.entity_group_id=eg.id
    """
    df_ev_details=SQL.fetch_data_with_schema(query_ev_details,"slave")

    query_cat="""
    select category_id as entity_value_id,lower(category_name) as entity_value,-2 as entity_group_id,'category' as entity_group
    from product_master
    group by category_id
    """
    df_cat=SQL.fetch_data_with_schema(query_cat,"warehouse")

    query_brand="""
    select brand_id as entity_value_id,lower(brand_name) as entity_value,-3 as entity_group_id,'brand' as entity_group
    from product_master
    group by brand_id
    """
    df_brand=SQL.fetch_data_with_schema(query_brand,"warehouse")

#     query_aliases="""
#     SELECT ev.id as entity_value_id,lower(ea.alias_name) as entity_value,ev.entity_group_id,lower(eg.name) as entity_group
#     FROM entity_values ev
#     join entity_aliases ea on ev.id=ea.module_id
#     join entity_groups eg on ev.entity_group_id=eg.id
#     where ea.module='entity_value'
#     """
#     df_aliases=SQL.fetch_data_with_schema(query_aliases,'slave')
    
    df_ev_details=df_ev_details.append([df_cat,df_brand],ignore_index=True)
    df_ev_details['entity_value']=df_ev_details['entity_value'].str.lower()
    #df_ev_details['entity_value']=df_ev_details['entity_value'].str.replace('&','and')
    df_ev_details=df_ev_details[~df_ev_details.entity_value.isnull()]
    return df_ev_details
#getting eve details
df_ev=get_ev_details()
df_entity_values=df_ev[['entity_value','entity_group']].drop_duplicates()
df_entity_values['str_len']=df_entity_values.entity_value.str.len()
df_entity_values=df_entity_values.sort_values('str_len',ascending=False)


# In[ ]:


df_ev_details=get_ev_details()


# getting aliases and removing aliases which are exactly same as other entities

# In[ ]:


query_aliases="""
select lower(ea.alias_name) as alias_name,lower(ev.entity_value) as entity_value
from entity_aliases as ea
join entity_values as ev on ea.module_id=ev.id
where ea.module='entity_value'
and ea.alias_name!=ev.entity_value
group by alias_name,entity_value
UNION
select lower(ea.alias_name) as alias_name,lower(ev.name) as entity_value
from entity_aliases as ea
join product_brand as ev on ea.module_id=ev.id
where ea.module='brand'
and ea.alias_name!=ev.name
group by alias_name,entity_value
UNION
select lower(ea.alias_name) as alias_name,lower(ev.name) as entity_value
from entity_aliases as ea
join product_category as ev on ea.module_id=ev.id
where ea.module='category'
and ea.alias_name!=ev.name
group by alias_name,entity_value
"""
df_aliases=SQL.fetch_data_with_schema(query_aliases,"slave")

query_root_cats="""
select lower(category_root_slug) as category_root_slug
from product_master
where category_root_slug!='' and category_root_slug!='0'
group by category_root_slug
"""
df_root_cats=SQL.fetch_data_with_schema(query_root_cats,"warehouse")
alias_entity_collision_list=list(df_ev_details['entity_value'].unique())+list(df_root_cats['category_root_slug'].unique())
df_aliases=df_aliases.query("alias_name not in @alias_entity_collision_list")


# adding aliases to flash text keyword processor to replace aliases with original value..

# In[ ]:


from flashtext.keyword import KeywordProcessor
keyword_processor = KeywordProcessor()
gc_var=df_aliases.apply(lambda x:keyword_processor.add_keyword(x['alias_name'],x['entity_value']),axis=1)


# getting clicks and buy info of search queries....

# In[ ]:


#search product clicks......
query_search_term_product_clicks="""
select lower(lc.page_type_value) as keyword,lc.targetentityid as product_id,count(*) as num_of_clicks,count(distinct lc.identifier ) as users
from `datos_pocos.listing_click` lc
where event_timestamp between '{}' and '{}'
and page_type='listing_search'
and identifier not in ({})
group by lc.page_type_value,lc.targetentityid
having num_of_clicks>={}
order by lc.page_type_value,num_of_clicks desc
""".format(clicks_start_date,end_date,retailer_track_ids,num_of_clicks_threshold)
df_search_term_product_clicks=SQL.fetch_data_with_schema(query_search_term_product_clicks,"bigquery")
df_search_term_product_clicks=df_search_term_product_clicks.drop_duplicates()
df_search_term_product_clicks=df_search_term_product_clicks[~df_search_term_product_clicks.keyword.isnull()]

#search term product buy details.......
query_search_term_buy="""
select keyword,targetentityid as product_id,count(distinct targetentityid) as num_of_products,sum(quantity) as quantity,sum(num_of_orders) as num_of_orders,sum(num_of_users) as num_of_purchased_users,sum(our_price) as nmv
    from(
      select lower(page_type_value) as keyword,targetentityid,count(distinct targetentityid) as num_of_distinct_products,sum(quantity) as quantity,count(distinct order_id) as num_of_orders,our_price,count(distinct   identifier) as num_of_users
      from `datos_pocos.recommendation_performance` 
      where buy_eventtime between unix_seconds(TIMESTAMP('{}')) and unix_seconds(TIMESTAMP('{}'))
      and page_type in('listing_search')
      and identifier not in ('{}')
      group by keyword,targetentityid,our_price,order_id
      
    ) as dt
    group by dt.keyword,targetentityid
    order by num_of_orders desc
""".format(start_date,end_date,start_date,end_date,retailer_track_ids)
df_search_term_buy=SQL.fetch_data_with_schema(query_search_term_buy,"bigquery")
df_search_term_buy=df_search_term_buy[~df_search_term_buy.keyword.isnull()]


# getting parenti ingradients info...

# In[ ]:


query_parent_child_entities="""
select * from parent_entity_value_childs
"""
df_parent_child_entities=SQL.fetch_data_with_schema(query_parent_child_entities,"sandbox_master")
df_parent_child_entities=pd.merge(df_parent_child_entities,df_ev[['entity_value_id','entity_value']].rename(columns={'entity_value':'parent_entity'}),on=['entity_value_id'])
df_parent_child_entities=pd.merge(df_parent_child_entities,df_ev[['entity_value_id','entity_value']].rename(columns={'entity_value':'child_entity','entity_value_id':'child_entity_value_id'}),on=['child_entity_value_id'])
df_parent_child_entities=df_parent_child_entities[['child_entity','parent_entity']].drop_duplicates()
df_parent_child_entities['entity_group']='active ingredients'


# joining clicks and buy info

# In[ ]:


#joining clicks and buy info
df_st_clicks_buy=pd.merge(df_search_term_product_clicks,df_search_term_buy,on=['keyword','product_id'],how='outer')
df_st_clicks_buy=df_st_clicks_buy.drop_duplicates()
for each_column in ['num_of_clicks','num_of_orders']:
    df_st_clicks_buy[each_column]=df_st_clicks_buy[each_column].fillna(0).astype(int)


# generating modified query to original query

# In[ ]:


#generating modified query to original query
df_st_clicks_buy=df_st_clicks_buy.rename(columns={'keyword':'original_search_query'})
df_st_clicks_buy['modified_search_query']=df_st_clicks_buy['original_search_query'].apply(lambda x:keyword_processor.replace_keywords(x))
df_st_clicks_buy['modified_search_query']=df_st_clicks_buy['modified_search_query'].apply(lambda x:re.sub(' +',' ',str(x)))
df_st_clicks_buy['modified_search_query']=df_st_clicks_buy['modified_search_query'].str.lower()
df_st_clicks_buy['modified_search_query']=df_st_clicks_buy['modified_search_query'].str.strip()


# In[ ]:


#original and modified queries
df_original_mod_query=df_st_clicks_buy[['original_search_query','modified_search_query']].drop_duplicates()


# In[ ]:


#search term level orders and clicks
df_st_orders_clicks=df_st_clicks_buy.groupby('modified_search_query').agg({'num_of_orders':'sum','num_of_clicks':'sum'}).reset_index().drop_duplicates().rename(columns={'num_of_orders':'st_num_of_orders','num_of_clicks':'st_num_of_clicks'})


# In[ ]:


#getting entity info of products
df_product_ev_details=get_old_tagging(list(df_st_clicks_buy.product_id.unique()))
df_product_ev_details=df_product_ev_details.drop_duplicates()


# In[ ]:


df_st_clicks_buy_ev=pd.merge(df_st_clicks_buy,df_product_ev_details,on=['product_id'],how='left')
df_st_clicks_buy_ev=df_st_clicks_buy_ev.rename(columns={'keyword':'original_search_query'})


# In[ ]:


#aggregating clicks and num of orders keyowrd entity group and entity value level
df_keyword_topic=df_st_clicks_buy_ev.groupby(['modified_search_query','entity_group','entity_value']).agg({'num_of_clicks':'sum','num_of_orders':'sum'}).reset_index().sort_values('num_of_orders',ascending=False)
#applying threshold values for num of orders
df_keyword_topic=df_keyword_topic.query("num_of_orders>1".format(num_of_orders_threshold))


# applying token set ratio

# In[ ]:


#token set ratio
df_keyword_topic['token_set_ratio']=df_keyword_topic.apply(lambda x:fuzz.token_set_ratio(x['modified_search_query'],x['entity_value']),axis=1)


# In[ ]:


#replacing child entities with parent entities
df_keyword_topic=pd.merge(df_keyword_topic,df_parent_child_entities.rename(columns={'child_entity':'entity_value'}),on=['entity_value','entity_group'],how='left')
df_keyword_topic['entity_value']=np.where(df_keyword_topic['parent_entity'].isnull(),df_keyword_topic['entity_value'],df_keyword_topic['parent_entity'])


# In[ ]:


df_keyword_topic['token_set_ratio']=df_keyword_topic.groupby(['modified_search_query','entity_group','entity_value'])['token_set_ratio'].transform('max')


# In[ ]:


#reaggregating clicks and num of orders keyowrd entity group and entity value level
df_keyword_topic=df_keyword_topic.groupby(['modified_search_query','entity_group','entity_value','token_set_ratio']).agg({'num_of_clicks':'sum','num_of_orders':'sum'}).reset_index().sort_values('num_of_orders',ascending=False)


# In[ ]:


#getting original query of modifed query
df_keyword_topic=pd.merge(df_keyword_topic,df_original_mod_query,on=['modified_search_query'],how='left')
df_keyword_topic=df_keyword_topic[['original_search_query','entity_value','entity_group','num_of_clicks','num_of_orders','token_set_ratio','modified_search_query']].drop_duplicates()
df_keyword_topic=df_keyword_topic.query("num_of_orders>={}".format(num_of_orders_threshold)).sort_values(['modified_search_query','num_of_orders'],ascending=[True,False])

#order and clicks contribution
df_keyword_topic=pd.merge(df_keyword_topic,df_st_orders_clicks,on=['modified_search_query'],how='left')
df_keyword_topic=df_keyword_topic.drop_duplicates()
#applying threshold values for num of orders
df_keyword_topic=df_keyword_topic.query("st_num_of_orders>={}".format(num_of_orders_threshold))

df_keyword_topic['num_of_orders']=np.where((df_keyword_topic['num_of_orders']>df_keyword_topic['st_num_of_orders']),df_keyword_topic['st_num_of_orders'],df_keyword_topic['num_of_orders'])
df_keyword_topic['orders_contrib']=(df_keyword_topic['num_of_orders']/df_keyword_topic['st_num_of_orders'])*100
df_keyword_topic['num_of_clicks']=np.where((df_keyword_topic['num_of_clicks']>df_keyword_topic['st_num_of_clicks']),df_keyword_topic['st_num_of_clicks'],df_keyword_topic['num_of_clicks'])
df_keyword_topic['clicks_contrib']=(df_keyword_topic['num_of_clicks']/df_keyword_topic['st_num_of_clicks'])*100

df_keyword_topic=df_keyword_topic.sort_values(['modified_search_query','orders_contrib','clicks_contrib'],ascending=[True,False,False])
df_keyword_topic=df_keyword_topic[~df_keyword_topic.original_search_query.isnull()]

for each_column in ['num_of_clicks','num_of_orders','st_num_of_clicks']:
    df_keyword_topic[each_column]=df_keyword_topic[each_column].fillna(0).astype(int)


# getting search term wise product ranking 

# In[ ]:


query_spr="""
select lower(spr.keyword) as original_search_query,lower(pm.category_name) as entity_value,spr.final_score
from search_term_wise_product_ranking as spr
join product_master pm on pm.product_id=spr.product_id
where final_score>={}
and thirty_num_of_orders>={}
order by original_search_query,final_score desc
""".format(final_score_threshold_in_search_term_wise_product_ranking,thirty_num_of_orders_threshold)
df_spr=SQL.fetch_data_with_schema(query_spr,"warehouse")

df_spr=df_spr.sort_values(['original_search_query','final_score'],ascending=[False,False]).groupby('original_search_query').head(10)
del df_spr['final_score']
df_spr['is_send_to_client']=1
df_spr=pd.merge(df_keyword_topic.query("entity_group=='category'"),df_spr,on=['original_search_query','entity_value'])
df_spr=df_spr.drop_duplicates()


# applying filters to send to client

# In[ ]:


#applying filters to send to client
df_keyword_topic['is_cat_exact_match']=np.where( (df_keyword_topic['entity_group']=='category') & (df_keyword_topic['entity_value']==df_keyword_topic['modified_search_query']) ,1,0)
df_keyword_topic['is_brand_exact_match']=np.where( (df_keyword_topic['entity_group']=='brand') & (df_keyword_topic['entity_value']==df_keyword_topic['modified_search_query']) ,1,0)
df_keyword_topic['is_entity_exact_match']=np.where( (~df_keyword_topic['entity_group'].isin(['brand','category'])) & (df_keyword_topic['entity_value']==df_keyword_topic['modified_search_query']) ,1,0)


# In[ ]:


for each_columm in ['is_cat_exact_match','is_brand_exact_match','is_entity_exact_match']:
    df_keyword_topic[each_columm]=df_keyword_topic.groupby('modified_search_query')[each_columm].transform('max')


# In[ ]:


df_temp_cat=df_keyword_topic.query("entity_group=='category'").copy()
df_temp_brand=df_keyword_topic.query("entity_group=='brand'").copy()
df_temp_entity=df_keyword_topic.query("entity_group not in ('brand','category')").copy()

#cats to client
df_temp_cat['is_send_to_client']=np.where( (df_temp_cat['token_set_ratio']>=cat_token_set_ratio) & (df_temp_cat['orders_contrib']>=cat_higher_order_contribution_threshold) ,1,0)
df_temp_cat['max_of_is_send_to_client']=df_temp_cat.groupby(['original_search_query'])['is_send_to_client'].transform('max')
df_temp_cat_lt_thre=df_temp_cat.query("max_of_is_send_to_client==0 & is_brand_exact_match==0").copy()
df_temp_cat_lt_thre['is_send_to_client']=np.where( (df_temp_cat_lt_thre['orders_contrib']>=cat_lower_order_contribution_threshold) ,1,0)
df_temp_cat=df_temp_cat.append(df_temp_cat_lt_thre,ignore_index=True).drop_duplicates().copy()
del df_temp_cat['max_of_is_send_to_client']

#adding search term wise product ranking top 10 sellers cats
search_queries_list_for_cats_list=df_temp_cat.query("is_send_to_client==1")['original_search_query'].unique()
df_temp_cat=df_temp_cat.append(df_spr.query("original_search_query in @search_queries_list_for_cats_list"),ignore_index=True,sort=False)
df_temp_cat=df_temp_cat.drop_duplicates(subset=['original_search_query','entity_value','entity_group','is_send_to_client'])

#brand to client
df_temp_brand['is_send_to_client']=np.where( (df_temp_brand['token_set_ratio']>=brand_token_set_ratio) & (df_temp_brand['orders_contrib']>=brand_order_contribution_threshold)  ,1,0)

#entities to client
df_temp_entity['is_send_to_client']=np.where( (df_temp_entity['token_set_ratio']>=entity_token_set_ratio) & (df_temp_entity['orders_contrib']>=entity_order_contribution_threshold) ,1,0)

#appending cat brand entities send to clent
df_cat_brand_entity=df_temp_cat.append([df_temp_brand,df_temp_entity],ignore_index=True,sort=False).query("is_send_to_client==1")
df_cat_brand_entity['orders_contrib']=df_cat_brand_entity['orders_contrib'].round(3)
#deleting not using dfs
del df_temp_cat
del df_temp_cat_lt_thre
del df_temp_brand
del df_temp_entity


# In[ ]:


df_ev_details=get_ev_details()
df_ev_details=df_ev_details.drop_duplicates(subset=['entity_value','entity_group'])

df_cbe=pd.merge(df_cat_brand_entity,df_ev_details,on=['entity_value','entity_group'],how='left')
df_cbe=df_cbe.drop_duplicates(subset=['original_search_query','entity_value','entity_group','entity_value_id'])


# In[ ]:


#replacing all entities groups as one group 'entity'
df_cbe['entity_group']=np.where(df_cbe['entity_group'].isin(['brand','category']),df_cbe['entity_group'],'entity')
#creating json object for each row
df_cbe['final_object']=df_cbe.apply(lambda x:{"id":x['entity_value_id'],"name":x['entity_value'],"score":x['orders_contrib']},axis=1)
#creating list of same group objects
df_cbe_json=df_cbe.groupby(['original_search_query','entity_group'])['final_object'].apply(list).reset_index(name='final_object')
#getting enities list group wise
df_cbe_ids_list=df_cbe.groupby(['original_search_query','entity_group'])['entity_value_id'].apply(list).reset_index(name='ids_list')
#joining entites list and cbe json
df_cbe_json=pd.merge(df_cbe_json,df_cbe_ids_list,on=['original_search_query','entity_group'],how='left')
#creating nested json for socres and ids list
df_cbe_json['final_object']=df_cbe_json.apply(lambda x:{"scores":x['final_object'],"ids_list":x['ids_list']},axis=1)
#creating final json list where key is entity group
df_cbe_json=df_cbe_json.groupby("original_search_query").apply(lambda x: {i:j for i, j in zip(x["entity_group"], x["final_object"])}).reset_index(name='final_object')
df_cbe_json['final_object']=df_cbe_json['final_object'].astype(str)


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# ----
#                                                                          rough work
# -----

# In[ ]:


# df_keyword_topic[df_keyword_topic['original_search_query']==df_keyword_topic['entity_value']].query("entity_group=='brand'")


# In[ ]:


# alias_entity_collision_list=[]
# for each_alias in df_aliases['alias_name'].unique():
#     for each_entity_value in df_ev_details['entity_value'].unique():
#         if each_alias==each_entity_value:
#             alias_entity_collision_list=alias_entity_collision_list+[each_alias]
#             break

# df_aliases=df_aliases.query("alias_name not in @alias_entity_collision_list")
# df_aliases=df_aliases.drop_duplicates()


# In[ ]:


# test_keyword='gv'

# df_temp=df_keyword_topic.query("original_search_query=='{}'".format(test_keyword)).sort_values('orders_contrib',ascending=False)

# df_temp['is_cat_exact_match']=np.where( (df_temp['entity_group']=='category') & (df_temp['entity_value']==df_temp['modified_search_query']) ,1,0)
# df_temp['is_brand_exact_match']=np.where( (df_temp['entity_group']=='brand') & (df_temp['entity_value']==df_temp['modified_search_query']) ,1,0)
# df_temp['is_entity_exact_match']=np.where( (~df_temp['entity_group'].isin(['brand','category'])) & (df_temp['entity_value']==df_temp['modified_search_query']) ,1,0)
# for each_columm in ['is_cat_exact_match','is_brand_exact_match','is_entity_exact_match']:
#     df_temp[each_columm]=df_temp.groupby(each_column)[each_columm].transform('sum')                                          

# df_temp_cat=df_temp.query("entity_group=='category'").copy()
# df_temp_brand=df_temp.query("entity_group=='brand'").copy()
# df_temp_entity=df_temp.query("entity_group not in ('brand','category')").copy()

# #cats to client
# df_temp_cat['is_send_to_clent']=np.where( (df_temp_cat['token_set_ratio']>=60) & (df_temp_cat['orders_contrib']>=70) ,1,0)
# df_temp_cat['max_of_is_send_to_clent']=df_temp_cat.groupby(['original_search_query'])['is_send_to_clent'].transform('max')
# df_temp_cat_lt_thre=df_temp_cat.query("max_of_is_send_to_clent==0 & is_brand_exact_match==0").copy()
# df_temp_cat_lt_thre['is_send_to_clent']=np.where( (df_temp_cat_lt_thre['orders_contrib']>=5) ,1,0)
# df_temp_cat=df_temp_cat.append(df_temp_cat_lt_thre,ignore_index=True).drop_duplicates().copy()
# del df_temp_cat['max_of_is_send_to_clent']


# #brand to client
# df_temp_brand['is_send_to_clent']=np.where( (df_temp_brand['token_set_ratio']>=50) & (df_temp_brand['orders_contrib']>=70)  ,1,0)

# #entities to client
# df_temp_entity['is_send_to_clent']=np.where( (df_temp_entity['token_set_ratio']>=60) & (df_temp_entity['orders_contrib']>=70) ,1,0)

# #appending cat brand entities send to clent
# df_cat_brand_entity=df_temp_cat.append([df_temp_brand,df_temp_entity],ignore_index=True).query("is_send_to_clent==1")
# #deleting not using dfs
# #del df_tem
# # del df_temp_cat
# # del df_temp_cat_lt_thre
# # del df_temp_brand
# # del df_temp_entity

# df_cat_brand_entity


# In[ ]:


# def get_ev_details():
#     query_ev_details="""
#     select ev.id as entity_value_id,lower(ev.entity_value) as entity_value,ev.entity_group_id,lower(eg.name) as entity_group
#     from entity_values ev
#     join entity_groups eg on ev.entity_group_id=eg.id
#     """
#     df_ev_details=SQL.fetch_data_with_schema(query_ev_details,"slave")

#     query_cat="""
#     select category_id as entity_value_id,lower(category_name) as entity_value,-2 as entity_group_id,'category' as entity_group
#     from product_master
#     group by category_id
#     """
#     df_cat=SQL.fetch_data_with_schema(query_cat,"warehouse")

#     query_brand="""
#     select brand_id as entity_value_id,lower(brand_name) as entity_value,-3 as entity_group_id,'brand' as entity_group
#     from product_master
#     group by brand_id
#     """
#     df_brand=SQL.fetch_data_with_schema(query_brand,"warehouse")

#     query_aliases="""
#     SELECT ev.id as entity_value_id,lower(ea.alias_name) as entity_value,ev.entity_group_id,lower(eg.name) as entity_group
#     FROM entity_values ev
#     join entity_aliases ea on ev.id=ea.module_id
#     join entity_groups eg on ev.entity_group_id=eg.id
#     where ea.module='entity_value'
#     """
#     df_aliases=SQL.fetch_data_with_schema(query_aliases,'slave')
    
#     df_ev_details=df_ev_details.append([df_cat,df_brand,df_aliases],ignore_index=True)
#     df_ev_details['entity_value']=df_ev_details['entity_value'].str.lower()
#     df_ev_details['entity_value']=df_ev_details['entity_value'].str.replace('&','and')
#     df_ev_details=df_ev_details[~df_ev_details.entity_value.isnull()]
#     return df_ev_details
# #getting eve details
# df_ev=get_ev_details()
# df_entity_values=df_ev[['entity_value']].drop_duplicates()
# df_entity_values['str_len']=df_entity_values.entity_value.str.len()
# df_entity_values=df_entity_values.sort_values('str_len',ascending=False)


# In[ ]:


# search_queries_list=list(df_keyword_topic.original_search_query.unique())

# cleaned_phrases_list=[]
# original_query_list=[]
# for each_row in search_queries_list:
#     tok=generate_candidate_phrases(each_row,stopwords)
#     cleaned_phrases_list=cleaned_phrases_list+tok
#     original_query_list=original_query_list+([each_row]*len(tok))
# df_cleaned_phrases=pd.DataFrame({'original_query':original_query_list,'sentences':cleaned_phrases_list})
# df_cleaned_phrases

# #cleaning untagged phrases after replacing entities....
# untagged_phrases_list=list(df_cleaned_phrases.sentences.unique())
# df_ut_phrases=parse_sentences(untagged_phrases_list)

# df_sentence_phrase_token=pd.merge(df_cleaned_phrases,df_ut_phrases,on=['sentences'],how='left')


# #for each_keyword in df_keyword_topic.keyword.unique():
# original_query_list=[]
# entities_list=[]
# for index,each_row in df_sentence_phrase_token.iterrows():    
#     for each_entity_value in df_keyword_topic.query("original_search_query=={}".format("'"+each_row['original_query']+"'"))['entity_value'].unique():
#         str_a=each_row['phrase']
#         str_b=each_entity_value
#         s = fuzz.token_set_ratio(str_a,str_b)
#         if s>=80:
#             #print(each_row['original_query'],'-----',each_row['sentences'],'----',str_a,"---",str_b,"----")
#             original_query_list=original_query_list+[each_row['original_query']]
#             entities_list=entities_list+[str_b]
#             #print(original_query_list,entities_list)
# df_text_entities=pd.DataFrame({'original_search_query':original_query_list,'entity_value':entities_list}).reset_index().drop_duplicates()
# del df_text_entities['index']
# df_text_entities=df_text_entities.drop_duplicates()
# df_entites_partial_match_in_text=pd.merge(df_text_entities,df_keyword_topic,on=['entity_value','original_search_query'])


# In[ ]:


# def import_data_in_chunks(df_data,max_rows,table_name):
#     total_count=df_data.shape[0]
#     total_splits=int(total_count/max_rows)+1
#     df_splits = np.array_split(df_data, total_splits)
#     result=False
#     for df_chunk in df_splits :
#         print("importing chunk:",df_chunk.shape)
#         SQL.insert_dataframe_to_db(df_chunk,"warehouse",table_name)
#         result=True
#     return result

# print("pushing data to test_topic_identification....")
# import_data_in_chunks(df_cat_brand_entity,100000,"topic_identification_of_query")


# In[ ]:


#SQL.update_data("drop table topic_identification_of_query","","warehouse")


# In[ ]:


# def import_data_in_chunks(df_data,max_rows,table_name):
#     total_count=df_data.shape[0]
#     total_splits=int(total_count/max_rows)+1
#     df_splits = np.array_split(df_data, total_splits)
#     result=False
#     for df_chunk in df_splits :
#         print("importing chunk:",df_chunk.shape)
#         SQL.insert_dataframe_to_db(df_chunk,"sandbox_master",table_name)
#         result=True
#     return result

# print("pushing data to topic_identification_of_query....")
# import_data_in_chunks(df_cbe_json,100000,"topic_identification_of_query")


# In[ ]:


# query_aliases="""
# select lower(ea.alias_name) as alias_name,lower(ev.entity_value) as entity_value
# from entity_aliases as ea
# join entity_values as ev on ea.module_id=ev.id
# where ea.module='entity_value'
# and ea.alias_name!=ev.entity_value
# group by alias_name,entity_value
# UNION
# select lower(ea.alias_name) as alias_name,lower(ev.name) as entity_value
# from entity_aliases as ea
# join product_brand as ev on ea.module_id=ev.id
# where ea.module='brand'
# and ea.alias_name!=ev.name
# group by alias_name,entity_value
# UNION
# select lower(ea.alias_name) as alias_name,lower(ev.name) as entity_value
# from entity_aliases as ea
# join product_category as ev on ea.module_id=ev.id
# where ea.module='category'
# and ea.alias_name!=ev.name
# group by alias_name,entity_value
# """
# df_aliases=SQL.fetch_data_with_schema(query_aliases,"slave")


# In[ ]:


# from flashtext.keyword import KeywordProcessor
# keyword_processor = KeywordProcessor()
# gc_var=df_aliases.apply(lambda x:keyword_processor.add_keyword(x['alias_name'],x['entity_value']),axis=1)
# df_keyword_topic['original_search_query'].apply(lambda x:keyword_processor.replace_keywords(x))


# In[ ]:


# df_keyword_topic['original_search_query'].apply(lambda x:keyword_processor.replace_keywords(x))


# In[ ]:





# In[ ]:


df_st_clicks_buy[df_st_clicks_buy.original_search_query.str.contains('shakti')].groupby('original_search_query').head(1)


# In[ ]:


test_keyword='lakme gel'


# In[ ]:


df_cat_brand_entity.query("original_search_query=='{}'".format(test_keyword))


# In[ ]:


df_cbe_json.query("original_search_query=='{}'".format(test_keyword)).reset_index()['final_object'][0]


# In[ ]:




