
# coding: utf-8

# In[ ]:




# In[ ]:

import mysql.connector
import psycopg2
from mysql.connector import Error
import os
import re
from sqlalchemy import create_engine
import pandas as pd
conn=False
from google.cloud import bigquery
from google.oauth2 import service_account

import resource
resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
def get_mysql_instance (database) :
    conn=False
    try:
        if database == "warehouse" :
            conn = mysql.connector.connect(host=os.environ['WH_SERVER_ADDRESS'],database=os.environ['WH_DB_NAME'],user=os.environ['WH_USER_NAME'],password=os.environ['WH_PASSWORD'])
        elif database == "master" :
            conn = mysql.connector.connect(host=os.environ['MASTER_SERVER_ADDRESS'],database=os.environ['MASTER_DB_NAME'],user=os.environ['MASTER_USER_NAME'],password=os.environ['MASTER_PASSWORD'])
        elif database == "slave" :
            conn = mysql.connector.connect(host=os.environ['SERVER_ADDRESS'],database=os.environ['DB_NAME'],user=os.environ['USER_NAME'],password=os.environ['PASSWORD'])
        elif database == "delayed_slave" :
            conn = mysql.connector.connect(host=os.environ['DELAYED_SLAVE_ADDRESS'],database=os.environ['DELAYED_SLAVE_DB_NAME'],user=os.environ['DELAYED_SLAVE_USER_NAME'],password=os.environ['DELAYED_SLAVE_PASSWORD'])
        elif database == "sandbox_warehouse" :
            conn = mysql.connector.connect(host=os.environ['SWH_SERVER_ADDRESS'],database=os.environ['SWH_DB_NAME'],user=os.environ['SWH_USER_NAME'],password=os.environ['SWH_PASSWORD'])
        elif database == "purplle_reporting" :
            conn = mysql.connector.connect(host=os.environ['PURPLLE_REPORTING_ADDRESS'],database=os.environ['PURPLLE_REPORTING_DB_NAME'],user=os.environ['PURPLLE_REPORTING_USER_NAME'],password=os.environ['PURPLLE_REPORTING_PASSWORD'])   
        elif database == "sandbox_master" :
            conn = mysql.connector.connect(host=os.getenv('SANDBOX_SERVER_ADDRESS'),database=os.getenv('SANDBOX_DB_NAME'),user=os.getenv('SANDBOX_USER_NAME'),password=os.getenv('SANDBOX_PASSWORD'))
        elif database == "redshift" :
            try:
                conn = psycopg2.connect(host=os.environ['PGHOST'],database=os.environ['PGDATABASE'],user=os.environ['PGUSER'],password=os.environ['PGPASSWORD'],port=os.environ['PGPORT'])
            except:
                print("I am unable to connect to the database")
        else:
            conn=False
        return conn
    except Error as e:
        print(e)
        return False


def get_db_instance (database) :
    try:
        if database == "warehouse" :
            conn = create_engine('mysql+mysqlconnector://%s:%s@%s/%s' % (os.environ['WH_USER_NAME'],os.environ['WH_PASSWORD'],os.environ['WH_SERVER_ADDRESS'],os.environ['WH_DB_NAME']), echo=False)
        elif database == "slave" :
            conn = create_engine('mysql+mysqlconnector://%s:%s@%s/%s' % (os.environ['USER_NAME'],os.environ['PASSWORD'],os.environ['SERVER_ADDRESS'],os.environ['DB_NAME']), echo=False)
        elif database == "master" :
            conn = create_engine('mysql+mysqlconnector://%s:%s@%s/%s' % (os.environ['MASTER_USER_NAME'],os.environ['MASTER_PASSWORD'],os.environ['MASTER_SERVER_ADDRESS'],os.environ['MASTER_DB_NAME']), echo=False)
        elif database == "delayed_slave" :
            conn = create_engine('mysql+mysqlconnector://%s:%s@%s/%s' % (os.environ['DELAYED_SLAVE_USER_NAME'],os.environ['DELAYED_SLAVE_PASSWORD'],os.environ['DELAYED_SLAVE_ADDRESS'],os.environ['DELAYED_SLAVE_DB_NAME']), echo=False)
        elif database == "sandbox_warehouse" :
            conn = create_engine('mysql+mysqlconnector://%s:%s@%s/%s' % (os.environ['SWH_USER_NAME'],os.environ['SWH_PASSWORD'],os.environ['SWH_SERVER_ADDRESS'],os.environ['SWH_DB_NAME']), echo=False)
        elif database == "purplle_reporting" :
            conn = create_engine('mysql+mysqlconnector://%s:%s@%s/%s' % (os.environ['PURPLLE_REPORTING_USER_NAME'],os.environ['PURPLLE_REPORTING_PASSWORD'],os.environ['PURPLLE_REPORTING_ADDRESS'],os.environ['PURPLLE_REPORTING_DB_NAME']), echo=False)    
        elif database == "sandbox_master" :
            conn = create_engine('mysql+mysqlconnector://%s:%s@%s/%s' % (os.environ['SANDBOX_USER_NAME'],os.environ['SANDBOX_PASSWORD'],os.environ['SANDBOX_SERVER_ADDRESS'],os.environ['SANDBOX_DB_NAME']), echo=False)
        elif database == "redshift" :
            try:
                conn = psycopg2.connect(host=os.environ['PGHOST'],database=os.environ['PGDATABASE'],user=os.environ['PGUSER'],password=os.environ['PGPASSWORD'],port=os.environ['PGPORT'])
            except:
                print("I am unable to connect to the database")
        else:
            conn=False
        return conn
    except Error as e:
        print(e)
        return False


def set_limits (sql,db):
        try:
                conn=get_mysql_instance(db)
                cursor = conn.cursor()
                cursor.execute(sql)
                return True
        except Error as e:
                print(e)
                return False

def select_data(sql,data,db) :
    try:
        conn=get_mysql_instance(db)
        cursor = conn.cursor()
        cursor.execute("SET SESSION group_concat_max_len = 10000000;")
        cursor.execute(sql,data)
        rows = cursor.fetchall()
        return rows
    except Error as e:
        print(e)
        return []

def fetch_data(sql,db) :
        try:
            if db == "bigquery" :
                credentials = service_account.Credentials.from_service_account_file('/purplle_data_science/credentials/DataPipelineProduction-8521e6e33aff.json')
                client = bigquery.Client(credentials= credentials,project='datapipelineproduction')
                dir(client)
                #dataset_ref = client.dataset('event', project='datapipelineproduction')
                sql = sql.replace('event.', os.environ['dataset']+'.')
                df = client.query(sql).to_dataframe()
                return df
            else :
                    conn=get_mysql_instance(db)
                    cursor = conn.cursor()
                    cursor.execute(sql)
                    rows = cursor.fetchall()
                    return rows
        except Error as e:
                print(e)
                return []

def execute_query(sql,db) :
        try:
            if db == "bigquery" :
                credentials = service_account.Credentials.from_service_account_file('/purplle_data_science/credentials/DataPipelineProduction-8521e6e33aff.json')
                client = bigquery.Client(credentials= credentials,project='datapipelineproduction')
                dir(client)
                #dataset_ref = client.dataset('event', project='datapipelineproduction')
                sql = sql.replace('event.', os.environ['dataset']+'.')
                df = client.query(sql).to_dataframe()
                return df
            else :
                    conn=get_mysql_instance(db)
                    cursor = conn.cursor()
                    cursor.execute(sql)
#                    rows = cursor.fetchall()
                    return cursor
        except Error as e:
                print(e)
                return []




def fetch_data_with_schema(sql,db) :
        try:
            if db == "bigquery" :
                credentials = service_account.Credentials.from_service_account_file('/purplle_data_science/credentials/DataPipelineProduction-8521e6e33aff.json')
                client = bigquery.Client(credentials= credentials,project='datapipelineproduction')
                dir(client)
                #dataset_ref = client.dataset('event', project='datapipelineproduction')
                sql = sql.replace('event.', os.environ['dataset']+'.')
                df = client.query(sql).to_dataframe()
                return df
            else :
                    conn=get_mysql_instance(db)
                    cursor = conn.cursor()
                    cursor.execute(sql)
                    rows = cursor.fetchall()
                    df=pd.DataFrame(rows,columns=cursor.column_names)
                    return df
        except Error as e:
                print(e)
                return []



def fetch_dataframe(sql,db):
        try:
                con=get_mysql_instance(db)
                data=pd.read_sql(sql,con=con)
                return data
        except Error as e:
                print(e)
                return []


def insert_dataframe_to_db(dataframe,db,tablename,if_exists='append'):
    try:
        con=get_db_instance(db)
        dataframe.to_sql(name=tablename, con=con, if_exists=if_exists, index=False)
        return
    except Error as e:
        print(e)
        return

def update_data(sql,data,db) :
    try :
        conn=get_mysql_instance(db)
        cursor=conn.cursor()
        cursor.execute(sql,data)
        conn.commit()
        return cursor.rowcount
    except Error as e:
        print(e)
        return False	

def insert_data(sql,db):
    try :
        conn=get_mysql_instance(db)
        cursor=conn.cursor()
        cursor.execute(sql)
        conn.commit()
        return cursor.rowcount
    except Error as e:
        print(e)
        return False

def truncate_table(table_name,db):
    try :
        conn=get_mysql_instance(db)
        cursor=conn.cursor()
        cursor.execute("truncate table "+table_name)
        conn.commit()
        return True
    except Error as e:
        print(e)
        return False

def get_content_from_database(source_info) :
    doc_text=""
    success_flag=False
    conn=get_mysql_instance("slave")
    cursor = conn.cursor()
    sql = "select "+source_info[3]+" from  product_product WHERE id="+str(source_info[6])
    cursor.execute(sql)
    row = cursor.fetchone()	
    if len(row) > 0 :
        if row[0] == None :
            doc_text=""
        else :
            doc_text=row[0]
            success_flag=True
    doc_text=re.sub('<[A-Za-z\/][^>]*>', '', doc_text)

    if source_info[3] == "features" : 
        doc_text=doc_text.replace('\n', '.').replace('\r', '').replace('\t','').strip()
    doc_text=doc_text.replace('\n', '').replace('\r', '').replace('\t','').strip()
    target=open(os.environ['KNOWLEDGE_ENGINE']+'data/raw','w+')
    target.write(doc_text)
    target.close
    return success_flag

if __name__ == '__main__':       # when run as a script
    #print(get_mysql_instance("slave"))
    print(fetch_data("select count(*) from `event.buy`","bigquery"))


