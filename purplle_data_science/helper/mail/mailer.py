#!/bin/python

"""
HOW TO USE -

python ~/purplle_data_science/marketing/dev/vishal/sendEmailAlert.py subject_here text_here recipents here

"""

import boto.ses
import sys


AWS_ACCESS_KEY = 'AKIAJQWPCRBERLUW2LOA'  
AWS_SECRET_KEY = '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'


class Email(object):  
    def __init__(self, to, subject):
        self.to = to
        self.subject = subject
        self._html = None
        self._text = None
        self._format = 'html'
    def html(self, html):
        self._html = html
    def text(self, text):
        self._text = text
    def send(self, from_addr=None):
        body = self._html
        if not isinstance(self.to, list):
            raise Exception("Error - provide recipents in list object")
        if not from_addr:
            from_addr = 'info@purplle.com'
        if not self._html and not self._text:
            raise Exception('You must provide a text or html body.')
        if not self._html:
            self._format = 'text'
            body = self._text
        connection = boto.ses.connect_to_region(
                'us-east-1',
                aws_access_key_id=AWS_ACCESS_KEY, 
                aws_secret_access_key=AWS_SECRET_KEY
                )
        return connection.send_email(
                from_addr,
                self.subject,
                None,
                self.to,
                format=self._format,
                text_body=self._text,
                html_body=self._html
                )


if __name__ == '__main__':       # when run as a script
    print('This a File to send Emails - `import Email`')

    arguments = sys.argv

    RECIPENTS = arguments[3:]
    SUBJECT = arguments[1]
    TEXT = arguments[2]

    print(arguments)
    email = Email(to=RECIPENTS, subject=SUBJECT)
    email.text(TEXT)
    email.send()
