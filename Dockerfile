FROM python:3.7
WORKDIR /

#data science redis cluster production IP
ENV REDIS_HOST=10.77.0.35
ENV REDIS_PORT=6379

#new db credentials
#purplle master database(read/write)
ENV MASTER_SERVER_ADDRESS=10.160.0.100
ENV MASTER_DB_NAME=purplle_purplle2
ENV MASTER_USER_NAME=data_user
ENV MASTER_PASSWORD=Z9HbkU5yDrmTRpG8UPF8

#purplle slave database(read only)
#ENV SERVER_ADDRESS=10.160.15.217
ENV SERVER_ADDRESS=10.160.15.247
ENV DB_NAME=purplle_purplle2
ENV USER_NAME=ephermal_jobs_user
ENV PASSWORD=gPZZLvLZmupGWqc5U267

#purplle delayed slave database(read only)
ENV DELAYED_SLAVE_ADDRESS=10.160.15.247
ENV DELAYED_SLAVE_DB_NAME=purplle_purplle2
ENV DELAYED_SLAVE_USER_NAME=ephermal_jobs_user
ENV DELAYED_SLAVE_PASSWORD=gPZZLvLZmupGWqc5U267

#warehouse master database(read/write)
ENV WH_SERVER_ADDRESS=10.160.15.220
ENV WH_DB_NAME=dw_ppl_prod
ENV WH_USER_NAME=ephermal_jobs_user
ENV WH_PASSWORD=3dmzzRnMd3Q4AAugqqcy
#Iloo0eexosah2Miehuob

#warehouse sandbox database(read/write)
ENV SWH_SERVER_ADDRESS=35.201.189.121
ENV SWH_DB_NAME=dw_ppl_prod
ENV SWH_USER_NAME=root
ENV SWH_PASSWORD=wxOfdpruiEKrHvCm

#sandbox master database(read/write)
ENV SB_SERVER_ADDRESS=35.201.189.121
ENV SB_DB_NAME=purplle_purplle2
ENV SB_USER_NAME=web_user
ENV SB_PASSWORD=36dXUwiaflxKKjposE90

#purplle reporting
ENV PURPLLE_REPORTING_ADDRESS=35.244.55.252
ENV PURPLLE_REPORTING_DB_NAME=purplle_purplle2
ENV PURPLLE_REPORTING_USER_NAME=reader
ENV PURPLLE_REPORTING_PASSWORD=ThoaDaab6za0Xohj6jab

#redshift

ENV PGHOST=purplle.coyzf62z30li.us-east-1.redshift.amazonaws.com
ENV PGPORT=5439
ENV PGDATABASE=warehouse
ENV PGUSER=purplle
ENV PGPASSWORD=123Purplle


#redis production server
#ENV REDIS_HOST=10.128.0.24
#ENV REDIS_HOST=104.199.164.85
#ENV REDIS_PORT=6379

#reco app host 
ENV APP_HOST=0.0.0.0
ENV FLASK_SECRET=1234567890
ENV API_TOKEN=YACsDfN6etAtLNqKTZ
ENV RECO_ENV=Production
ENV DAYS_TO_CONSIDER=60

#reco engine home
ENV ENGINE_HOME=/recoEnginePyhton
ENV ENGINE_HOME_NEW=/recoEnginePyhton_new
ENV DATA_SCIENCE_HOME=/purplle_data_science
ENV EDITOR=nano

#event headers
ENV DSH_ADD_TO_CART='setevent|eventtype|entitytype|entityid|targetentitytype|targetentityid|mode_device|eventtime|stock_status|mrp|our_price|identifier|properties|cart_id|page_type|page_type_value|feature_type|feature_value|offer_price|page_number|build_number|feature_position|item_position|event_timestamp|page_title|page_url|x_id|page_deeplink|is_logged_in|cart_count|is_elite|version|user_agent|device_brand|device_model|os_version|session_id|event_viewtype|event_viewtype_value'
ENV DSH_ADD_TO_WISHLIST='setevent|eventtype|entitytype|entityid|targetentitytype|targetentityid|mode_device|eventtime|stock_status|mrp|our_price|identifier|properties|cart_id|page_type|page_type_value|feature_type|feature_value|offer_price|page_number|build_number|feature_position|item_position|event_timestamp|page_title|page_url|page_deeplink|x_id|is_logged_in|cart_count|is_elite|version|user_agent|device_brand|device_model|os_version|session_id|event_viewtype|event_viewtype_value'
ENV DSH_APP_INSTALL='setevent|mode_device|hardware_id|source|medium|campaign|eventtime|google_advertising_id|referred|reinstall|identifier|visitorppl|unique_identifier|limit_ad_tracking|app_version|app_type|via_features|event_timestamp|x_id|build_number|user_agent|device_brand|device_model|os_version|session_id|entityid|entitytype|is_logged_in|cart_count|is_elite'
ENV DSH_APP_OPEN='setevent|mode_device|entityid|source|medium|campaign|eventtime|identifier|is_first_session|clicked_branch_link|google_advertising_id|gclid|visitorppl|properties|hardware_id|build_number|unique_identifier|event_timestamp|os_version|eventtype|session_id|entitytype|page_title|page_type|page_type_value|page_number|page_url|page_deeplink|is_logged_in|cart_count|x_id|is_elite|version|user_agent|device_brand|device_model|event_viewtype|event_viewtype_value'
ENV DSH_AUTH_ACTION='setevent|eventtype|actiontype|entityid|identifier|mode_device|eventtime|app_version|event_timestamp|page_type_value|page_number|page_url|page_deeplink|is_logged_in|reference|cart_count|is_elite|build_number|user_agent|device_brand|device_model|os_version|session_id|entitytype|page_title|page_type|event_viewtype|event_viewtype_value'
ENV DSH_BEAUTY_PROFILE='setevent|eventtype|entitytype|entityid|targetentitytype|targetentityid|mode_device|eventTime|identifier|page_type|page_type_value|entity_value|action_type|version|build_number|event_timestamp|device_model|os_version|session_id|page_title|page_number|page_url|page_deeplink|x_id|is_logged_in|cart_count|is_elite|user_agent|device_brand|event_viewtype|event_viewtype_value'
ENV DSH_BUY='setevent|eventtype|entitytype|entityid|targetentitytype|targetentityid|mode_device|eventtime|order_id|identifier|stock_status|mrp|our_price|email_id|coupon_name|coupon_amount|quantity|event_timestamp|is_logged_in|cart_count|is_elite|x_id|offer_price|version|build_number|user_agent|device_brand|device_model|os_version|session_id'
ENV DSH_CAMPAIGN_CLICK='setevent|entityid|userid|sendtype|targetsendtype|eventsource|eventmedium|eventcampaign|ispaid|eventtime|gclid|useragent|modedevice|targetentityid|eventcontent|event_timestamp|is_logged_in|cart_count|is_elite|x_id|version|build_number|identifier|device_brand|device_model|os_version|session_id|event_type|entitytype'
ENV DSH_CAMPAIGN_OPEN='setevent|entityid|userid|sendtype|targetsendtype|eventsource|eventmedium|eventcampaign|ispaid|eventtime|gclid|useragent|modedevice|event_timestamp|device_model|os_version|session_id|entitytype|eventtype|is_logged_in|cart_count|is_elite|x_id|version|build_number|identifier|device_brand'
ENV DSH_CAMPAIGN_SENT='setevent|entityid|userid|sendtype|targetsendtype|eventsource|eventmedium|eventcampaign|ispaid|eventtime|messageid|ignore_flag|segment|email_type|event_timestamp|mode_device|version|build_number|user_agent|identifier|device_brand|device_model|os_version|session_id|entitytype|is_logged_in|cart_count|is_elite|x_id'
ENV DSH_CART='setevent|eventtype|entityid|entitytype|targetentityid|targetentitytype|eventtime|cart_product|cart_product_oos|list_total|sub_total|total|discount|discount_tax|coupon_code|unique_coupon_code|shipping_cost|cod_cost|is_login|x_id|mode_device|version|build_number|user_agent|identifier|device_brand|device_model|os_version|referrer|actiontype|event_timestamp|is_elite|session_id|page_title|page_type|page_type_value|page_number|page_url|page_deeplink|event_viewtype|event_viewtype_value'
ENV DSH_DEVICE_REGISTRATION='setevent|user_id|identifier|email_id|device_token|mode_device|time_stamp|app_version|user_status|socket_id|event_timestamp|eventtype|build_number|user_agent|device_brand|device_model|os_version|session_id|is_logged_in|cart_count|is_elite'
ENV DSH_ERROR_LOG='setevent|module|sub_module|error_code|severity|mode_device|reason|input_value|eventtime|event_timestamp'
ENV DSH_FEATURE_CLICK='setevent|eventtype|entitytype|entityid|targetentitytype|targetentityid|mode_device|eventtime|identifier|page_type|page_type_value|page_title|feature_type|item_position|feature_value|build_number|x_id|event_timestamp|device_brand|device_model|os_version|session_id|version|page_number|page_url|user_agent|page_deeplink|is_logged_in|cart_count|is_elite|event_viewtype|event_viewtype_value'
ENV DSH_FEATURE_IMPRESSION='setevent|eventtype|entitytype|entityid|targetentitytype|targetentityid|mode_device|eventtime|identifier|item_position|page_type|page_type_value|feature_type|page_title|version|build_number|feature_value|x_id|event_timestamp|page_number|page_url|page_deeplink|is_logged_in|cart_count|is_elite|user_agent|device_brand|device_model|os_version|session_id|feature_position|event_viewtype|event_viewtype_value'
ENV DSH_FEATURE_LIKE='setevent|eventtype|entitytype|entityid|targetentitytype|targetentityid|mode_device|eventtime|identifier|page_type|page_type_value|page_title|feature_type|version|build_number|event_timestamp|os_version|session_id|page_number|page_url|page_deeplink|is_logged_in|cart_count|x_id|is_elite|user_agent|device_brand|device_model|event_viewtype|event_viewtype_value'
ENV DSH_FEATURE_SHARE='setevent|eventtype|entitytype|entityid|targetentitytype|targetentityid|mode_device|eventtime|identifier|page_type|page_type_value|page_title|feature_type|feature_type_value|version|build_number|x_id|event_timestamp|is_logged_in|feature_position|cart_count|is_elite|user_agent|device_brand|device_model|os_version|session_id|page_number|page_url|page_deeplink|event_viewtype|event_viewtype_value'
ENV DSH_FEATURE_VIEW='setevent|eventtype|entitytype|entityid|targetentitytype|targetentityid|mode_device|eventtime|identifier|page_type|page_type_value|page_title|feature_type|version|build_number|event_timestamp|page_number|page_url|page_deeplink|is_logged_in|x_id|cart_count|is_elite|user_agent|device_brand|device_model|os_version|session_id|event_viewtype|event_viewtype_value'
ENV DSH_FILTER_SORT='setevent|eventtype|entitytype|entityid|targetentitytype|targetentityid|mode_device|eventtime|identifier|key|values|page_type|page_type_value|x_id|feature_type|feature_value|event_timestamp|version|build_number|user_agent|device_brand|device_model|os_version|session_id|page_title|page_number|page_url|page_deeplink|is_logged_in|cart_count|is_elite|event_viewtype|event_viewtype_value'
ENV DSH_INTERACTION='setevent|action|type|subtype|value|position|x_id|mode_device|version|build_number|user_agent|identifier|device_brand|device_model|os_version|session_id|eventtime|user_id|entity_type|is_logged_in|cart_count|is_elite|page_name|page_type|page_type_value|page_number|page_url|page_deeplink|event_timestamp|event_viewtype|event_viewtype_value'
ENV DSH_LISTING_CLICK='setevent|eventtype|entitytype|entityid|targetentitytype|targetentityid|mode_device|eventtime|identifier|page_type|page_type_value|item_position|page_title|page_number|x_id|build_number|listed_by|matched_status|mrp|our_price|offer_price|stock_status|target_type|target_type_value|event_timestamp|page_url|page_deeplink|is_logged_in|cart_count|is_elite|version|user_agent|device_brand|device_model|os_version|session_id|event_viewtype|event_viewtype_value'
ENV DSH_LISTING_IMPRESSION='setevent|eventtype|entitytype|entityid|targetentitytype|targetentityid|mode_device|eventtime|identifier|item_position|x_id|widget_id|page_type|page_type_value|page_title|build_number|listed_by|matched_status|mrp|our_price|offer_price|feature_type|feature_value|stock_status|event_timestamp|device_brand|device_model|os_version|session_id|page_number|page_url|page_deeplink|is_logged_in|cart_count|is_elite|version|user_agent|event_viewtype|event_viewtype_value'
ENV DSH_LISTING_VIEW='setevent|eventtype|entitytype|entityid|targetentitytype|targetentityid|mode_device|eventtime|identifier|page_type|page_type_value|count_result|page_title|page_number|build_number|x_id|event_timestamp|is_logged_in|cart_count|is_elite|version|user_agent|device_brand|device_model|os_version|session_id|page_url|page_deeplink|event_viewtype|event_viewtype_value'
ENV DSH_NOTIFICATION='setevent|entitytype|entityid|identifier|notify_alerts|type|mode_device|eventtime|version|build_number|eventsource|eventmedium|eventcampaign|delivery_method|device_model|device_brand|priority|os_version|user_agent|event_timestamp|x_id|is_logged_in|cart_count|is_elite|session_id'
ENV DSH_NOTIFY_ME='setevent|eventtype|entitytype|entityid|targetentitytype|targetentityid|mode_device|eventtime|stock_status|mrp|our_price|identifier|page_type|page_type_value|feature_type|feature_value|mail_id|offer_price|page_number|build_number|feature_position|item_position|event_timestamp|cart_count|is_elite|version|user_agent|device_brand|device_model|os_version|session_id|page_title|x_id|page_url|page_deeplink|is_logged_in|event_viewtype|event_viewtype_value'
ENV DSH_ORDER_STATUS='setevent|order_id|order_status|eventtime|event_timestamp'
ENV DSH_PAGE_VIEW='setevent|eventtype|entitytype|entityid|targetentitytype|targetentityid|mode_device|eventtime|identifier|feature_type|feature_value|is_landing_page|build_number|x_id|event_timestamp|page_title|page_type|page_type_value|page_number|page_url|page_deeplink|referrer|referrer_page_type|referrer_page_value|is_logged_in|version|cart_count|is_elite|user_agent|device_brand|device_model|os_version|session_id|event_viewtype|event_viewtype_value'
ENV DSH_PRODUCT_CLICK='event_date|event|page_type|page_type_value|setevent_module_id|item_position|targetentityid|clicks_count|brand_id|brand_name|category_id|category_name|event_timestamp'
ENV DSH_PRODUCT_STATUS='setevent|product_id|moq|purchase_limit|instock_time|outstock_time|stockstatus|active|ourprice|updated_on|event_timestamp'
ENV DSH_SESSION_DETAILS='setevent|eventcampaign|eventmedium|eventsource|eventcontent|ispaid|clicked_branch_link|gclid|google_advertising_id|referrer|referrer_page_type|referrer_page_value|ip|is_landing_page|is_first_session|x_id|mode_device|version|build_number|user_agent|identifier|device_brand|device_model|os_version|session_id|eventtime|entityid|entitytype|page_title|page_type|page_type_value|page_number|page_url|page_deeplink|is_logged_in|cart_count|is_elite|event_timestamp|event_viewtype|event_viewtype_value'
ENV DSH_SITE_VISIT='setevent|mode_device|entityid|source|medium|campaign|eventtime|identifier|is_first_session|clicked_branch_link|google_advertising_id|gclid|visitorppl|properties|session_initiator|landing_page|event_timestamp|page_number|page_url|page_deeplink|referrer|x_id|referrer_page_type|referrer_page_value|os_version|is_logged_in|session_id|cart_count|is_elite|entitytype|version|build_number|user_agent|eventtype|device_brand|device_model|page_title|page_type|page_type_value|event_viewtype|event_viewtype_value'
ENV DSH_STORY_VIDEO_PLAY='setevent|eventtype|entitytype|entityid|targetentitytype|targetentityid|mode_device|eventtime|identifier|page_type|page_type_value|page_title|feature_type|version|build_number|event_timestamp'
ENV DSH_SUGGESTION='setevent|eventtype|entitytype|entityid|mode_device|eventtime|identifier|search_term|selected_search_type|selected_search_type_text|selected_search_id|intent|search_place|impression_keyword|x_id|event_timestamp|page_deeplink|is_logged_in|cart_count|is_elite|version|build_number|user_agent|device_brand|device_model|os_version|session_id|page_title|page_type|page_type_value|page_number|page_url|event_viewtype|event_viewtype_value'
ENV DSH_VIEW='setevent|eventtype|entitytype|entityid|targetentitytype|targetentityid|mode_device|eventtime|stock_status|mrp|our_price|identifier|offer_price|build_number|user_agent|x_id|event_timestamp|cart_count|is_elite|version|device_brand|device_model|os_version|session_id|page_title|page_type|page_type_value|page_number|page_url|page_deeplink|is_logged_in|event_viewtype|event_viewtype_value'
ENV DSH_WIDGET_CLICK='setevent|eventtype|entitytype|entityid|targetentitytype|targetentityid|mode_device|eventtime|identifier|page_type|page_type_value|item_position|page_title|x_id|widget_id|build_number|mrp|our_price|offer_price|target_type|target_type_value|event_timestamp|user_agent|device_brand|device_model|os_version|session_id|stock_status|page_number|page_url|page_deeplink|is_logged_in|cart_count|is_elite|version|event_viewtype|event_viewtype_value'
ENV DSH_WIDGET_IMPRESSION='setevent|eventtype|entitytype|entityid|targetentitytype|targetentityid|mode_device|eventtime|identifier|item_position|x_id|widget_id|page_type|page_type_value|page_title|build_number|listed_by|matched_status|mrp|our_price|offer_price|feature_type|feature_value|stock_status|event_timestamp|version|user_agent|device_brand|device_model|os_version|session_id|page_number|page_url|page_deeplink|is_logged_in|cart_count|is_elite|event_viewtype|event_viewtype_value'

ENV EVENTS='feature_impression|widget_impression|listing_impression|view|add_to_cart|add_to_wishlist|notify_me|listing_click|widget_click|feature_view|feature_like|feature_click|feature_share|app_open|campaign_click|campaign_sent|campaign_open|suggestion|device_registration|app_install|buy|notify_me|campaign_open'


#data pipeline

ENV DSH_PRODUCT_MASTER='product_id|sku|name|price|app_our_price|app_discount|app_discount_price|our_price|discount|discount_price|stock_status|stock_status_message|moq|active|product_group|brand_id|brand_name|category_id|category_name|category_slug|category_root_id|category_root_slug|p_group|group_parent_id|clubbing_type|clubbing_value|iscombo|inv_closing_stock|inv_closing_stock_cost|inv_closing_stock_mrp|inv_thirdparty_quantity|active_sales_pending_order_quantity|in_stock_percentage_7days|in_stock_percentage_30days|inv_olderthan_45days|inv_olderthan_90days|inv_olderthan_120days|inv_olderthan_180days|active_sales_last_7_days|active_sales_last_30_days|active_sales_last_45_days|active_sales_last_60_days|active_sales_last_90_days|inv_pending_quantity_in_po|inv_qty_closed_not_inwarded|total_views_1days|total_views_7days|total_views_30days|total_oos_views_30days|total_oos_views_7days|total_oos_views_1days|drr|moving_stock_flag|inventory_aging_count_for_365|inventory_aging_count_for_180|inventory_aging_count_for_60|product_manager|active_sales_last_1_days|sales_amount_last_1_days|sales_amount_last_7_days|sales_amount_last_30_days|sales_amount_last_45_days|sales_amount_last_60_days|sales_amount_last_90_days|ean_code|hsn_code|hsn_tax|grn_quantity_30days|grn_quantity_60days|grn_quantity_90days'

ENV DSH_PRODUCT_MASTER_TEST='product_id|sku|name|price|app_our_price|app_discount|app_discount_price|our_price|discount|discount_price|stock_status|stock_status_message|moq|active|product_group|brand_id|brand_name|category_id|category_name|category_slug|category_root_id|category_root_slug|p_group|group_parent_id|clubbing_type|clubbing_value|iscombo|inv_closing_stock|inv_closing_stock_cost|inv_closing_stock_mrp|inv_thirdparty_quantity|active_sales_pending_order_quantity|in_stock_percentage_7days|in_stock_percentage_30days|inv_olderthan_45days|inv_olderthan_90days|inv_olderthan_120days|inv_olderthan_180days|active_sales_last_7_days|active_sales_last_30_days|active_sales_last_45_days|active_sales_last_60_days|active_sales_last_90_days|inv_pending_quantity_in_po|inv_qty_closed_not_inwarded|total_views_1days|total_views_7days|total_views_30days|total_oos_views_30days|total_oos_views_7days|total_oos_views_1days|drr|moving_stock_flag|inventory_aging_count_for_365|inventory_aging_count_for_180|inventory_aging_count_for_60|product_manager|active_sales_last_1_days|sales_amount_last_1_days|sales_amount_last_7_days|sales_amount_last_30_days|sales_amount_last_45_days|sales_amount_last_60_days|sales_amount_last_90_days|ean_code|hsn_code|hsn_tax|grn_quantity_30days|grn_quantity_60days|grn_quantity_90days'

ENV DSH_CUSTOMER_MASTER='user_id|contact_id|registered_time|pincode|city|state|dob|gender|age_in_months|ltv|total_orders|percentage_coupon_orders|first_order_timestamp|last_order_timestamp|ticket_size|average_buy_time|first_attempt|reachable|visited|product_viewed|cart_abandoner|frequency_label|recency_label|ltv_label|transaction_value_label'

ENV DSH_CUSTOMER_MASTER_CRM='user_id|activity_tag|latest_buy_time|last_segment|last_mail_type|last_sent_time|last_mail|mobile_identifier|desktop_identifier|android_identifier|ios_identifier|contact_id|user_name|email|phone_number|dob|firstorder_timestamp|is_approved|last_login|title|subscriber_tag|upcoming_segment|upcoming_campaign|is_send'

ENV DSH_CUSTOMER_MASTER_TEST='user_id|contact_id|registered_time|pincode|city|state|dob|gender|age_in_months|ltv|total_orders|percentage_coupon_orders|first_order_timestamp|last_order_timestamp|ticket_size|average_buy_time|first_attempt|reachable|visited|product_viewed|cart_abandoner|frequency_label|recency_label|ltv_label|transaction_value_label'
ENV DSH_CUSTOMER_MASTER_CRM_TEST='user_id|activity_tag|latest_buy_time|last_segment|last_mail_type|last_sent_time|last_mail|mobile_identifier|desktop_identifier|android_identifier|ios_identifier|contact_id|user_name|email|phone_number|dob|firstorder_timestamp|is_approved|last_login|title|subscriber_tag|upcoming_segment|upcoming_campaign|is_send'

ENV DSH_PRODUCT_ORDERING='product_id|product_sku|view_score|add_to_cart_score|orders_score|gmv_score|add_to_cart_per_view_score|orders_per_view_score|gmv_per_view_score|ordering_score|x_id'
ENV DSH_PRODUCT_ORDERING_TEST='product_id|product_sku|view_score|add_to_cart_score|orders_score|gmv_score|add_to_cart_per_view_score|orders_per_view_score|gmv_per_view_score|ordering_score|x_id'

ENV DSH_CAMPAIGN_CLICK_CONTRIBUTION='contribution_date|identifier|buy_timestamp|buy_date|click_timestamp|click_date|eventsource|eventmedium|eventcampaign|ispaid|order_id|event|click_id|gmv|nmv|mrp|order_value|mode_device|contribution_per_each_click|gmv_contribution|order_value_contribution'
ENV DSH_CAMPAIGN_CLICK_CONTRIBUTION_TEST='contribution_date|identifier|buy_timestamp|buy_date|click_timestamp|click_date|eventsource|eventmedium|eventcampaign|ispaid|order_id|event|click_id|gmv|nmv|mrp|order_value|mode_device|contribution_per_each_click|gmv_contribution|order_value_contribution'


ENV DSH_ORDER_ITEM_MASTER='order_id|time_stamp|coupon|coupon_value|payment_method|verification_method|verification_time_stamp|order_time|verification_time|updated_by|pincode|city|state|shop_orderitem_id|product_id|product_name|product_sku|our_price|mrp|order_qauntity|shipping_id|status|shipping_status_comment|awb|carrier_id|weight|item_volume|qauntity|first_picklist_time|prepared_time|dispatch_time|delivery_time|item_code|expiry_date|mfg_date|wieght|volume|cost_price|in_time|out_time|vendor_id|cancel_reason|cancel_created_by|cancel_processing_time|refund_reason|refund_processing_time|rvp_return_reason|rvp_return_processing_time|contact_id|mode_device|is_first_time_user|phone|email|delivery_tat_in_sec|category_slug|category_root_slug|brand_name|device_identifier|order_status|first_site_visit_time|time_taken_from_first_visit|source|medium|campaign|carrier_name'
ENV DSH_ORDER_ITEM_MASTER_TEST='order_id|time_stamp|coupon|coupon_value|payment_method|verification_method|verification_time_stamp|order_time|verification_time|updated_by|pincode|city|state|shop_orderitem_id|product_id|product_name|product_sku|our_price|mrp|order_qauntity|shipping_id|status|shipping_status_comment|awb|carrier_id|weight|item_volume|qauntity|first_picklist_time|prepared_time|dispatch_time|delivery_time|item_code|expiry_date|mfg_date|wieght|volume|cost_price|in_time|out_time|vendor_id|cancel_reason|cancel_created_by|cancel_processing_time|refund_reason|refund_processing_time|rvp_return_reason|rvp_return_processing_time|contact_id|mode_device|is_first_time_user|phone|email|delivery_tat_in_sec|category_slug|category_root_slug|brand_name|device_identifier|order_status|first_site_visit_time|time_taken_from_first_visit|source|medium|campaign|carrier_name'


ENV DSH_DAILY_CATEGORY_PERFORMANCE_TEST='performance_date|category_root_slug|category_slug|nmv|total_qauntity|total_orders|top_category_qauntity_sku|top_category_qauntity_sku_count|top_category_nmv_sku|top_category_nmv_sku_count|nmv_percentage|qauntity_percentage|first_order|repeat_order|view_count|add_to_cart_count|lost_nmv_category|product_skus|lost_nmv_product_count|nmv_v_iv|add_to_cart_v_iv|total_orders_with_coupon|total_orders_without_coupon|coupon_value|purchased_skus_count|active_skus_count|gmv|coupon_nmv|non_coupon_nmv'
ENV DSH_DAILY_CATEGORY_PERFORMANCE='performance_date|category_root_slug|category_slug|nmv|total_qauntity|total_orders|top_category_qauntity_sku|top_category_qauntity_sku_count|top_category_nmv_sku|top_category_nmv_sku_count|nmv_percentage|qauntity_percentage|first_order|repeat_order|view_count|add_to_cart_count|lost_nmv_category|product_skus|lost_nmv_product_count|nmv_v_iv|add_to_cart_v_iv|total_orders_with_coupon|total_orders_without_coupon|coupon_value|purchased_skus_count|active_skus_count|gmv|coupon_nmv|non_coupon_nmv'



ENV DSH_PRODUCT_GMV_PER_ITEM_VIEW_TEST='id|product_id|product_sku|product_name|stock_status|mrp|our_price|total_quantity|discount_quantity|total_orders|with_coupon|without_coupon|time_stamp|overall_couponvalue|view|brand_name|category_slug|category_root_slug|brand_id|category_id'
ENV DSH_PRODUCT_GMV_PER_ITEM_VIEW='id|product_id|product_sku|product_name|stock_status|mrp|our_price|total_quantity|discount_quantity|total_orders|with_coupon|without_coupon|time_stamp|overall_couponvalue|view|brand_name|category_slug|category_root_slug|brand_id|category_id'


ENV DSH_FEATURE_ORDERING=='entity_id|product_id|is_explicit|buy_ratio|add_to_cart_ratio|add_to_wishlist_ratio|buy_score|feature_score'

#gcp
ENV dataset=datos_pocos
ENV region=us-central1
ENV data_pipeline_bucket=purplle-dataproc-pipeline
ENV zone=us-central1-a
ENV bq_project=datapipelineproduction

#bigTable
ENV cluster_recommendation=cluster_recommendation
ENV user_cluster_info=user_cluster_info
ENV project_id=datapipelineproduction
ENV instance_id=reco-prod

ENV APP_DEBUG_FLAG=False

ENV GOOGLE_APPLICATION_CREDENTIALS=/recoEnginePyhton_new/helper/mysql/DataPipelineProduction-8521e6e33aff.json


ENV DATA_ROOT=/home/purplle/data/purplle_science_data

ENV JOBLIB_MULTIPROCESSING=0
# The next line updates PATH for the Google Cloud SDK.
#if [ -f '/home/purplle/google-cloud-sdk/path.bash.inc' ]; then . '/home/purplle/google-cloud-sdk/path.bash.inc'; fi

# The next line enables shell command completion for gcloud.
#if [ -f '/home/purplle/google-cloud-sdk/completion.bash.inc' ]; then . '/home/purplle/google-cloud-sdk/completion.bash.inc'; fi

COPY . .
RUN apt-get update && apt-get -y upgrade
# RUN apt-get install make automake gcc g++ subversion python3-dev
RUN pip install --upgrade pip
RUN pip install --upgrade setuptools
ADD requirements.txt .
RUN ls -la
#RUN pip install grpcio==1.30.0
RUN pip install --user -r requirements.txt
RUN python --version
RUN pip --version

RUN pip install numpy --upgrade
RUN pip install --user -r requirements.txt
# RUN apt-get install -y curl \
#   && curl -sL https://deb.nodesource.com/setup_9.x | bash - \
#   && apt-get install -y nodejs \
#   && curl -L https://www.npmjs.com/install.sh | sh
# RUN node --version
# RUN npm --version
# RUN npm install pm2 -g
#RUN pm2 status
RUN pip install mysql-connector-python
RUN pip install psycopg2-binary
RUN pip install --upgrade google-api-core
RUN pip install elasticsearch
RUN pip install openpyxl

RUN pip install fuzzywuzzy
RUN pip install flashtext
#RUN apt-get install libomp-dev
RUN apt-get install -y gcc make swig libomp-dev
RUN pip install faiss-cpu
RUN pip install swifter
RUN pip install gcloud
RUN pip install google-cloud-storage
RUN pip install sqlalchemy-utils
RUN pip install google-cloud-bigquery
RUN pip install google-cloud-bigtable
RUN pip install nltk
RUN pip install pytest
RUN GRPC_HEALTH_PROBE_VERSION=v0.3.1 && \
    wget -qO/bin/grpc_health_probe https://github.com/grpc-ecosystem/grpc-health-probe/releases/download/${GRPC_HEALTH_PROBE_VERSION}/grpc_health_probe-linux-amd64 && \
    chmod +x /bin/grpc_health_probe
RUN pip install gspread
RUN pip install gspread-dataframe
RUN pip install torch
RUN pip install scikit-learn
RUN pip install oauth2client
RUN pip install statistics
RUN pip install scipy
RUN pip install pyarrow
# RUN pip install boto
RUN pip install redis
RUN pip install pandas-gbq -U
RUN pip install pytz
RUN pip install pika
EXPOSE 5000
CMD ["python","client.py"]
#CMD ["pm2","start","server.py"]
#CMD ["sh", "-c", "tail -f /dev/null"]








